<?php namespace Clearweb\Clearwebapps\Form;

abstract class PlaceholderField extends Field 
{
	var $placeholder = '';
	
	function setPlaceholder($placeholder) {
		$this->placeholder = $placeholder;
		return $this;
	}
	
	function getPlaceholder() {
		return $this->placeholder;
	}
}