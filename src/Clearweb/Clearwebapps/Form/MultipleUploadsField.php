<?php namespace Clearweb\Clearwebapps\Form;

use Clearweb\Clearwebapps\File\File;
use \FileManager;
use Clearweb\Clearwebapps\Upload\UploadAction;

class MultipleUploadsField extends Field
{
    public function __construct()
    {
        $this->addScript('upload-field', 'packages/clearweb/clearwebapps/js/upload_field.js', array('jquery'));
    }
    
	/**
	 * Gets the type of this field
	 */
	public function getType()
	{
		return 'multiple-uploads';
	}
	
	function getView() {
		$html = '<label>'.$this->getLabel().'</label>';
		$html .= '<p>'.$this->getDescription().'</p>';		
		
		$html .= '<ul class="multi file-container">';
		foreach($this->getFiles() as $i => $file) {
			$html .= '<li data-fileindex="'.$i.'" class="existing-file">'.
				"<a target='_blank' href='{$file->getUrl()}'>{$file->getName()}</a>".
				"<input type='hidden' name='{$this->getName()}[]' value='{$file->getId()}' />".
				"<a class='delete'>delete</a>".
				"</li>";
		}
		$html .= '</ul>';
		
		$html .= '<input multiple="multiple" data-uploadurl="'.\Clearworks::getActionUrl(new UploadAction).'" type="file" class="ajaxupload" data-fieldname="'.$this->getName().'[]" />';
		
		return $html;
	}
	
	
	function getMultiple() {
		return $this->multiple;
	}
	
	function setMultiple($multiple=true) {
		$this->multiple = $multiple;
		return $this;
	}
	
	function getFiles() {
		$value = $this->getValue();
		
		if (empty($value)) {
			$files = array();
		} else {
			$file_ids = explode(';', $value);
			
			$files = array_map(
							   function($file_id) {
								   return FileManager::getFile($file_id);
							   },
							   $file_ids
							   );
		}
		
		return $files;
	}
	
	function setFiles($files) {
		return implode(
					   ';',
					   array_map(
								 function($file) {
									 return $file->getId();
								 },
								 $files
								 )
					   );
	}

}