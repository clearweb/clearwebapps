<?php namespace Clearweb\Clearwebapps\Form;

class DateField extends PlaceholderField {
    public function __construct()
    {
        $this->addScript('jquery-ui', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js', array('jquery'));
    }
    
	/**
	 * Gets the type of this field
	 */
	public function getType()
	{
		return 'date';
	}
	
	function getView() {
		return \View::make('clearwebapps::text_field')
            ->with('name', $this->getName())
            ->with('label', $this->getLabel())
            ->with('value', $this->getValue())
            ->with('placeholder', $this->getPlaceholder())
            ->with('description', $this->getDescription())
            ->with('class', 'datepicker')
            ->with('disabled', $this->getDisabled())
            ;
	}
	
}