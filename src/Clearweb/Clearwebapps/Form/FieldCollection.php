<?php namespace Clearweb\Clearwebapps\Form;

use Clearweb\Clearworks\Contracts\IViewable;

class FieldCollection implements IHasFields, IViewable
{
    private $fields = array();
	private $values = array();
    

    public function getView() {
		return $this->getFieldViews();
	}

    /**
	 * gets the scripts which should be added for the form.
	 * @return array An array with the script path as string.
	 */
	public function getScripts() {
        $scripts = array();
		
		foreach($this->getFields() as $field) {
			$scripts = array_merge($scripts, $field->getScripts());
		}
		
		return $scripts;
	}
	
	public function getStyles() {
		$styles = array();
		
		foreach($this->getFields() as $field) {
			$styles = array_merge($styles, $field->getStyles());
		}
		
		return $styles;
	}
    
    /**
	 * Gets the form fields
	 * @return Array an array with IField objects
	 */
	public function getFields() {
		return $this->fields;
	}

    /**
	 * Gets a field by name
	 * @param string $name the name of the field
	 * @return \Form\IField the field
	 */
	public function getField($name) {
		$field = NULL;
		foreach($this->getFields() as $current_field) {
			if ($current_field->getName() == $name) {
				$field = $current_field; 
			} else {
                if ($current_field instanceof IHasFields) {
                    try{
                        $field = $current_field->getField($name);
                    } catch(Exception\UnknownFieldException $exception) {
                        
                    }
                }
            }
		}
		
		if (is_null($field)) {
			throw new Exception\UnknownFieldException("The field with name '$name' is unknown in form '{$this->getName()}'");
		}
		
		return $field;
	}
    
    /**
	 * Adds a field to the form
	 * @param IField $field the field to add
	 * @param int $position the position of the field. Equal or less than 0 means append field.
	 * @return $this
	 * @throws DuplicateFieldException
	 */
	public function addField(IField $field, $position=0) {
		if ($this->nameExists($field->getName()))
			throw new Exception\DuplicateFieldException("field '{$field->getName()}' already exists");
		
        if ($field instanceof IHasValue) {
            $this->setValue($field->getName(), $field->getValue());
        }
		
		if ($position > 0) {
			$this->fields = array_merge(array_slice($this->fields, 0, $position-1), array($field), array_slice($this->fields, $position-1));
		} else {
			$this->fields[] = $field;
		}
		
		return $this;
	}
    
	/**
	 * Adds fields to the form
	 * @param Array $fields an array with IField objects to add to the list
	 * @return $this
	 * @throws DuplicateFieldException
	 */
	public function addFields(array $fields, $position=0) {
		foreach($fields as $field) {
			$this->addField($field, $position);
			if ($position > 0) {
				$position++;
			}
		}
		return $this;
	}
    
    	
	/**
	 * Removes a field by name.
	 * @throws UnknownFieldException
	 * @return $this
	 */
	public function removeField($name) {
		if (!$this->nameExists($name))
			throw new Exception\UnknownFieldException("field '{$name}' does not exist in field");
		
		foreach($this->fields as $i => $field) {
			if ($field->getName() == $name)
				unset($this->fields[$i]);
		}
		
		return $this;
	}
	
	/**
	 * Replaces a field by another one.
	 * @param string $name the name of the field
	 * @param \Form\IField $field the field to replace the old one with
	 * return $this;
	 * @throws UnknownFieldException
	 * @throws DuplicateFieldException
	 */
	public function replaceField($name, IField $field) {
		if (!$this->nameExists($name))
			throw new Exception\UnknownFieldException("field '{$name}' does not exist in field");
		
		if ($field->getName() != $name && $this->nameExists($field->getName()))
			throw new Exception\DuplicateFieldException("field '{$field->getName()}' already exists");
		
		foreach($this->fields as $i => $current_field) {
			if ($current_field->getName() == $name) {
				$this->fields[$i] = $field;
			}
		}
		
		return $this;
	}
	
	/**
	 * Moves field to another position
	 * @param $name the name of the field
	 * @param $position the new position
	 * @return $this;
	 */
	public function moveField($name, $position) {
		$field = $this->getField($name);
		$this->removeField($name)
			->addField($field, $position);
		
		return $this;
	}
	
	/**
	 * Clears all fields in the form
	 * @return $this
	 */
	public function clearFields() {
		$this->fields = array();
		
		return $this;
	}
    
    /**
     * Checks if the field with the given name exists
     * @param string $name the name of the field to check
     * @return boolean if the field exists or not.
     */
    public function hasField($name) {
        $exists = $this->nameExists($name);
        
        foreach($this->getFields() as $field) {
            if ($field instanceof IHasFields) {
                if ($field->hasField($name)) {
                    $exists = true;
                }
            }
        }
        
        return $exists;
    }
    
    protected function nameExists($name) {
        $exists = FALSE;
		
        foreach($this->getFields() as $field) {
            if ($field->getName() == $name)
                $exists = TRUE;
        }
		
        return $exists;
    }
    
	protected function getFieldView(IField $field) {
        if ($field instanceof FieldCollection) {
            $field->setValues($this->getValues());
        }
        
		if ( ! is_null($this->getValue($field->getName()))) {
			if ($field instanceof IHasValue) {
                $field->setValue($this->getValue($field->getName()));
            }
        }
		
		return \View::make('clearwebapps::field_wrapper')
            ->with('name', $field->getName())
            ->with('type', $field->getType())
            ->with('disabled', $field->getDisabled())
            ->with('field', $field->getView())
            ;
	}
	
	public function getFieldViews() {
		$html = '';
		$fields = $this->getFields();
		foreach($fields as $field) {
			if ($field->getName()) {
				$html .= $this->getFieldView($field);
			}
		}
		
		return $html;
	}
    
	/**
	 * Sets a field value
	 * @param string $key the field name.
	 * @param string $value the value of the field.
	 * @return $this
	 */
	public function setValue($key, $value) {
		$this->values[$key] = $value;
		return $this;
	}
	
	/**
	 * Gets the value of a key
	 * @param string $key the key
	 * @param mixed $default the default value if the value is not set.
	 */
	public function getValue($key, $default = NULL) {
		if (isset($this->values[$key]))
			$value = $this->values[$key];
		else
			$value = $default;
		
		return $value;
	}
	
    
    /**
	 * Sets the values of the fields (merges with current values, if this is
	 * undesirable, use clearValues before).
	 * @param array $values the values you want to be set/
	 * @return  $this.
	 */
	public function setValues(array $values) {
		$this->values = array_merge($this->values, $values);
	}
	
	/**
	 * Gets all the values
	 */
	public function getValues() {
		return $this->values;
	}
	
	/**
	 * Clears all the values of the fields
	 * @return $this.
	 */
	public function clearValues() {
		$this->values = array();
		return $this;
	}
}