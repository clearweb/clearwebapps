<?php namespace Clearweb\Clearwebapps\Form;

use Clearweb\Clearworks\Contracts\IViewable;

interface IField extends IViewable
{
    public function getName();
    public function setName($name);
    public function getType();
    public function getDisabled();
    public function setDisabled($disabled);
}
