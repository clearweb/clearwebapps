<?php namespace Clearweb\Clearwebapps\Form;

use View;

class TextField extends PlaceholderField {
	/**
	 * Gets the type of this field
	 */
	public function getType()
	{
		return 'text';
	}
	
	function getView() {
		return View::make('clearwebapps::text_field')
            ->with('name', $this->getName())
            ->with('label', $this->getLabel())
            ->with('value', $this->getValue())
            ->with('placeholder', $this->getPlaceholder())
            ->with('description', $this->getDescription())
            ->with('class', '')
            ->with('disabled', $this->getDisabled())
            ;
	}

	function getScripts() {
		return array();
	}
	
	function getStyles() {
		return array();
	}
}