<?php namespace Clearweb\Clearwebapps\Form;

interface IHasValue
{
    public function getValue();
    public function setValue($value);
}