<?php namespace Clearweb\Clearwebapps\Form;

class SelectField extends PlaceholderField {
	private $options = array();
	//private $selected = NULL;
    private $multiple = false;
	private $default = NULL;
    
	/*
	 * alphabetical sorting
	 */
	private $sort = true;


    public function __construct()
    {
        $this->addScript('chosen', 'packages/clearweb/clearwebapps/js/chosen/chosen.jquery.min.js', array('jquery'));
        $this->addStyle('packages/clearweb/clearwebapps/js/chosen/chosen.min.css');
    }
    
	/**
	 * Gets the type of this field
	 */
	public function getType()
	{
		return 'select';
	}
	
	/**
	 * Sets the options for the SelectField
	 * @param array $options an array of the following form: array([value]=>[nice text])
	 * @param string|NULL $default the default choice of the select. If is null, first is default
	 * @return SelectField $this
	 * @throws \Form\Exception\InvalidOptionArrayException
	 * @throws \Form\Exception\InvalidOptionException
	 */
	function setOptions(array $options, $default=NULL)
	{
		if ($this->validateOptions($options)) {
			$this->options = $options;
			$this->setDefault($default);
		} else {
			throw new Exception\InvalidOptionArrayException("invalid options '".json_encode($options)."' in select '{$this->getName()}'");
		}
		
		return $this;
	}

	function getOptions()
	{
		return $this->options;
	}
	
	/**
	 * Sets alphabetical sorting on or off
	 * @param boolean $sort if <code>true</code>, sort options alphabetically.
	 */
	function setSort($sort = true) {
		$this->sort = $sort;
        return $this;
	}

	/**
	 * Gets if alphabetical sorting on or off
	 * @return boolean if <code>true</code>, sort options alphabetically, otherwise, does not sort.
	 */
	function getSort() {
		return $this->sort;
	}

    /**
     * Sets if the multiple selection is allowed or not
     * @param boolean multiple or not.
     * @return this.
     */
    public function setMultiple($multiple)
    {
        $this->multiple = $multiple;
        return $this;
    }

    /**
     * Gets if the select is multiple
     * @return bookean if multiple selection is allowed or not
     */
    public function getMultiple()
    {
        return $this->multiple;
    }

	/**
	 * Sets the default option for the select
	 * @param String|NULL $option_value the default option value. If null, the default is the first option.
	 * @return SelectField $this
	 * @throws \Form\Exception\InvalidOptionException
	 */
	function setDefault($option_value)
	{
		if (is_null($option_value) || $this->hasOption($option_value)) {
			$this->default = $option_value;
		} else {
			throw new Exception\InvalidOptionException("'$option_value' is not a valid choice in select '{$this->getName()}'. Valid choices are: ".json_encode($this->getOptions()));
		}
		
		return $this;
	}
	
	/**
	 * Gets the default option. If no default option exists, get the first option
	 * @return string|NULL if we have no options, NULL otherwise the default
	 */
	function getDefault()
	{
		if (empty($this->default) && ! empty($this->options) && ! $this->getMultiple()) {
			$option_keys = array_keys($this->options);
			return array_shift($option_keys);
		} else {
			return $this->default;
		}
	}
	
	/**
	 * Sets the selected value
	 * @param $option_value the value of the select.
	 * @return SelectField $this
	 * @throws \Form\Exception\InvalidOptionException
	 */
	function setValue($option_value=null) {
        $bad_option = false;
        
        if (is_array($option_value)) {
            foreach($option_value as $value) {
                $bad_option = false;
                
                if ( ! (empty($value) || $this->hasOption($value))) {
                    $bad_option = $value;
                    continue;
                }
            }
            
            if ($bad_option) {
                throw new Exception\InvalidOptionException("'$bad_option' is not a valid choice in select '{$this->getName()}'. Valid choices are: ".json_encode($this->getOptions()));
            } else {
                $this->value = implode(';', $option_value);
            }
        } else {
            if (empty($option_value) || $this->hasOption($option_value)) {
                $this->value = $option_value;
            } else {
                throw new Exception\InvalidOptionException("'$option_value' is not a valid choice in select '{$this->getName()}'. Valid choices are: ".json_encode($this->getOptions()));
            }
        }
        
		return $this;
	}
	
	/**
	 * Gets the option which should be selected.
	 * @return string|NULL if we have no options NULL, otherwise the selected
	 */
	function getValue() {
		return $this->value;
	}
	
	/**
	 * @todo add options HTML to select
	 */
	function getView()
	{
		$selected = $this->getValue();
		
		$options = $this->getOptions();
		
		if ($this->getSort()) {
			asort($options);
		}
		
		if (empty($selected))
			$selected = $this->getDefault();
        
        $placeholder = $this->getPlaceholder();
        if (empty($placeholder)) {
            $placeholder = trans('clearwebapps::form_widget.choose');
        }
        
        return \View::make('clearwebapps::select_field')
            ->with('name', $this->getName())
            ->with('label', $this->getLabel())
            ->with('description', $this->getDescription())
            ->with('selected', $selected)
            ->with('options', $options)
            ->with('multiple', $this->getMultiple())
            ->with('placeholder', $placeholder)
            ;
        
	}
	
	/**
	 * Checks if we have an option with a given value
	 */
	protected function hasOption($option_value) {
		return ((is_string($option_value) || is_numeric($option_value)) && isset($this->options[$option_value]));
	}
	
	/**
	 * Validates if an array is fit to use it as an options array
	 */
	private function validateOptions(array $options)
	{
		$validated_options = array();
		$valid = TRUE;
		
		foreach($options as $key => $value) {
			if (isset($validated_options[$key])) {
				$valid = FALSE;
			} elseif( ! (preg_match('#[a-zA-Z0-9]#', $key))) {
				$valid = FALSE;
			} else {
				$validated_options[$key] = TRUE;
			}
		}
		
		return $valid;
	}

}