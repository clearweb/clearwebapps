<?php namespace Clearweb\Clearwebapps\Form;

class Text extends Field {
	function __construct($name) {
		$this->setName($name);
	}
	
	/**
	 * Gets the type of this field
	 */
	public function getType()
	{
		return 'form-text';
	}
	
	function getView() {
		return '<p>'.$this->getLabel().'</p>';
	}

	function getScripts() {
		return array();
	}
	
	function getStyles() {
		return array();
	}
}