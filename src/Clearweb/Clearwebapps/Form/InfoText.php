<?php namespace Clearweb\Clearwebapps\Form;

class InfoText extends Field {
	function __construct($name, $content=false) {
		if ($content) {
			$this->setLabel($content);
		} else {
			$this->setLabel($name);
		}
		
		$this->setName($name);
	}
	
	/**
	 * Gets the type of this field
	 */
	public function getType()
	{
		return 'info-text';
	}
	
	function getView() {
		return '<legend>'.$this->getLabel().'</legend>';
	}

	function getScripts() {
		return array();
	}
	
	function getStyles() {
		return array();
	}
}