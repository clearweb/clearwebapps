<?php namespace Clearweb\Clearwebapps\Form\Exception;

class InvalidValueException extends FormException {}