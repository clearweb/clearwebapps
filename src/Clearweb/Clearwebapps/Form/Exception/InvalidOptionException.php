<?php namespace Clearweb\Clearwebapps\Form\Exception;

class InvalidOptionException extends InvalidValueException {}