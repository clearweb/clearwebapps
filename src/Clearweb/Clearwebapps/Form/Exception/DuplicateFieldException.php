<?php namespace Clearweb\Clearwebapps\Form\Exception;

class DuplicateFieldException extends FormException {}