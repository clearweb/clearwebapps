<?php namespace Clearweb\Clearwebapps\Form\Exception;

class UnknownFieldException extends FormException{}