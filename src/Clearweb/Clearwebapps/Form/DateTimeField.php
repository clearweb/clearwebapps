<?php namespace Clearweb\Clearwebapps\Form;

class DateTimeField extends PlaceholderField {
    public function __construct()
    {
        $this->addScript('jquery-ui', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js', array('jquery'))
            ->addScript('timepicker', 'packages/clearweb/clearwebapps/js/jquery-ui-timepicker-addon.js', array('jquery-ui'))
            ;
    }
    
	/**
	 * Gets the type of this field
	 */
	public function getType()
	{
		return 'datetime';
	}
	
	function getView() {
		return \View::make('clearwebapps::text_field')
            ->with('name', $this->getName())
            ->with('label', $this->getLabel())
            ->with('value', $this->getValue())
            ->with('placeholder', $this->getPlaceholder())
            ->with('description', $this->getDescription())
            ->with('class', 'datetimepicker')
            ->with('disabled', $this->getDisabled())
            ;
	}

}