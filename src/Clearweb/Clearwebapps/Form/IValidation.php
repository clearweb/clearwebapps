<?php namespace Form;

interface IValidation {
	/**
	 * Gets the validation errors
	 * @return array with errors. format: array([field]=>[error])
	 */
	function getErrors();
}