<?php namespace Clearweb\Clearwebapps\Form;

class HTMLField extends Field
{	
    public function __construct()
    {
        $this->addScript('ck','packages/clearweb/clearwebapps/js/ckeditor/ckeditor.js',array())
            ->addScript('ck-jquery', 'packages/clearweb/clearwebapps/js/ckeditor/adapters/jquery.js', array('jquery', 'ck'))
            ;
    }
    
	/**
	 * Gets the type of this field
	 */
	public function getType()
	{
		return 'html-edit-field';
	}
	
	function getView() {
        return \View::make('clearwebapps::html_field')
            ->with('name', $this->getName())
            ->with('label', $this->getLabel())
            ->with('value', $this->getValue())
            ;
	}
}