<?php namespace Clearweb\Clearwebapps\Form;

use View;

class SubmitField extends Field {
	/**
	 * Gets the type of this field
	 */
	public function getType()
	{
		return 'submit';
	}
	
	function getView() {
        return View::make('clearwebapps::submit_field')
            ->with('label', $this->getLabel())
            ->with('name', $this->getName())
            ;
	}
	
	function getScripts() {
		return array();
	}
	
	function getStyles() {
		return array();
	}
}