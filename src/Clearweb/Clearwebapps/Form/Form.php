<?php namespace Clearweb\Clearwebapps\Form;

use Illuminate\Support\Str;
use Clearweb\Clearworks\Contracts\IParametrizable;

class Form extends FieldCollection implements IParametrizable
{
	/**
	 * This is a list with parameters for the form.
	 */
	private $parameters = array();
	
	private $validation_errors = array();
    
	private $action = '';
	private $values = array();
	
	private $name = '';
	
	/**
	 * Constant for POST method
	 */
	const METHOD_POST = 1;

	/**
	 * Constant for GET method
	 */
	const METHOD_GET = 2;
	
	public function init() {
		return $this;
	}
	
	public function execute() {
		return $this;
	}
	
	function getName() {
		return $this->name;
	}
	
	function setName($name) {
		$this->name = $name;
		return $this;
	}
	
	
	/**
	 * Set a parameter
	 * @param $key string the key of the parameter
	 * @param $value string the value of the parameter
	 * @return Form the current object for chaining purposes
	 */
	public function setParameter($key, $value=null) {
		$this->parameters[$key] = $value;
		return $this;
	}
	
	/**
	 * Get a parameter
	 * @param string $key the key of the parameter
	 * @param string|NULL $default the default value of the parameter
	 * @return string|NULL the value of the parameters.
	 */
	public function getParameter($key, $default=NULL) {
		if (isset($this->parameters[$key]))
			return $this->parameters[$key];
		else
			return $default;
	}
	
	/**
	 * Get all parameters
	 * @return array an array with all parameters.
	 */
	public function getParameters() {
		return $this->parameters;
	}

	/**
	 * Get all parameters
     * @param array $parameters an array with all parameters.
	 * @return $this
	 */
	public function setParameters(array $parameters) {
	    $this->parameters = $parameters;
        
        return $this;
	}
	
	/**
	 * gets the scripts which should be added for the form.
	 * @return array An array with the script path as string.
	 */
	public function getScripts() {
        $scripts = array(
                         array('name' => 'form',
                               'url'=>'packages/clearweb/clearwebapps/js/form.js',
                               'dependencies' => array('jquery'),
                               )
                         );
		
		foreach($this->getFields() as $field) {
			$scripts = array_merge($scripts, $field->getScripts());
		}
		
		return $scripts;
	}
	
	public function getStyles() {
		$styles = array();
		
		foreach($this->getFields() as $field) {
			$styles = array_merge($styles, $field->getStyles());
		}
		
		return $styles;
	}
    
    /**
     * Sets the form action.
     * @param string $action the action url
     * @return this for chaining purposes
     */
	public function setAction($action) {
		$this->action = $action;
		return $this;
	}
	
    /**
     * Gets the form action.
     * @return string the form action
     */
	public function getAction() {
		return $this->action;
	}
	
	public function getMethod() {
		return self::METHOD_POST;
	}
	
	/**
	 * Gets the validation errors
	 * @return array An array in the following form array([field]=>[message])
	 */
	public function getValidationErrors() {
		return $this->validation_errors; 
	}
	
	/**
	 * Gets the validation errors
	 * @params $validation_errors array An array in the following form array([field]=>[message])
	 * @return $this
	 */
	public function setValidationErrors(array $validation_errors) {
		$this->validation_errors = $validation_errors;
		return $this;
	}
	
	
	
    public function getView() {
		return \View::make('clearwebapps::form')
			->with('fields', $this->getFieldViews())
			->with('action', $this->getAction())
			->with('method', $this->getMethod());
	}
	
	/**
	 * Adds an validation error to the form.
	 * @param string $field the field with error.
	 *
	 * @return $this
	 */
	protected function addValidationError($field, $message='') {
		$this->validation_errors[$field] = $message;
		
		return $this;
	}

}