<?php namespace Clearweb\Clearwebapps\Form;

use \Symfony\Component\Translation\IdentityTranslator;
use \Symfony\Component\Translation\MessageSelector;

class Validator extends \Illuminate\Validation\Validator
{
	public function __construct() {
        $this->setPresenceVerifier(\App::make('validation.presence'));
		$this->setTranslator(new IdentityTranslator);
	}
}