<?php namespace Clearweb\Clearwebapps\Form;

use View;

class ColorField extends PlaceholderField
{
	/**
	 * Gets the type of this field
	 */
	public function getType()
	{
		return 'color';
	}
	
	function getView()
	{
		$value = $this->getValue();
		if (empty($value)) {
			$value = '#000000';
		}
        return View::make('clearwebapps::color_field')
            ->with('name', $this->getName())
            ->with('label', $this->getLabel())
            ->with('value', $value)
            ->with('placeholder', $this->getPlaceholder())
            ->with('description', $this->getDescription())
            ->with('disabled', $this->getDisabled())
            ;
	}
	
	function getStyles()
	{
		return array(
					 'css/colpick.css',
					 'css/color_field.css'
					 );
	}
	
	function getScripts()
	{
		return array(
					 array('name'=>'colpick', 'url'=>'js/colpick.js', 'dependencies'=>array()),
					 array('name'=>'color-field', 'url'=>'js/color_field.js', 'dependencies'=>array()),
					 );
	}
}