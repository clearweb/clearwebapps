<?php namespace Clearweb\Clearwebapps\Form;

abstract class Field implements IField, IHasValue {
	protected $value = '';
	protected $description = '';
	protected $name = '';
	protected $label = null;
    protected $disabled = false;
    
    private $additional_scripts = array();
	private $additional_styles  = array();
	
	abstract function getView();
    
    function addScript($name, $url, array $dependencies=array()) {
        $this->additional_scripts[$name] = array('name'=>$name, 'url'=>$url, 'dependencies'=>$dependencies);
		return $this;
	}
	
	function addStyle($style) {
		$this->additional_styles[] = $style;
		return $this;
	}
	
    function getScripts()
    {
        return $this->additional_scripts;
    }
    
    function getStyles()
    {
        return $this->additional_styles;
    }
	
	abstract function getType();

    function setDisabled($disabled) {
		$this->disabled = $disabled;
		
		return $this;
	}
	
	function getDisabled() {
		return $this->disabled;
	}
    
	function setDescription($description) {
		$this->description = $description;
		
		return $this;
	}
	
	function getDescription() {
		return $this->description;
	}
	
	function setValue($value) {
		$this->value = $value;
		
		return $this;
	}
	
	function getValue() {
		return $this->value;
	}
	
	function setLabel($label) {
		$this->label = $label;
		
		return $this;
	}
	
	function getLabel() {
		if (is_null($this->label))
			return $this->name;
		else
			return $this->label;
	}
	
	function setName($name) {
		$this->name = $name;
		
		return $this;
	}
	
	function getName() {
		return $this->name;
	}
}