<?php namespace Clearweb\Clearwebapps\Form;

use View;

class PasswordField extends Field {
	/**
	 * Gets the type of this field
	 */
	public function getType()
	{
		return 'password';
	}
	
	function getView() {
        return View::make('clearwebapps::password_field')
            ->with('name',  $this->getName())
            ->with('label', $this->getLabel())
            ->with('value', $this->getValue())
            ->with('description', $this->getDescription())
            ;
	}

	function getScripts() {
		return array();
	}
	
	function getStyles() {
		return array();
	}
}