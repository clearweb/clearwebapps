<?php namespace Clearweb\Clearwebapps\Form;

use Clearweb\Clearwebapps\File\File;
use FileManager;
use Clearweb\Clearwebapps\Upload\UploadAction;

class ImageUploadField extends UploadField
{
	/**
	 * Gets the type of this field
	 */
	public function getType()
	{
		return 'image-upload-field';
	}
	
	function getView() {
		$html = '<label>'.$this->getLabel().'</label>';
		$html .= '<p>'.$this->getDescription().'</p>';
		$html .= '<ul class="file-container">';
		
		$file = $this->getFile();
		
		if ( is_null($file)) {
			$html .= '<li class="placeholder"><span>klik hier om een afbeelding te selecteren</span></li>';
		} else {
			$html .= "<li class='placeholder' style='background-image: url({$file->getUrl()});'><span>klik hier om de afbeelding te wijzigen</span></li>".

				'<li data-fileindex="0" class="existing-file">'.
				"<input type='hidden' class='image-handle' name='{$this->getName()}' value='{$file->getId()}' />".
				"<div class='file-content'><a target='_blank' class='file-name' href='{$file->getUrl()}'>{$file->getName()}</a><div class='delete'>delete</div></div>".
				"</li>";
		}
		$html .= '</ul>';
		$html .= '<div class="img-loader"></div>';
		
		
		$html .= '<input data-uploadurl="'.\Clearworks::getActionUrl(new UploadAction).'" type="file" class="ajaxupload" data-fieldname="'.
			$this->getName().'" />';
		
		return $html;
	}
}