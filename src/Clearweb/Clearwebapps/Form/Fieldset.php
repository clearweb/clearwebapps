<?php namespace Clearweb\Clearwebapps\Form;

use Clearweb\Clearworks\Contracts\IParametrizable;

class Fieldset extends FieldCollection implements IField, IParametrizable
{
    /**
	 * This is a list with parameters for the form.
	 */
	private $parameters = array();
    
    protected $name = '';
    protected $disabled = false;
    
    function getType()
    {
        return 'fieldset';
    }
    
	function getName()
    {
		return $this->name;
	}
    
    function setName($name)
    {
		$this->name = $name;
		
		return $this;
	}
    
	function getDisabled()
    {
		return $this->disabled;
	}
    
    function setDisabled($disabled)
    {
		$this->disabled = $disabled;
		
		return $this;
	}
    
    
    	
	/**
	 * Set a parameter
	 * @param $key string the key of the parameter
	 * @param $value string the value of the parameter
	 * @return Form the current object for chaining purposes
	 */
	public function setParameter($key, $value=null) {
		$this->parameters[$key] = $value;
		return $this;
	}
	
	/**
	 * Get a parameter
	 * @param string $key the key of the parameter
	 * @param string|NULL $default the default value of the parameter
	 * @return string|NULL the value of the parameters.
	 */
	public function getParameter($key, $default=NULL) {
		if (isset($this->parameters[$key]))
			return $this->parameters[$key];
		else
			return $default;
	}
	
	/**
	 * Get all parameters
	 * @return array an array with all parameters.
	 */
	public function getParameters() {
		return $this->parameters;
	}

	/**
	 * Get all parameters
     * @param array $parameters an array with all parameters.
	 * @return $this
	 */
	public function setParameters(array $parameters) {
	    $this->parameters = $parameters;
        
        return $this;
	}
    
    public function init() {
        return $this;
    }
    
    public function getView() {
		return \View::make('clearwebapps::fieldset')->with('fields', $this->getFieldViews());
	}
}