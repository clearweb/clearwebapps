<?php namespace Clearweb\Clearwebapps\Form;

use Clearweb\Clearwebapps\File\File;
use FileManager;
use Clearweb\Clearwebapps\Upload\UploadAction;

class UploadField extends Field
{
    public function __construct()
    {
        $this->addScript('upload-field', 'packages/clearweb/clearwebapps/js/upload_field.js', array('jquery'));
    }
    
	/**
	 * Gets the type of this field
	 */
	public function getType()
	{
		return 'upload';
	}
	
	function getView() {
		$html = '<label>'.$this->getLabel().'</label>';
		$html .= '<p>'.$this->getDescription().'</p>';		
		$html .= '<ul class="file-container">';
		
		$file = $this->getFile();
		
		if ( ! is_null($file)) {
			$html .= '<li data-fileindex="0" class="existing-file">'.
				"<a target='_blank' href='{$file->getUrl()}'>{$file->getName()}</a>".
				"<input type='hidden' name='{$this->getName()}' value='{$file->getId()}' />".
				"<a class='delete'>delete</a>".
				"</li>";
		}
		
		$html .= '</ul>';
		
		
		$html .= '<input data-uploadurl="'.\Clearworks::getActionUrl(new UploadAction).'" type="file" class="ajaxupload" data-fieldname="'.
			$this->getName().'" />';
		
		return $html;
	}
	
	function getFile() {
		if (is_null($this->getvalue()) || $this->getvalue() == '')
			return null;
		else
		return FileManager::getFile($this->getValue());
	}
	
	function setFile(IFile $file) {
		$this->setValue($file->getId());
	}
	
}