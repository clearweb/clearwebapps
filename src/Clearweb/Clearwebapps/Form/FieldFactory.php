<?php namespace Clearweb\Clearwebapps\Form;

class FieldFactory {
	static function make($field_name, $type, $value=null, $options=array()) {
		switch($type) {
			case "text":
				$field = with(new TextField())->setName($field_name)->setValue($value);
				break;
			case "password":
				$field = with(new PasswordField())->setName($field_name)->setValue($value);
				break;
			case "textarea":
				$field = with(new TextArea())->setName($field_name)->setValue($value);
				break;
			case "html":
				$field = with(new HTMLField())->setName($field_name)->setValue($value);
				break;
			case "date":
				$field = with(new DateField())->setName($field_name)->setValue($value);
				break;
			case "datetime":
				$field = with(new DateTimeField())->setName($field_name)->setValue($value);
				break;
			case "checkbox":
				$field = with(new CheckboxField())->setName($field_name)->setValue($value);
				break;
			case 'select':
				$field = with(new SelectField())->setName($field_name)->setOptions($options)->setValue($value);
				break;
			case "hidden":
				$field = with(new HiddenField())->setName($field_name)->setValue($value);
				break;
			case "submit":
				$field = with(new SubmitField())->setName($field_name)->setLabel($value);
				break;
			default:
				$field = null;
		}
		
		return $field;
	}
}