<?php namespace Clearweb\Clearwebapps\Form;

class CheckboxField extends Field {
	/**
	 * Gets the type of this field
	 */
	public function getType()
	{
		return 'checkbox';
	}
	

	function getView() {
		$checked = '';
		
		if ($this->getValue() == true) {
			$checked='checked="checked"';
		}
		
		return '<input type="checkbox" name="'.
			$this->getName().'" '.$checked.' />'.
            '<label>'.$this->getLabel().'</label>';
	}

	function getScripts() {
		return array();
	}
	
	function getStyles() {
		return array();
	}
}