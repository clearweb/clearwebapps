<?php namespace Clearweb\Clearwebapps\Form;

class HiddenField extends Field {
	/**
	 * Gets the type of this field
	 */
	public function getType()
	{
		return 'hidden';
	}
	
	function getView() {
		return '<input type="hidden" name="'.$this->getName().'" value="'.$this->getValue().'" />';
	}
	
	function getScripts() {
		return array();
	}
	
	function getStyles() {
		return array();
	}
}