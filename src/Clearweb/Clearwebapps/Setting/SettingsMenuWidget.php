<?php namespace Clearweb\Clearwebapps\Setting;

use Clearweb\Clearworks\Widget\ContainerWidget;

use Clearweb\Clearworks\Content\RawHTML;

use Clearweb\Clearwebapps\Form\Form;
use Clearweb\Clearwebapps\Form\Validator;

use View;

class SettingsMenuWidget extends ContainerWidget
{
    private $packages = array();
    
    public function init()
    {
        parent::init();
        
        $this->getContainer()
            ->addViewable(
                          with(new RawHTML)
                          ->setHTML(
                                    View::make('clearwebapps::settings_menu')
                                    ->with('packages', $this->getPackages())
                                    )
                          
                          )
            ;
        
        
        return $this;
    }
    
    public function setPackages($packages)
    {
        $this->packages = $packages;
        
        return $this;
    }
    
    public function getPackages()
    {
        return $this->packages;
    }
    
}