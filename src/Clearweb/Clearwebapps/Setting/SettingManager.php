<?php namespace Clearweb\Clearwebapps\Setting;

class SettingManager
{
    public static $packages = array();
    
    public static function get($key, $default = false)
    {
        $setting = Setting::where('key', $key)->first();
        
        if (empty($setting)) {
            $value = $default;
        } else {
            $value = $setting->value;
        }
        
        return $value;
    }
    
    public static function set($key, $value)
    {
        $setting = Setting::where('key', $key)->first();
		
        if (empty($setting)) {
            $setting = new Setting;
        }
        
        $setting->key   = $key;
        $setting->value = $value;					
        $setting->save();
    }
    
    public static function getPage()
    {
        return new SettingsPage;
    }
    
    public static function addPackage(SettingPackage $package)
    {
        static::$packages[$package->getName()] = $package;
    }
    
    public static function getPackages()
    {
        return static::$packages;
    }
    
    public static function getPackage($name)
    {
        $package = false;
        
        if (isset(static::$packages[$name]))
            $package = static::$packages[$name];
        
        return $package;
    }
}