<?php namespace Clearweb\Clearwebapps\Setting;

use Clearweb\Clearwebapps\Page\Page;
use Clearweb\Clearwebapps\Layout\FullWidthLayout;

use Clearweb\Clearwebapps\Setting\SettingsMenuWidget;

use Settings;
use App;

class SettingsPage extends Page
{
    public function __construct()
    {
        $this->setSlug('settings');
    }
    
    public function init()
    {
        $this->setLayout(new FullWidthLayout);
        
        $packageName = $this->getParameter('package', false);
        
        if (false === $packageName) {
            $this->setTitle(trans('clearwebapps::settings.settings'));
            
            $packages = Settings::getPackages();
            
            $menu = new SettingsMenuWidget;
            $menu->setPackages($packages);
            
            $this->addWidgetLinear($menu, 'content');
        } else {
            $package = Settings::getPackage($packageName);

            $this->setTitle($package->getTitle());
            
            if (empty($package)) {
                App::abort(404);
            } else {
                foreach($package->getWidgets() as $widget) {
                    $this->addWidgetLinear($widget, 'content');
                }
            }
        }
        
        parent::init();
        
        return $this;
    }
}