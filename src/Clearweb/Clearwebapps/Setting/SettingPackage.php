<?php namespace Clearweb\Clearwebapps\Setting;

use Clearweb\Clearworks\Widget\Widget;

use Settings;
use Clearworks;

class SettingPackage
{
    private $name = '';
    private $title = '';
    private $widgets = array();
    
    public function addWidget(Widget $widget)
    {
        $this->widgets[] = $widget;
        
        return $this;
    }
    
    public function getWidgets()
    {
        return $this->widgets;
    }
    
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }
    
    public function getTitle()
    {
        return $this->title;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }
    
    public function getName()
    {
        return $this->name;
    }
    
    public function getUrl()
    {
        return Clearworks::getPageUrl(Settings::getPage(), array('package'=>$this->getName()));
    }
}