<?php namespace Clearweb\Clearwebapps\Upload;

use Clearweb\Clearworks\Action\Action;

class UploadAction extends Action {
	private $file_path = '';
	private $file      = null;
	
	function init() {
        parent::init();
        
		$matches = array();
		preg_match('#^(.*?)(\.[0-9a-zA-Z]{2,10})?$#',$this->getParameter('filename', 'temp'), $matches);
		
		if (count($matches) <= 2) {
			$this->file_path = preg_replace("/[^0-9a-zA-Z]/", "_", $this->getParameter('filename', 'temp'));
		} else {
			$this->file_path = preg_replace("/[^0-9a-zA-Z]/", "_", $matches[1]).$matches[2];
		}
        
        return $this;
	}
	
	function execute() {
		$input = fopen('php://input', 'rb');
		$upload_handler = new UploadHandler;
		
		$this->file = $upload_handler->uploadFromStream($input, $this->file_path);
		
		fclose($input);
        
        return $this;
	}
	
	function getJSON() {
		return array(
					 'file_id'  =>$this->file->getId(),
					 'file_url' => $this->file->getUrl()
					 );
	}
}