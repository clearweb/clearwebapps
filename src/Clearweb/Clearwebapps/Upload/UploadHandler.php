<?php namespace Clearweb\Clearwebapps\Upload;

use \FileManager;

class UploadHandler implements IUploadHandler {
	function uploadFromStream($input_stream, $file_path)
	{
		$file = FileManager::generateNewFile($file_path);
		$file_pointer = fopen($file->getFullPath(), 'wb');
		
		stream_copy_to_stream($input_stream, $file_pointer);
		fclose($file_pointer);
		
		return $file;
	}

}