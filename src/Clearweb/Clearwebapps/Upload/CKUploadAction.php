<?php namespace Clearweb\Clearwebapps\Upload;

use Clearweb\Clearworks\Action\Action;

class CKUploadAction extends Action {
	function init() {return $this;}
	function getJSON() {return array();}
	
	function execute() {
		$funcNum = $_GET['CKEditorFuncNum'] ;
		$message = '';
		
		if (isset($_FILES['upload'])) {
			$name = $_FILES['upload']['name'];
			$input_stream = fopen($_FILES["upload"]["tmp_name"], 'r');
			$upload_handler = new UploadHandler;

			$file = $upload_handler->uploadFromStream($input_stream, $name);
			fclose($input_stream);
			
			$url = $file->getUrl();
		} else {
			$message = 'No file has been sent';
		}
		
		echo "<script type='text/javascript'> window.parent.CKEDITOR.tools.callFunction($funcNum, '$url', '$message')</script>";
		die;
	}
}
