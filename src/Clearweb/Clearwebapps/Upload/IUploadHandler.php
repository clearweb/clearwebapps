<?php namespace Clearweb\Clearwebapps\Upload;

interface IUploadHandler {
	/**
	 * Upload a file from input stream and return the uploaded file.
	 * @param stream $input_stream a stream to read upload from.
	 * @param $file_path the path (relative to file root) including the filename
	 */
	function uploadFromStream($input_stream, $file_path);
}