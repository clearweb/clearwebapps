<?php namespace Clearweb\Clearwebapps\Layout;

use Clearweb\Clearworks\Layout\LinearLayout;

class ThreeColumnsLayout extends Layout {
	
    function __construct()
    {
        parent::__construct();
        
        $this->addScript('sexy_sidebar', 'packages/clearweb/clearwebapps/js/sexy_sidebar.js', array('clearwebapps'))
            ->addStyle('css/3cols.css')
            ->addStyle('css/sexy_sidebar.css')
            ;
    }
    
	function loadContainers() {
		$this->containers = array('top', 'sidebar', 'col1', 'col2', 'col3');
		$this->containers_loaded = TRUE;
	}
    
	function getView() {
		return \View::make('clearwebapps::3cols')
			->with('stylesheets', $this->getStyles())
			->with('javascripts', $this->getScripts())
			->with('sidebar',     $this->getContainerView('sidebar'))
			->with('top',         $this->getContainerView('top'))
			->with('col1',        $this->getContainerView('col1'))
			->with('col2',        $this->getContainerView('col2'))
			->with('col3',        $this->getContainerView('col3'))
			;
	}
	
}