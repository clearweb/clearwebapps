<?php namespace Clearweb\Clearwebapps\Layout;

use Clearweb\Clearworks\Layout\LinearLayout;
use View;

abstract class Layout extends LinearLayout {
    public function __construct() {
        $this->addScript('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.js')
            ->addScript('jquery-ui', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js', array('jquery'))
            ->addScript('clearwebapps', 'packages/clearweb/clearwebapps/js/clearwebapps.js', array('jquery-ui'))
            ->addStyle('css/style.css')
            ->addStyle('https://ajax.aspnetcdn.com/ajax/jquery.ui/1.10.4/themes/blitzer/jquery-ui.min.css')
            ;
        
    }
    
    public function loadContainers()
    {
        $this->containers = array('sidebar', 'top');
        $this->containers_loaded = TRUE;
    }
    
    public function getWidgetView($widget)
    {
        if ($widget->getShouldWrap()) {
            return View::make('clearwebapps::widget')->with('widgetHTML', $widget->getView());
        } else {
            return $widget->getView();
        }
    }
}