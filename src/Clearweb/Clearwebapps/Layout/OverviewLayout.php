<?php namespace Clearweb\Clearwebapps\Layout;

class OverviewLayout extends Layout {
	
	function loadContainers() {
		$this->containers = array('top', 'sidebar', 'left', 'right');
		$this->containers_loaded = TRUE;
	}
	
	function getStyles() {
		$styles = parent::getStyles();
		$styles[] = 'css/overview.css';
		return $styles;
	}
	
	function getView() {
		return \View::make('clearwebapps::overview')
			->with('stylesheets', $this->getStyles())
			->with('javascripts', $this->getScripts())
			->with('top',         $this->getContainerView('top'))
			->with('sidebar',     $this->getContainerView('sidebar'))
			->with('left_info',   $this->getContainerView('left'))
			->with('right_info',  $this->getContainerView('right'))
			;
	}
	
}