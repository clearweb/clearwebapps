<?php namespace Clearweb\Clearwebapps\Layout;

class VerticalOverviewLayout extends Layout {

    function __construct()
    {
        parent::__construct();
        $this->addScript('jquery-ui', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js', array('jquery'))
            ->addScript('vertical-overview', 'packages/clearweb/clearwebapps/js/vertical_overview.js', array('jquery-ui'))
            ->addStyle('css/vertical_overview.css')
            ;
    }
    
	function loadContainers() {
		$this->containers = array('top', 'sidebar', 'middle', 'bottom');
		$this->containers_loaded = TRUE;
	}
    
	function getView() {
		return \View::make('clearwebapps::vertical_overview')
			->with('stylesheets', $this->getStyles())
			->with('javascripts', $this->getScripts())
			->with('top',         $this->getContainerView('top'))
			->with('sidebar',     $this->getContainerView('sidebar'))
			->with('middle',   $this->getContainerView('middle'))
			->with('bottom',  $this->getContainerView('bottom'))
			;
	}
	
}