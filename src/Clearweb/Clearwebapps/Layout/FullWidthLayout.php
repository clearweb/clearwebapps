<?php namespace Clearweb\Clearwebapps\Layout;

class FullWidthLayout extends Layout
{
	public function loadContainers()
	{
		$this->containers = array('top', 'sidebar', 'content');
		$this->containers_loaded = TRUE;
	}
	
	function getView() {
		return \View::make('clearwebapps::full_width')
			->with('stylesheets', $this->getStyles())
			->with('javascripts', $this->getScripts())
			->with('sidebar',     $this->getContainerView('sidebar'))
			->with('top',         $this->getContainerView('top'))
			->with('content',     $this->getContainerView('content'))
			;
	}
}