<?php namespace Clearweb\Clearwebapps\Layout;

use Clearweb\Clearworks\Layout\LinearLayout;

class DashboardLayout extends Layout {
	
	function loadContainers() {
		$this->containers = array('sidebar', 'left_info', 'right_info');
		$this->containers_loaded = TRUE;
	}
	
	function getStyles() {
		$styles = parent::getStyles();
		$styles[] = 'css/dashboard.css';
		return $styles;
	}
	
	function getView() {
		return \View::make('clearwebapps::dashboard')
			->with('stylesheets', $this->getStyles())
			->with('javascripts', $this->getScripts())
			->with('sidebar',     $this->getContainerView('sidebar'))
			->with('left_info',   $this->getContainerView('left_info'))
			->with('right_info',  $this->getContainerView('right_info'))
			;
	}
	
}