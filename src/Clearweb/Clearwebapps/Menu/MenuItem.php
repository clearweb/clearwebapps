<?php namespace Clearweb\Clearwebapps\Menu;

use Clearweb\Clearworks\Asset\AssetBag;

use Clearweb\Clearworks\Traits\AssetsHolder;

use View;
use Clearworks;

class MenuItem implements IMenuItem
{
    use AssetsHolder;

    private $id = '';
    private $children = [];

    private $label = '';
    private $url = '';
    private $baseUrl = '';
    
    public function getLabel()
    {
        return $this->label;
    }

    public function setLabel($label)
    {
        $this->label = $label;
        
        return $this;
    }
    
    public function getUrl()
    {
        return $this->url;
    }

    public function setUrl($url)
    {
        $this->url = $url;
        
        return $this;
    }

    public function getBaseUrl()
    {
        return $this->baseUrl;
    }

    public function setBaseUrl($baseUrl)
    {
        $this->baseUrl = $baseUrl;
        
        return $this;
    }

    public function getChildren()
    {
        return $this->children;
    }

    public function setChildren($children)
    {
        $this->children = $children;
        
        return $this;
    }
    
    public function addChild(IMenuItem $child)
    {
        $this->children[] = $child;
    }
    
    public function getView()
    {
        $active = false;
        
        if (preg_match('#([^/]+/)*([^/]+)#', $this->getUrl(), $matches)) {
            $linkClass = $matches[2];
        } else {
            $linkClass = $this->getUrl();
        }
        $menuClass = str_replace(' ','-', strtolower( $linkClass )); 
        
        $page = Clearworks::getCurrentPage();
        if ($page) {
            $url = Clearworks::getPageUrl(Clearworks::getCurrentPage(), []);
        } else {
            $url = Request::url();
        }
        
        if ($url == $this->getBaseUrl()) {
            $menuClass .= ' selected';
            $active     = true;
        }
        
        
        return View::make('clearwebapps::menu-item')
            ->with('label', $this->getLabel())
            ->with('url', $this->getUrl())
            ->with('class', $menuClass)
            ->with('active', $active)
            ->with('children', $this->getChildren())
            ;
    }
}