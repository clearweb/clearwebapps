<?php namespace Clearweb\Clearwebapps\Menu;

use \Clearweb\Clearwebapps\Widget\VerticalMenuWidget;

class MenuHandler
{
    private $menus = [];
    
    public function __construct()
    {
        $menu = new VerticalMenuWidget;
        $this->add('admin-sidebar', $menu);
    } 

    public function all()
    {
        return $this->menus;
    }
    
    public function exists($menuName)
    {
        return isset($this->menus[$menuName]);
    }
    
    public function get($menuName)
    {
        if ($this->exists($menuName)) {
            return $this->menus[$menuName];
        } else {
            throw new MenuDoesNotExistsException("The menu '$menuName' does not exist");
        }
    }
    
    public function add($menuName, IMenu $menu)
    {
        $this->menus[$menuName] = $menu;
        
        return $this;
    }
    
    /**
     * Adds a link to the admin menu.
     *
     * For backwards compatibility purposes
     */
	function addLink($name, $url, $base_url = false) {
        $this->get('admin-sidebar')->addLink($name, $url, $base_url);
    }
    
}