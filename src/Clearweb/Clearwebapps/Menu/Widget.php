<?php namespace Clearweb\Clearwebapps\Menu;

use Clearweb\Clearwebapps\Widget\ViewWidget;

class Widget extends ViewWidget implements IMenu {
    //private $links = [];
    private $items = [];
    private $title = '';
    private $mobile = false;
    private $mobileOptions = ['navbars'=>true];
	private $mobileConfig = ['clone' => true];
    
	function init() {
		parent::init();
		$this->setName('menu')
            ->setShouldWrap(false)
            ->addScript('mmenu', 'https://cdnjs.cloudflare.com/ajax/libs/jQuery.mmenu/5.3.4/js/jquery.mmenu.min.all.min.js', ['jquery'])
            ->addStyle('https://cdnjs.cloudflare.com/ajax/libs/jQuery.mmenu/5.3.4/css/jquery.mmenu.all.min.css')
            ;
	}

	/**
	 * Adds a link to the navigation
	 */
	function add(IMenuItem $item, $position = 0) {
        if ($position > 0) {
			$this->items = array_merge(array_slice($this->items, 0, $position-1), array($item), array_slice($this->items, $position-1));
        } else {
            $this->items[] = $item;
        }
        
		return $this;
	}
    
	/**
	 * Set the menu items.
	 * @param array $items the items
	 * @return $this;
	 */
	function setItems(array $items) {
		$this->items = $items;
		
		return $this;
	}
	
    /**
     * Gets the menu items.
     * @return array with items
     */
	function getItems() {
		return $this->items;
	}
    
    public function setMobile($mobile, $options = false, $config = false)
    {
        $this->mobile = $mobile;

        if ($options) {
            $this->mobileOptions = $options;
        }

        if ($config) {
            $this->mobileConfig = $config;
        }
        
        return $this;
    }

    public function getMobile()
    {
        return $this->mobile;
    }
    
	function execute() {
		parent::execute();
		
		$items = $this->getItems();
        
        $this->setViewName('clearwebapps::menu')
			->with('items', $items)
            ->with('id', $this->getId())
            ->with('mobile', $this->getMobile())
            ->with('mobileOptions', $this->mobileOptions)
            ->with('mobileConfig', $this->mobileConfig)
            ;
	}
}