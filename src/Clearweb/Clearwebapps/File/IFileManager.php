<?php namespace Clearweb\Clearwebapps\File;

interface IFileManager {
	static function getFile($id);
	static function getFileFromPath($path);
}