<?php namespace Clearweb\Clearwebapps\File;

use Clearweb\Clearworks\Environment;

class File {
	private $id;
	
	/* - hard coded functions, maybe put them into config - */
	
	function getFilesDir() {
 		$files_dir = base_path().DIRECTORY_SEPARATOR.'files';
		
		if ( ! is_dir($files_dir)) {
			mkdir($files_dir);
		}
		
		return $files_dir;
	}
	
	
	/* - deducable function. maybe make setters for them and deduce them on construction - */
	
	function exists() {
		return file_exists($this->getFullPath());
	}
	
	function getUrl(Environment $env=null) {
		return \Clearworks::getPageUrl(new ShowFilePage, array('file'=>$this->getId()), $env);
	}
	
	function getName() {
		return basename($this->getFullPath());
	}
	
	function getExtension() {
		return pathinfo($this->getFullPath(), PATHINFO_EXTENSION);
	}
	
	function getFullPath() {
		return $this->getFilesDir().DIRECTORY_SEPARATOR.$this->getPath();
	}
	
	function getMimeType() {
		$finfo = finfo_open(FILEINFO_MIME_TYPE);
		
		return finfo_file($finfo, $this->getFullPath());
	}
	
	function getSize() {
		return filesize($this->getFullPath());
	}
	


	

	/* - getters and setters - */
	
	
	/**
	 * Sets the path of the file
	 * @todo checks if path is not valid (security checks)
	 */
	function setPath($path) {
		$this->path = $path;
		
		return $this;
	}

	function getPath() {
		return $this->path;
	}
	
	/**
	 * sets the id of the file.
	 */
	function setId($id) {
		$this->id = $id;
		return $this;
	}
	
	function getId() {
		return $this->id;
	}
}