<?php namespace Clearweb\Clearwebapps\File;

class FileManager implements IFileManager {
	static function getFile($id)
	{
		$path = base64_decode($id);
		
		return with(new File)
			->setId($id)
			->setPath($path)
			;
	}
	
	static function getFileFromPath($path)
	{
		$id = base64_encode($path);
		
		return with(new File)
			->setId($id)
			->setPath($path)
			;
	}
	
	/**
	 * generates info for a new file from path.
	 */
	static function generateNewFile($path) {
		$file = static::getFileFromPath($path);
		
		$i = 1;
		while($file->exists()) {
			$matches = array();
			preg_match('#^(.+?)(\.[0-9a-zA-Z]{2,10})?$#', $path, $matches);
			
			$path_without_extension = $matches[1];
			$extension = $matches[2];
			$new_path = $path_without_extension . "_$i" .$extension;
			
			$file = static::getFileFromPath($new_path);
			$i++;
		}
		
		return $file;
	}
}