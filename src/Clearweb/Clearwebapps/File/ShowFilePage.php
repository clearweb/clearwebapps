<?php namespace Clearweb\Clearwebapps\File; 

use \Clearweb\Clearworks\Page\SlugPage;

class ShowFilePage extends SlugPage {
	function getSlug() {
		return 'files';
	}
	
	function getView() {
		$file_id = $this->getParameter('file', false);
		if (false === $file_id) {
			$html = 'no file';
		} else {
            header("Cache-Control: private, max-age=10800, pre-check=10800");
            header("Pragma: private");
            header("Expires: " . gmdate('D, d M Y H:i:s', strtotime(" 2 day"))).' GMT';
            
            $file = FileManager::getFile($file_id);
            header('Content-Disposition: filename="'.$file->getName().'"');
            
            $eTag        = md5(filemtime($file->getFullPath()).$file->getFullPath());
            $lastModTime = filemtime($file->getFullPath());
            
            if (
                (
                 isset($_SERVER['HTTP_IF_MODIFIED_SINCE'])
                 &&
                 strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE']) == $lastModTime
                )
                ||
                (
                 isset($_SERVER['HTTP_IF_NONE_MATCH'])
                 &&
                 substr($_SERVER['HTTP_IF_NONE_MATCH'], 0, 32) == $etag
                )
            ) {
                // send the last mod time of the file back
                header('HTTP/1.1 304 Not Modified');
                exit;
            }

            header("Content-type: ".$file->getMimeType());
            header("Last-Modified: ".gmdate("D, d M Y H:i:s", $lastModTime)." GMT");
            header('ETag: '.$eTag);
            header("Content-Length: " . filesize($file->getFullPath()));
            
            $html = file_get_contents($file->getFullPath());
        }

        die($html);
        
		return $html;
	}
	
	function getStyles() {
		return array();
	}
	
	function getScripts() {
		return array();
	}
}