<?php namespace Clearweb\Clearwebapps\User;

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends \Eloquent implements UserInterface, RemindableInterface {

	protected $fillable = array('image', 'email', 'name');
	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password');

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}

	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail()
	{
		return $this->email;
	}
	
	public function getRememberToken()
	{
		return $this->remember_token;
	}

	public function setRememberToken($value)
	{
		$this->remember_token = $value;
	}

	public function getRememberTokenName()
	{
		return 'remember_token';
	}
    
    public function associatedRoles() {
        return $this->hasMany('\Clearweb\Clearwebapps\User\AssociatedRole');
    }
    
    /**
     * checks if a user has a specific role
     * @param string $role the role name
     * @return boolean <code>true</code> if the user has the role, otherwise <code>false</code>
     */
    public function hasRole($role) {
        $role_found = false;
        
        foreach($this->associatedRoles as $associatedRole) {
            if ($associatedRole->role == $role) {
                $role_found = true;
            }
        }
        
        return $role_found;
    }
    
    public function addRole($role) {
        if ( ! $this->hasRole($role)) {
            $user_role = new AssociatedRole;
            $user_role->user_id = $this->id;
            $user_role->role = $role;
            
            $user_role->save();
        }
    }

    public function removeRole($role) {
        if ($this->hasRole($role)) {
            foreach($this->associatedRoles as $role) {
                if ($role->role == $role) {
                    $role->delete();
                }
            }
        }
    }
}