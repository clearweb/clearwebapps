<?php namespace Clearweb\Clearwebapps\User;

use Clearweb\Clearwebapps\Eloquent\ListWidget;
use Clearweb\Clearwebapps\Eloquent\DeleteActionLink;

use Config;

class UserListWidget extends ListWidget {

	function init() {
        $this->setLangPrefix('clearwebapps::user');
        
        parent::init();
        
		$this->getList()
            ->removeColumn('remember_token')
            ->removeColumn('image')
            ->addActionLink(
                            with(new DeleteActionLink)
                            ->setModelClass(Config::get('auth.model'))
                            ->setDeleteAction(new DeleteAction)
                            )
            ;
		return $this;
	}
}