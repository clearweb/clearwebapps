<?php namespace Clearweb\Clearwebapps\User;

use Clearweb\Clearwebapps\Eloquent\FormWidget;
use Clearweb\Clearwebapps\Form\FieldFactory;
use Clearweb\Clearwebapps\Form\ImageUploadField;

class UserFormWidget extends FormWidget {
	function getName() {
		return 'user_form_widget';
	}
	
	function init() {
        $this->setLangPrefix('clearwebapps::user');

		parent::init();
        
		$this->getForm()
            ->removeField('remember_token')
            ->replaceField(
                           'image',
                           (new ImageUploadField)
                           ->setName('image')
                           )
            ->moveField('image', 1)
            ->addField(
                       FieldFactory::make('new_password', 'password')
                       ->setDescription(trans('clearwebapps::user.password-desc')),
                       5
                       )
            ;
	}
	
	function submit(array $post) {
		$new_password = $post['new_password'];
		unset($post['new_password']);
		$id = parent::submit($post);
		$user = User::find($id);
		
		if ( ! empty($new_password)) {
			$user->password = \Hash::make($new_password);
			$user->save();
		}
		
		return $id;
	}

}