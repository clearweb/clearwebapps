<?php namespace Clearweb\Clearwebapps\User;

use Clearweb\Clearwebapps\Page\Page;
use Clearweb\Clearwebapps\Layout\FullWidthLayout;

use Auth;
use Event;

class Account extends Page
{
    function __construct() {
		$this->setSlug('account');
	}

    function init() {
        $this->setLayout(new FullWidthLayout)
            ->setParameter('user', Auth::user()->id)
            ->setTitle(trans('clearwebapps::user.account'))
            ->addWidgetLinear(new UserFormWidget, 'content')
            ;
        
        Event::fire('cw-account');
        
        return parent::init();
    }
}