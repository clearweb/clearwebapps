<?php namespace Clearweb\Clearwebapps\User;

class AssociatedRole extends \Eloquent {
    protected $table = 'user_roles';
    protected $fillable = ['user_id'];
}