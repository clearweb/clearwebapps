<?php namespace Clearweb\Clearwebapps\User;

use Illuminate\Support\ServiceProvider;
use Event;
use Menu;
use Clearworks;
use Auth;

class UserServiceProvider  extends ServiceProvider {
    	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
        
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
        Event::listen('env.init', function () {
            if (Auth::check() && Clearworks::getCurrentEnvironment()->getName() == 'admin') {
                $env = Clearworks::getCurrentEnvironment();
                
                if (Auth::user()->hasRole('users')) {
                    Menu::addLink(trans_choice('clearwebapps::user.user', 2), Clearworks::getPageUrl(new UserManagementPage));
                    $env->addPage(new UserManagementPage);
                }
                
                $sidebar = Menu::get('admin-sidebar');
                $sidebar->add(new AccountInfo, 1);
                
                $env->addPage(new Account);
            }
        });
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array();
	}

}