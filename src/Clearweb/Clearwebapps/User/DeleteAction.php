<?php namespace Clearweb\Clearwebapps\User;

use Config;

class DeleteAction extends \Clearweb\Clearwebapps\Eloquent\DeleteAction
{
    public function init()
    {
        $this->setModelClass(Config::get('auth.model'));
        
        return parent::init();
    }
}