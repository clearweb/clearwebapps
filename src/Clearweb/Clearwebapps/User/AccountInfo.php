<?php namespace Clearweb\Clearwebapps\User;

use Clearweb\Clearwebapps\Menu\IMenuItem;

use Clearweb\Clearworks\Traits\AssetsHolder;

use View;
use Auth;
use FileManager;

class AccountInfo implements IMenuItem
{
    use AssetsHolder;
    
    public function getView()
    {
        $profileImage = null;

        if(Auth::user()->image) {
            $file = FileManager::getFile(Auth::user()->image);
            $profileImage = $file->getUrl();
        }
        
        return View::make('clearwebapps::user.menu-item')
            ->with('user', Auth::user())
            ->with('profileImage', $profileImage)
            ;
    }
}