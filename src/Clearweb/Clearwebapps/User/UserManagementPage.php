<?php namespace Clearweb\Clearwebapps\User;

use Clearweb\Clearworks\Page\WidgetLayoutPage;
use Clearweb\Clearworks\Page\WidgetLocation;
use Clearweb\Clearworks\Layout\Location\LinearLayoutLocation;

use Clearweb\Clearwebapps\Page\Page;
use Clearweb\Clearwebapps\Layout\OverviewLayout;

use Config;
use Clearweb\Clearwebapps\Eloquent\NewButton;

class UserManagementPage extends Page {
	function __construct() {
		$this->setSlug('users');
	}
	
	function init() {
        $this->setLayout(new OverviewLayout)
             ->addWidgetLinear(new UserListWidget, 'left')
             ->addWidgetLinear(new UserFormWidget, 'right')
             ->setTitle(ucfirst(trans_choice('clearwebapps::user.user', 2)))
            ->addAction('new', with(new NewButton)->setModelClass(Config::get('auth.model'))->setLangPrefix('clearwebapps::user'))
            ;
		
		return parent::init();
	}
}