<?php namespace Clearweb\Clearwebapps\Widget;

use Clearweb\Clearworks\Widget\ContainerWidget;
use Clearweb\Clearworks\Contracts\IParametrizable;

use Clearweb\Clearwebapps\Widget\Exception\NoViewSetException;

use Clearweb\Clearworks\Content\RawHTML;
use View;

class ViewWidget extends HTMLWidget
{
    private $viewName   = '';
    private $viewParams = array();
    
    public function getView() {
        if ($this->getViewName() == '') {
            throw new NoViewSetException("ViewWidget {$this->getName()} has no view set.");
        }
        
        $this->setHTML(View::make($this->getViewName(), $this->getViewParameters()));
        
        return parent::getView();
    }

    public function setViewName($viewName) {
		$this->viewName = $viewName;
        
        return $this;
	}
	
	public function getViewName() {
        return $this->viewName;
	}
    
    public function with($key, $value) {
		$this->viewParams[$key] = $value;
        
        return $this;
	}
    
    public function getViewParameters() {
        return $this->viewParams;
    }
}