<?php namespace Clearweb\Clearwebapps\Widget;

use Clearweb\Clearwebapps\Form\Form;
use Clearweb\Clearwebapps\Form\Validator;

use Clearweb\Clearwebapps\Content\Notice;
use Clearweb\Clearwebapps\Content\Error;

use Clearweb\Clearwebapps\Action\ScrollToId;

use Clearweb\Clearwebapps\Widget\Exception\NoFormException;
use Clearweb\Clearwebapps\Widget\Exception\NoValidatorException;

abstract class FormWidget extends Widget
{
	private $form      = null;
	private $validator = null;
	private $submitted = false;
    private $action    = '';
    private $submitted_notice = '';
	
	/**
	 * Sets the form state to submitted.
	 */
	public function setSubmitted($submitted = TRUE) {
		$this->submitted = $submitted;
	}
	
	/**
	 * Checks if the form is submitted.
	 */
	public function getSubmitted() {
		return $this->submitted;
	}

	/**
	 * Set submitted notice text
	 */
	public function setSubmittedNotice($notice) {
		$this->submitted_notice = $notice;
	}

	/**
	 * Get submitted notice text
	 */	
	public function getSubmittedNotice() {
		return $this->submitted_notice;
	}
	
	public function getForm()
	{
		return $this->form;
	}
	
	public function setForm(Form $form)
	{
		$this->form = $form;
		return $this;
	}
	
	public function getValidator() {
		return $this->validator;
	}
	
	public function setValidator(Validator $validator) {
		$this->validator = $validator;
		return $this;
	}

    public function getAction() {
        return $this->action;
    }
    
    public function setAction($action) {
        $this->action = $action;
        return $this;
    }
    
	abstract public function submit(array $post);
	
	public function addErrors(array $errors) 
	{
		$error_html = '';
		
		foreach($errors as $field => $field_errors) {
			foreach($field_errors as $error) {
				if ($field == 'form_general') {
					$error_html .= "$error<br />";
				} else {
					$error_html .= trans($error, array('attribute'=>strtolower($this->getForm()->getField($field)->getLabel())))."<br />";
				}
			}
		}
		
		$this->addViewable(new Error($error_html));
	}
	
	/**
	 * @pre has form set with setForm
	 * @pre has validator set with setValidator
	 */
	public function execute() {
		parent::execute();
		if (is_null($this->getForm())) {
			throw new NoFormException('The widget "'.$this->getName().'" has no form set. Please set one in the init function.');
		}
		
		if (is_null($this->getValidator())) {
			throw new NoValidatorException('The widget "'.$this->getName().'" has no validator set. Please set one in the init function.');
		}
        
        $action = $this->getAction();
        
        if (empty($action)) {
            $action = \Clearworks::getWidgetUrl($this);
        }
        
		$this->getForm()
			->setAction($action)
            ->setParameters($this->getParameters())
			->init()
			->execute()
			;
		
		$errors = array();
		
		$this->getContainer()->addClass('widget-form-container');
		
		$validator = $this->getValidator();
		$validator->setData($_POST);
		
		if ( ! empty($_POST)) {
			if ($validator->passes()) {
				$this->submit($_POST);
				$this->setSubmitted(true);
				
				$notice = $this->getSubmittedNotice();
				if ($notice) {
					$this->addViewable(new Notice($notice), 'submitted_notice')
						->addViewable(new ScrollToId('widget-'.$this->getID()))
						;
				}
			} else {
				$this->getForm()->setValues($_POST);
				$errors = $validator->errors();
				$errors_array = $errors->getMessages();
				$this->addErrors($errors_array);
				$this->addViewable(new ScrollToId('widget-'.$this->getID()))
					;
			}
		}
        
		$this->addViewable($this->getForm(), 'form');
		
		$this->addStyle('css/form_widget.css');
		$this->addScript('form_widget', 'packages/clearweb/clearwebapps/js/form_widget.js', array('jquery'));
		
		return $this;
	}
}