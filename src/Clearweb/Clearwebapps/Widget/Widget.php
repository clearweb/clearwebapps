<?php namespace Clearweb\Clearwebapps\Widget;

use Clearweb\Clearworks\Widget\CommunicatingWidget;
use Clearweb\Clearworks\Content\Container;
use Clearweb\Clearwebapps\Content\WidgetHeader;

use Clearweb\Clearworks\Contracts\IViewable;

use Clearweb\Clearworks\Action\IActionButton;

use Event;

class Widget extends CommunicatingWidget
{
    private $actions         = array();
	private $title           = '';
    
    private $bodyContainer   = null;
    private $actionContainer = null;
    private $headContainer   = null;
    private $footContainer   = null;

    public function __construct() {
        parent::__construct();
        
        $this->setHeadContainer(new Container)
            ->setActionContainer(new Container)
            ->setBodyContainer(new Container)
            ->setFootContainer(new Container)
            ;
    }
    
    public function execute()
    {
        parent::execute();
        
        if ($this->getTitle()) {
            $this->getHeadContainer()->addViewable(new WidgetHeader($this->getTitle()));
		}

        if ($this->hasActions()) {
            $this->getBodyContainer()->addViewable($this->getActionContainer(), 'widget-actions', 1);
            
            foreach($this->getActions() as $actionData) {
                $action = $actionData['action'];
                $action->setParameters($this->getParameters());
                $action->init();
                $action->execute();
                $this->getActionContainer()->addViewable($action);
            }
            
            $this->getActionContainer()->addClass('widget-actions')->addClass('operations');
        }
        
        $this->getContainer()
            ->addViewable($this->getHeadContainer(), 'widget-head')
            ->addViewable($this->getBodyContainer(), 'widget-body')
            ->addViewable($this->getFootContainer(), 'widget-foot')
            ;
        
        
        $this->getHeadContainer()->addClass('widget-head');
        $this->getBodyContainer()->addClass('widget-body');
        $this->getFootContainer()->addClass('widget-foot');
        
        
    }
    
    public function getView()
    {
        Event::fire('clearwebapps.widget_theme', array('widget'=>$this));
        
        return parent::getView();
    }
    
	/**
	 * Gets the optional title of the widget
	 */
	public function getTitle()
	{
		return $this->title;
	}

	/**
	 * Sets the optional title of the widget
	 * @param string $title the title to set
	 */
	public function setTitle($title)
	{
		$this->title = $title;
		
		return $this;
	}
    
    public function getHeadContainer()
    {
        return $this->headContainer;
    }

    public function setHeadContainer(Container $container)
    {
        $this->headContainer = $container;
        return $this;
    }
    
    public function getActionContainer()
    {
        return $this->actionContainer;
    }

    public function setActionContainer(Container $container)
    {
        $this->actionContainer = $container;
        return $this;
    }

    public function getBodyContainer()
    {
        return $this->bodyContainer;
    }

    public function setBodyContainer(Container $container)
    {
        $this->bodyContainer = $container;
        return $this;
    }

    public function getFootContainer()
    {
        return $this->footContainer;
    }

    public function setFootContainer(Container $container)
    {
        $this->footContainer = $container;
        return $this;
    }
	
    /**
     * Override method to make it add viewables to body container
     */
    public function addViewable(IViewable $viewable, $name=null, $position=0) {
        $this->getBodyContainer()
            ->addViewable($viewable, $name, $position)
            ;
        
        return $this;
    }
    
    
    /**
     * returns actions of the widget
	 * @return Array with elements of \Clearweb\Clearworks\Action\IActionButton
	 */
	public function getActions() {
		return $this->actions;
	}
    
    /**
     * Returns if the widget has some actions or not
     * @return boolean
     */
    public function hasActions() {
        return ( ! empty($this->actions));
    }
    
    /**
	 * Sets actions of the widget
	 * @param Array $actions with elements of \Clearweb\Clearworks\Action\IActionButton
     * @retun $this
	 */
	public function setActions(array $actions) {
		$this->actions = $actions;
        
        return $this;
	}
    
    /**
     * Adds an action to the widget actions
     * @param \Clearweb\Clearworks\Action\IActionButton $action the action
     * @return $this
     */
    public function addAction($name, IActionButton $action, $position=0) {
        $actionArray = array('name' => $name, 'action' => $action);
        
        if ($position > 0) {
            $this->actions = array_merge(array_slice($this->actions, 0, $position-1), array($actionArray), array_slice($this->actions, $position-1));
        } else {
            $this->actions[] = $actionArray;
        }
    }
    
    /**
     * Removes a named action link from the links
     * @param string $name the name of the link
     * @return $this
     */
    function removeAction($name) {
        foreach($this->actions as $i => $action) {
            if ($action['name'] == $name) {
                unset($this->actions[$i]);
            }
        }
        
        return $this;
    }
}