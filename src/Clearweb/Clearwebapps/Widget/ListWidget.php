<?php namespace Clearweb\Clearwebapps\Widget;

use \Clearweb\Clearwebapps\Lists\ListObject;

abstract class ListWidget extends Widget {
	private $list = null;
    private $show_new_button = false;
    private $lang_prefix = 'app';
    
	/**
	 * Sets the list for the widget.
	 * @param \Clearweb\Clearwebapps\Lists\ListObject $list the list to set
	 * @return $this
	 */
	public function setList(ListObject $list) {
		$this->list = $list;
		return $this;
	}
	
	/**
	 * Gets the list
	 * @return mixed the list as \Clearweb\Clearwebapps\Lists\ListObject or <code>NULL</code>
	 */
	public function getList() {
		return $this->list;
	}
	
	public function init() {
        if (is_null($this->getList())) {
            $this->setList(new ListObject);
        }
        
		parent::init();
		$this->addStyle('css/list_widget.css');
		$this->getList()->init();
		
        $this->addClass('list-widget');
        
		return $this;
	}
	
    /**
     * Sets the prefix for translation of the columns trans([prefix].[colname]);
     * @param string $lang_prefix the prefix
     */
    public function setLangPrefix($lang_prefix) {
		$this->lang_prefix = $lang_prefix;
		
		return $this;
	}
    
    /**
     * gets the prefix for translation of the columns trans([prefix].[colname]);
     * @return string the prefix
     */
	public function getLangPrefix() {
		return $this->lang_prefix;
	}
    
    /**
     * Gets if to show the add new item button to the widget or not.
     * @return boolean value <code>true</code> if we will show, otherwise <code>false</code>
     */
    public function getShowNewButton() {
        return $this->show_new_button;
    }
    
    /**
     * Sets if to show the add new item button to the widget or not.
     * @param boolean $show_new_button to show or not to show the new button
     * @return this for chaining
     */
    public function setShowNewButton($show_new_button) {
        $this->show_new_button = $show_new_button;
        
        return $this;
    }
    
	public function execute() {
		parent::execute();
		$this->getList()->execute();
		
        
        if ($this->getShowNewButton()) {
            $new_button = new NewListItemButton($this->getList()->getListItemName());
            $new_button->init();
            $new_button->execute();
            
            $this->getHeadContainer()->addViewable($new_button, 'new-button', 1);
        }
        
		$this->addViewable($this->getList());
		
        $this->getList()->setLangPrefix($this->getLangPrefix());
        
		return $this;
	}
}