<?php namespace Clearweb\Clearwebapps\Widget;

use Clearweb\Clearworks\Content\RawHTML;

use Clearweb\Clearwebapps\Menu\Widget;
use Clearweb\Clearwebapps\Menu\MenuItem;

class VerticalMenuWidget extends Widget {
	function init() {
		parent::init();
		$this->addStyle('css/vertical_menu.css')
            ->setShouldWrap(false)
            ->setName('navigation')
            ;
	}
	
	/**
	 * Adds a link to the navigation
	 */
	function addLink($name, $url, $base_url = false) {
		//$this->links[] = array('name'=>$name, 'baseurl'=>(empty($base_url)?$url:$base_url), 'url'=>$url);
        $item = (new MenuItem)
            ->setLabel(ucfirst($name))
            ->setUrl($url)
            ->setBaseUrl(empty($base_url)?$url:$base_url)
            ;
        
        $this->add($item);
        
		return $this;
	}
	
	/**
	 * Set the navigation links.
	 * @param array $links the links in following format: array( array('name'=>[link text], 'url'=>[link url]) )
	 * @return $this;
	function setLinks(array $links) {
		$this->links = $links;
		
		return $this;
	}
	
	function getLinks() {
		return $this->links;
	}
    
	function execute() {
		parent::execute();
		
		$links = $this->getLinks();
		
		foreach($links as &$link) {
			$link['selected'] = ($link['baseurl'] == \Clearworks::getPageUrl(\Clearworks::getCurrentPage(), array()));
		}
		
        $this->with('links', $links)
            ->with('id', $this->getId())
            ;
	}
    
    protected function recursiveCheckSelected(&$links){}
    */
}