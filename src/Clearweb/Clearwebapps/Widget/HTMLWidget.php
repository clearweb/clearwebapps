<?php namespace Clearweb\Clearwebapps\Widget;

use Clearweb\Clearworks\Widget\CommunicatingWidget;
use Clearweb\Clearworks\Contracts\IParametrizable;

use Clearweb\Clearwebapps\Widget\Exception\NoViewSetException;

use Clearweb\Clearworks\Content\RawHTML;
use View;

class HTMLWidget extends CommunicatingWidget // ContainerWidget
{
    private $html   = '';
    private $title = '';    
    
    public function init()
    {
        $this->setShouldWrap(false);
        return parent::init();
    }
    
    public function execute()
    {
        parent::execute();
	
        $title = $this->getTitle();
	
        if ( ! empty($title)) {
            $this->addViewable((new RawHTML)->setHTML("<h3>{$this->getTitle()}</h3>"));
        }
        
        return $this;
    }

    public function getHTML()
    {
        return $this->html;
    }

    public function setHTML($html)
    {
        $this->html = $html;

        return $this;
    }
    
    public function getView() {
        $this->addViewable(with(new RawHTML)->setHTML($this->getHTML()));
        
        return parent::getView();
    }
    
    public function getTitle() {
        return $this->title;
    }
    
    public function setTitle($title)
    {
        $this->title = $title;
        
        return $this;
    }
}