<?php namespace Clearweb\Clearwebapps\Widget;

use Clearweb\Clearwebapps\Form\Form;
use Clearweb\Clearwebapps\Form\FieldFactory;
use Clearweb\Clearwebapps\Form\Validator;
use Clearweb\Clearworks\Communication\ParameterChanger;

class SearchWidget extends FormWidget {
	function getName() {
		return 'search';
	}
	
	public function init() {
		parent::init();
		
		$this->setForm(
					   with(new Form)
					   ->addField(FieldFactory::make('search', 'text')->setLabel(trans('clearwebapps::search_widget.search'))->setPlaceholder(trans('clearwebapps::search_widget.search')))
					   );
		
		$this->setValidator(
							with(new Validator)
							->setRules(array())
							);
		
        $this->addClass('search-widget');
        
		return $this;
	}
	
	public function execute() {
		$this->getForm()->setValue('search', $this->getParameter('search', ''));
		parent::execute();
		
		if ($this->getContainer()->hasViewable('submitted_notice')) {
			$this->getContainer()->removeViewable('submitted_notice');
		}
	}
	
	
	public function submit(array $post) {
		if (isset($post['search'])) {
			$this->getContainer()
				->addViewable(
							  with(new ParameterChanger)
							  ->setParameter('search', $post['search'])
							  )
				;
			
			$this->getForm()->setValue('search', $post['search']);
		}
		
		return 0;
	}
}