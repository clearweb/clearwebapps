<?php namespace Clearweb\Clearwebapps\Widget;

use Clearweb\Clearworks\Content\RawHTML;
use Clearweb\Clearwebapps\Menu\IMenu;
use Clearworks;
use Request;

class MenuWidget extends ViewWidget implements IMenu {
	private $links = [];
    private $title = '';
    private $mobile = false;
    private $mobileOptions = ['navbars'=>true];
	private $mobileConfig = ['clone' => true];
    
	function init() {
		parent::init();
		$this->setName('menu')
            ->setShouldWrap(false)
            ->addScript('mmenu', 'https://cdnjs.cloudflare.com/ajax/libs/jQuery.mmenu/5.3.4/js/jquery.mmenu.min.all.min.js', ['jquery'])
            ->addStyle('https://cdnjs.cloudflare.com/ajax/libs/jQuery.mmenu/5.3.4/css/jquery.mmenu.all.min.css')
            ;
	}

	/**
	 * Adds a link to the navigation
	 */
	function add($id, $label, $url, $parentId = null, $baseUrl = false) {
        $link = [
                 'id' => $id,
                 'label' => $label,
                 'baseurl' => (empty($base_url)?$url:$base_url),
                 'url' => $url,
                 'children' => [],
                 ];
        
        if ($parentId) {
            $this->recursiveAddChildToParentId($link, $this->links, $parentId);
        } else {
            $this->links[] = $link;
        }
        
		return $this;
	}
    
    private function recursiveAddChildToParentId($link, &$links, $parentId)
    {
        foreach($links as &$currentParent) {
            if ($currentParent['id'] == $parentId) {
                $currentParent['children'][] = $link;
            } else {
                $this->recursiveAddChildToParentId($link, $currentParent['children'], $currentParent['id']);
            }
        }
    }
    
	/**
	 * Set the navigation links.
	 * @param array $links the links in following format: array( array('name'=>[link text], 'url'=>[link url]) )
	 * @return $this;
	 */
	function setLinks(array $links) {
		$this->links = $links;
		
		return $this;
	}
	
	function getLinks() {
		return $this->links;
	}
    
    public function setMobile($mobile, $options = false, $config = false)
    {
        $this->mobile = $mobile;

        if ($options) {
            $this->mobileOptions = $options;
        }

        if ($config) {
            $this->mobileConfig = $config;
        }
        
        return $this;
    }

    public function getMobile()
    {
        return $this->mobile;
    }
    
	function execute() {
		parent::execute();
		
		$links = $this->getLinks();
        
		$this->recursiveCheckSelected($links);
		
        $this->setViewName('clearwebapps::menu_old')
			->with('links', $links)
            ->with('id', $this->getId())
            ->with('mobile', $this->getMobile())
            ->with('mobileOptions', $this->mobileOptions)
            ->with('mobileConfig', $this->mobileConfig)
            ;
	}
    
    protected function recursiveCheckSelected(&$links)
    {
        foreach($links as &$link) {
            $page = Clearworks::getCurrentPage();
            if ($page) {
                $url = Clearworks::getPageUrl(Clearworks::getCurrentPage(), []);
            } else {
                $url = Request::url();
            }
            $link['selected'] = ($link['baseurl'] == $url);
            
            $this->recursiveCheckSelected($link['children']);
		}
    }
}