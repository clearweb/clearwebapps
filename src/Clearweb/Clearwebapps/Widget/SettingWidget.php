<?php namespace Clearweb\Clearwebapps\Widget;

use Clearweb\Clearwebapps\Form\Validator;
use Clearweb\Clearwebapps\Setting\Setting;

use Settings;

class SettingWidget extends \Clearweb\Clearwebapps\Widget\FormWidget
{
    private $settings = array();
    
	public function init()
	{
        $validator = new Validator;
        foreach($this->getSettings() as $setting) {
            $validator->mergeRules($setting, 'required');
        }
        
		$this->setValidator($validator);
		
		parent::init();
	}
    
	public function execute() {
		parent::execute();
        
        foreach($this->getSettings() as $key) {
            $this->getForm()->setValue($key, Settings::get($key));
        }
	}
	
	public function submit(array $post)
	{
		foreach($this->getSettings() as $key) {
            if (isset($post[$key])) {
                $value = $post[$key];
            } else {
                $value = '';
            }
            
            Settings::set($key, $value);
		}
        
	}
    
    /**
     * Returns the settings which can be set using this widget.
     * @return array An array with the keys of the settings
     */
    public function getSettings()
    {
        return $this->settings;
    }

    public function setSettings(array $settings)
    {
        $this->settings = $settings;
        
        return $this;
    }
    
    public function addSetting($setting)
    {
        $this->settings[] = $setting;
        
        return $this;
    }
    
    public function removeSetting($setting)
    {
        foreach($this->settings as $i=>$settingCursor) {
            if ($setting == $settingCursor) {
                unset($this->settings[$i]);
            }
        }
        
        return $this;
    }
    
}
