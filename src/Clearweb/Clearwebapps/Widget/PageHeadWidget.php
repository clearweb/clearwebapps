<?php namespace Clearweb\Clearwebapps\Widget;

class PageHeadWidget extends ViewWidget
{
    private $actions;
    
    public function init()
    {
        $this->setName('page-head')
            ->setViewName('clearwebapps::page_head_widget')
            ->with('title', $this->getTitle())
            ->with('actions', $this->getActions())
            ->setTitle('')
            ;
        
        return parent::init();
    }
    

    /**
     * returns actions of the page
	 * @return Array with elements of \Clearweb\Clearworks\Action\IActionButton
	 */
	public function getActions() {
		return $this->actions;
	}
    
    /**
	 * Sets actions of the page
	 * @param Array $actions with elements of \Clearweb\Clearworks\Action\IActionButton
     * @retun $this
	 */
	public function setActions(array $actions) {
		$this->actions = $actions;
        
        return $this;
	}
    
    public function setTitle($title) {
		$this->title = $title;
        
        return $this;
	}
	
	public function getTitle() {
        return $this->title;
    }
}