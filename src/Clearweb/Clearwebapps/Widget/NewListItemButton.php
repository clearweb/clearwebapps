<?php namespace Clearweb\Clearwebapps\Widget;

use Clearweb\Clearwebapps\Action\ScriptActionButton;
use Clearweb\Clearworks\Communication\ParameterChanger;

class NewListItemButton extends ScriptActionButton
{
	private $list_item_name = '';
	
	function __construct($list_item_name) {
		$this->setListItemName($list_item_name);
	}
	
	function setListItemName($list_item_name) {
		$this->list_item_name = $list_item_name;
		return $this;
	}
	
	function getListItemName() {
		return $this->list_item_name;
	}
	
	function init() {
		parent::init();
		
		$this->setTitle('<i class="fa fa-plus"></i>')
			->addClass('new-item')
			->addClass('button')
			->setScriptAction(
							  with(new ParameterChanger())
							  ->setParameter($this->getListItemName(), '0')
							  )
			;

		
		return $this;
	}
}