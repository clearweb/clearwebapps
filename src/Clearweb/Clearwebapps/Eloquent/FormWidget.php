<?php namespace Clearweb\Clearwebapps\Eloquent;

use Clearweb\Clearworks\Communication\ParameterChanger;
use Clearweb\Clearwebapps\Form\FieldFactory;
use Clearweb\Clearwebapps\Form\SubmitField;
use Clearweb\Clearwebapps\Form\Validator;
use Clearweb\Clearwebapps\Form\Form;

use Clearweb\Clearwebapps\Content\Error;

use Event;

abstract class FormWidget extends \Clearweb\Clearwebapps\Widget\FormWidget {
	private   $model         = null;
    private   $model_class   = null;
    private   $lang_prefix   = 'app';
	protected $hidden_fields = array('id', 'password', 'created_at', 'updated_at');
    
    public function __construct() {
        parent::__construct();
        $this->setModelClass(ModelManager::getModelClassFromSibling($this));
    }
    
	public function init() {
		$this->loadModel();
		$this->loadForm();
		$this->loadValidator();
		
		$this->setName($this->getModelName().'-form');
		
		parent::init();
		
		$this->addListener($this->getModelName());

		$this->setSubmittedNotice(trans('clearwebapps::form_widget.saved'));
		
		$this->loadTitle();
		
		return parent::init();
	}
	
	public function execute() {
		parent::execute();
        
		$this->getForm()->setValues($this->getModel()->toArray());
		if ($this->getSubmitted()) {
            $this->afterSubmit();
		}
		
		
        $this->translateLabels();
		
		return $this;
	}
    
    public function setLangPrefix($lang_prefix) {
		$this->lang_prefix = $lang_prefix;
		
		return $this;
	}
	
	public function getLangPrefix() {
		return $this->lang_prefix;
	}
    
    protected function afterSubmit()
    {
        $this->getContainer()
            ->addViewable(
                          with(new ParameterChanger())
                          ->setParameter($this->getModelName(), $this->getModel()->id)
                          );
    }
    
	protected function loadTitle()
	{
		$label = trim(ModelManager::getModelLabel($this->getModel()));
		
		if (empty($label) || !is_string($label)) {
			$label = trans(
						   'clearwebapps::form_widget.create_new',
						   array('item'=>\Lang::choice($this->getLangPrefix().'.'.$this->getModelName(), 1))
						   );
		}
		
		$this->setTitle($label);
	}
	
    protected function translateLabels()
    {
		foreach($this->getForm()->getFields() as $field) {
			$label = $this->translateLabel($field->getName());
			$field->setLabel($label);
		}
    }
    
    protected function translateLabel($field_name)
    {
        $field = $this->getForm()->getField($field_name);
        if ($field_name == 'submit') {
            return $field->getLabel();
        } else {
            $field = $this->getForm()->getField($field_name);
            // @TODO this should be checked differently!
            if ($field->getLabel() != $field->getName() && $field->getLabel().'_id' != $field->getName()) {
                return $field->getLabel();
            } else {
                return ucfirst(trans_choice($this->getLangPrefix().'.'.$field->getLabel(), 1));
            }
        }
    }
    
	public function submit(array $post) {
		$model = $this->model;
		
		foreach($post as $key => $value) {
			if ($key != 'id') {
				if ($model->isFillable($key)) {
					if (is_array($value)) {
						if (array_keys($value) === range(0, count($value) - 1)) {
							// array is not associative
							$model->$key = implode(';', $value);
						} else {
							// array is associative
							$model->$key = serialize($value);
						}
					} else {
						$model->$key = $value;
					}
				} elseif ($model->isGuarded($key)) {
					throw new \Illuminate\Database\Eloquent\MassAssignmentException($key);
				}
			}
		}
		
		$model->save();
		
		$this->model = $model;
		
		return $model->id;
	}
	
	public function addErrors(array $errors) 
	{
		$error_html = '';
		
		foreach($errors as $field => $field_errors) {
			
			$field_label = trans($this->getLangPrefix().'.'."$field");
			
			foreach($field_errors as $error) {
				$error_html .= ucfirst("$field_label: ".trans($error, array('attribute'=>$field_label))."<br />");
			}
		}
		
		$this->addViewable(new Error($error_html));
	}
	
	public function getModel() {
		return $this->model;
	}
	
	public function setModel(\Eloquent $model) {
		$this->model = $model;
		return $this;
	}
	
	protected function loadModel() {
        $class = $this->getModelClass();
		
		$this->setModel(ModelManager::create($class));
        
        
        $model = $this->getModel()->find($this->getParameter($this->getModelName()));
		
		if ( ! is_null($model)) {
			$this->setModel($model);
		}
	}
	
	protected function loadForm() {
		$form = new Form;
		
		$form->addField(FieldFactory::make('id', 'hidden', $this->getModel()->id));
		$form->addFields(ModelFieldScaffolder::getModelFields($this->getModel(), $this->hidden_fields));
		$form->addField(with(new SubmitField)->setName('submit')->setLabel(trans('clearwebapps::form_widget.submit')));
		
		$this->setForm($form);
	}
	
	protected function loadValidator() {
		$validator = with(new Validator)
			->setRules(array());
			;
		
		$this->setValidator($validator);
	}
    
    public function setModelClass($model_class) {
        $this->model_class = $model_class;
        
        Event::fire('formwidget.model-class-set', array($this, $model_class));
        
        return $this;
    }
    
    public function getModelClass() {
        return $this->model_class;
    }
	
	protected function getModelName() {
		return ModelManager::getModelName(get_class($this->getModel()));
	}
}