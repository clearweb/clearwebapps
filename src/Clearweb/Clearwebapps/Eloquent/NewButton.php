<?php namespace Clearweb\Clearwebapps\Eloquent;

use Clearweb\Clearwebapps\Action\ScriptActionButton;
use Clearweb\Clearworks\Communication\ParameterChanger;

class NewButton extends ScriptActionButton
{
    private $model_class   = null;
    private $lang_prefix = 'app';
    
    function __construct() {
        $this->setModelClass(ModelManager::getModelClassFromSibling($this));
    }
    
	function init() {
        $list_item_name = ModelManager::getModelName($this->getModelClass());
        
        $nice_name = trans_choice($this->getLangPrefix().'.'.$list_item_name, 1);
        
		parent::init();
		
		$this->setTitle(trans('clearwebapps::general.make_new', array('model-name'=>$nice_name)))
			->addClass('new-item')
			->addClass('button')
			->setScriptAction(
							  with(new ParameterChanger())
							  ->setParameter($list_item_name, '0')
							  )
			;

		
		return $this;
	}
    
    /**
     * Sets the prefix for translation of the columns trans([prefix].[colname]);
     * @param string $lang_prefix the prefix
     */
    public function setLangPrefix($lang_prefix) {
		$this->lang_prefix = $lang_prefix;
		
		return $this;
	}
    
    /**
     * gets the prefix for translation of the columns trans([prefix].[colname]);
     * @return string the prefix
     */
	public function getLangPrefix() {
		return $this->lang_prefix;
	}
    
    public function setModelClass($model_class) {
        $this->model_class = $model_class;
        return $this;
    }
    
    public function getModelClass() {
        return $this->model_class;
    }
    
}