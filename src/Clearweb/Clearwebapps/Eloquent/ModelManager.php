<?php namespace Clearweb\Clearwebapps\Eloquent;

class ModelManager {
	/**
	 * Creates the eloquent model of given class and id
	 * @param string $class the class of the model
	 * @param mixed $id the id of the model
	 * @todo correct exception here
	 */
	static function create($class, $id=null) {
		if (is_subclass_of($class, '\Illuminate\Database\Eloquent\Model')) {
			$model = new $class;
			if (! is_null($id)) {
				$model = $model->find($id);
			}
			
			return $model;
		} else {
			throw new \Exception('model "'.$class.'" is not an eloquent model');
		}
	}
	
	static function getModelName($class) {
		return snake_case(class_basename($class));
	}
	
	static function getModelLabel(\Illuminate\Database\Eloquent\Model $model) {
		if (method_exists($model, 'getLabel')) {
			$label = $model->getLabel();
		} elseif (method_exists($model, 'getTitle')) {
			$label = $model->getTitle();
		} elseif (method_exists($model, 'getName')) {
			$label = $model->getName();
		} elseif ( ! empty($model->title)) {
			$label = $model->title;
		} elseif ( ! empty($model->name)) {
			$label = $model->name;
		} else {
			$label = $model->id;
		}
		
		return $label;
	}
	
	public static function getModelClassFromSibling($sibling) {
		$reflector      = new \ReflectionClass($sibling);
		$namespace      = $reflector->getNamespaceName();
		$namespaces     = explode('\\', $namespace);
		$last_namespace = array_pop($namespaces);
		$class = '\\'.$namespace.'\\'.$last_namespace;
		
		return $class;
	}
	
	
	/* column magic */
		
	public static function getAssociatedModel($model, $field) {
		$relation = call_user_func(array(&$model , substr($field, 0, -3)));
		
		return $relation->getRelated();
	}
	
	public static function getModelLabelOrderColumn(\Illuminate\Database\Eloquent\Model $model)
	{
		if (method_exists($model, 'getNameOrderColumn')) {
			$label = $model->getNameOrderColumn();
		} elseif (static::hasColumn($model, 'name')) {
			$label = 'name';
		} else {
			$label = 'id';
		}
		
		return $label;
	}
	
	public static function getColumns($model)
	{
		$columns = \DB::getDoctrineSchemaManager()->listTableDetails($model->getTable())->getColumns();
		return array_map(function($e) {return $e->getName();}, $columns);
	}
	
	public static function hasColumn($model, $attribute)
	{
		$columns = static::getColumns($model);
		return (isset($columns[$attribute]));
	}
	
	public static function isAssociatedModelField($model, $field) {
		$types = self::getColumnTypes($model);
		
		return (
				isset($types[$field])
				&& $types[$field] instanceof \Doctrine\DBAL\Types\IntegerType
				&& substr($field, -3) == '_id'
				&& method_exists($model, substr($field, 0, -3))
				&& call_user_func(array(&$model , substr($field, 0, -3))) instanceof \Illuminate\Database\Eloquent\Relations\Relation
				);
	}
	
	public static function getColumnTypes($model) {
		$columns = \DB::getDoctrineSchemaManager()->listTableDetails($model->getTable())->getColumns();
		return array_map(function($e) {return $e->getType();}, $columns);
	}

}