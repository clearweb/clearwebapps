<?php namespace Clearweb\Clearwebapps\Eloquent;

use Clearweb\Clearwebapps\Paging\ListPager;

class PagableListWidget extends SortableListWidget
{
	private $items_per_page = 20;
	
	/*
	 * these variables will only be filled after calling the execute function of the parent.
	 */
	private $last_page = 0;
	private $total_items = 0;
	
	public function setItemsPerPage($items_per_page)
	{
		$this->items_per_page = $items_per_page;
		return $this;
	}
	
	public function getItemsPerPage()
	{
		return $this->items_per_page;
	}
	
	protected function deduceSelectedPage() {
		return $this->getParameter($this->getList()->getListItemName().'-list-page', 1);
	}
	
	protected function applyQueryFilters(\Illuminate\Database\Eloquent\Builder $builder)
	{
		$builder = parent::applyQueryFilters($builder);
		
		$page    = $this->deduceSelectedPage();
		
		$this->total_items = $builder->count();
		$this->last_page   = ceil($this->total_items / $this->getItemsPerPage());
		
		
		if ($page < 1 || $page > $this->last_page) {
			$page = 1;
		}
		
		$builder->offset(($page - 1) * $this->getItemsPerPage())
			->limit($this->getItemsPerPage())
			;
		
		return $builder;
	}
	
	function init() {
		parent::init();
		$this->addListener($this->getList()->getListItemName().'-list-page');
	}
	
	function execute() {
		parent::execute();
		
		$this->addViewable(
						  with(new ListPager())
						  ->setNumberPages($this->last_page)
						  ->setSelectedPage($this->deduceSelectedPage())
						  ->setItemsPerPage($this->getItemsPerPage())
						  ->setPagingParameter($this->getList()->getListItemName().'-list-page')
						  )
			;
		
		return $this;
	}
}