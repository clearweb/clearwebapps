<?php namespace Clearweb\Clearwebapps\Eloquent;

abstract class ListObject extends \Clearweb\Clearwebapps\Lists\ListObject {

	/**
	 * Standard implementation of getModelClassName
	 */
	function getModelClassName() {
		$reflector = new \ReflectionClass($this);
		$namespace = $reflector->getNamespaceName();
		return "\\$namespace\\$namespace";
	}
	
	public function init() {
		parent::init();
		$this->loadItems();
		$this->loadColumns();
	}
	
	public function setItems(array $items) 
	{
		$model = ModelManager::create($this->getModelClassName());
		$columns = ModelManager::getColumns($model);
		
		foreach($columns as $i=>$column) {
			if (ModelManager::isAssociatedModelField($model, $column)) {
				$related_model = ModelManager::getAssociatedModel($model, $column);
				foreach($items as &$item) {
					$current_related_model = $related_model->find($item[$column]);
					if ( empty($current_related_model)) {
						$item[substr($column, 0, -3)] = '';
					} else {
						$item[substr($column, 0, -3)] = ModelManager::getModelLabel($current_related_model);
					}
				}
			}
		}
		
		return parent::setItems($items);
	}
	
	protected function loadItems() {
		$class = $this->getModelClassName();
		$this->setItems($class::all()->toArray());
	}

	protected function loadColumns() {
		$model = ModelManager::create($this->getModelClassName());
		$columns = ModelManager::getColumns($model);
		
		foreach($columns as $i=>$column) {
			if (ModelManager::isAssociatedModelField($model, $column)) {
				$columns[$i] = substr($column, 0, -3);
			}
		}
		
		$this->setColumns($columns);
	}
	
	function getHiddenColumns() {
		$model = ModelManager::create($this->getModelClassName());
		$columns = ModelManager::getColumns($model);
		
		$hidden_columns = parent::getHiddenColumns();
		
		foreach($columns as $i=>$column) {
			if (ModelManager::isAssociatedModelField($model, $column)) {
				foreach($hidden_columns as $i => $hidden_column) {
					if ($hidden_column == $column) {
						$hidden_columns[$i] = substr($column, 0, -3);
					}
				}
			}
		}
		return $hidden_columns;
	}
	
	function getListItemName() {
		return ModelManager::getModelName($this->getModelClassName());
	}
}