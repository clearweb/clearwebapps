<?php namespace Clearweb\Clearwebapps\Eloquent;

use Clearweb\Clearworks\Action\Action;

use Auth;

class SwapRankAction extends Action
{
    private $success = false;
    
    public function execute()
    {
        if (Auth::check()) {
            $modelName = $this->getParameter('model');
            
            $entity = ModelManager::create($modelName, $this->getParameter('id'));
            $entity->rank = $this->getParameter('newRank');
            
            foreach($modelName::where('rank', $this->getParameter('newRank'))->get() as $swapped) {
                $swapped->rank = $this->getParameter('oldRank');
                $swapped->save();
            }
            
            $entity->save();
            
            $this->success = true;
        }
    }
    
    public function getJSON()
    {
        return ($this->success)?'success':'failed';
    }
}