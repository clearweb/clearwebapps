<?php namespace Clearweb\Clearwebapps\Eloquent;

use Event;

abstract class ListWidget extends \Clearweb\Clearwebapps\Widget\ListWidget {
    private $model_class   = null;
    
	private $query_listeners = array();
	
	private $order_attribute = 'id';
	private $order_direction = 'asc';
    
    private $lang_prefix = 'app';
    private $list_item_name = '';
	
	private $multi_lang = false;
    
    public function __construct() {
        parent::__construct();
        
        $this->setModelClass(ModelManager::getModelClassFromSibling($this));
    }
    
	function init() {
        if ($this->getListItemName() == '') {
            $this->setListItemName(ModelManager::getModelName($this->getModelClass()));
        }
        
		$this->loadList();
		$this->setName($this->getListItemName().'-list');
		
		$this->loadTitle();
		
		$this->addListener('search');
		$this->addListener($this->getListItemName());
		
		return parent::init();
	}
	
	public function execute() {
		$list = parent::getList();
		$class = $this->getModelClass();
		
		$builder = $this->applyQueryFilters($class::query());
		$builder   = $this->applyOrdering($builder);
		
		$list->setItems($builder->get()->toArray())
			->hideColumn('created_at')
			->hideColumn('updated_at')
			->hideColumn('id')
			->setSelected($this->getModelID())
			;
		
		return parent::execute();
	}

	public function setListItemName($list_item_name) {
		$this->list_item_name = $list_item_name;
		return $this;
	}
	
	public function getListItemName() {
		return $this->list_item_name;
	}

	public function setMultiLanguage($multi_lang) {
		$this->multi_lang = $multi_lang;
		return $this;
	}
	
	public function getMultiLanguage() {
		return $this->multi_lang;
	}
	
	public function setOrderAttribute($order_attribute) {
		$this->order_attribute = $order_attribute;
		
		return $this;
	}
	
	public function getOrderAttribute() {
		return $this->order_attribute;
	}
    
	public function setOrderDirection($order_direction) {
		$this->order_direction = $order_direction;
		
		return $this;
	}
	
	public function getOrderDirection() {
		return $this->order_direction;
	}
	
	/**
	 * Add query listener (this listener reacts to parameter changes 
	 * and automatically adds a where to the List items retrieve query)
	 * @param string $listener the parameter name
	 * @param mixed $database_field this is the name of the field we filter on in the database.
	 * If the value of this parameter is <code>null</code>, then the parameter listener
	 * is used for this.
	 * @return $this
	 */
	public function addQueryListener($listener, $database_field=null) {
		if (is_null($database_field))
			$database_field = $listener;
		
		$this->query_listeners[$listener] = $database_field;
		return $this;
	}
	
	/**
	 * Get query listeners (this listeners reacts to parameter changes 
	 * and automatically add where clauses to the List items retrieve query)
	 * @return array an array with query listeners. The format is array([parameter]=>[table_column])
	 */
	public function getQueryListeners() {
		return $this->query_listeners;
	}
	
	public function getListeners() {
		return array_merge(parent::getListeners(), array_keys($this->getQueryListeners()));
	}
	
	protected function loadList() {
		$class = $this->getModelClass();
		
		$list = with(new \Clearweb\Clearwebapps\Lists\ListObject)
			->setListItemName($this->getListItemName())
			;
		
		/* scaffold columns */
		$model = ModelManager::create($class);
		$columns = ModelManager::getColumns($model);
		
		foreach($columns as $i=>$column) {
			if (ModelManager::isAssociatedModelField($model, $column)) {
				$list->addColumn(substr($column, 0, -3), function ($row) use($model, $column) {
						$related_model = ModelManager::getAssociatedModel($model, $column);
						$current_related_model = $related_model::find($row[$column]);
                        if (empty($current_related_model)) {
                            $related_model_name = ucfirst(trans_choice($this->getLangPrefix().'.'.ModelManager::getModelName(get_class($related_model)), 1));
                            return trans('clearwebapps::list_widget.unknown_related_item', array('item-name'=>$related_model_name));
                        } else {
                            return ModelManager::getModelLabel($current_related_model);
                        }
					});
			} else {
				$list->addColumn($column);
			}
		}
		
		$this->setList($list);
	}
	
	/**
	 * Applies ordering to the query builder (the query which gets all list items)
	 * @param \Illuminate\Database\Eloquent\Builder $builder the query builder
	 * @return \Illuminate\Database\Eloquent\Builder the query builder
	 */
	protected function applyOrdering(\Illuminate\Database\Eloquent\Builder $builder)
	{
		$attribute  = $this->getOrderAttribute();
		$order_type = $this->getOrderDirection();
		
		$model = ModelManager::create($this->getModelClass());
		if (ModelManager::isAssociatedModelField($model, $attribute.'_id')) {
			$associated_model = ModelManager::getAssociatedModel($model, $attribute.'_id');
			
			$associated_order_column = ModelManager::getModelLabelOrderColumn($associated_model);
			
			$associated_table = $associated_model->getTable();
			
			$builder = $builder->leftJoin($associated_table, "{$model->getTable()}.{$attribute}_id", '=', "{$associated_table}.id")
				->orderBy("{$associated_table}.{$associated_order_column}", $order_type)
				->select("{$model->getTable()}.*")
				;
		} elseif(ModelManager::hasColumn($model, $attribute)) {
			$builder->orderBy($attribute, $order_type);
		}
		
		return $builder;
	}
	
	
	/**
	 * Applies filters to the query builder (the query which gets all list items)
	 * @param \Illuminate\Database\Eloquent\Builder $builder the query builder
	 * @return \Illuminate\Database\Eloquent\Builder the query builder
	 */
	protected function applyQueryFilters(\Illuminate\Database\Eloquent\Builder $builder) {
		/* apply filters for query listeners */
		foreach($this->getQueryListeners() as $key => $column) {
			$value = $this->getParameter($key, false);
			if ($value != false) {
				$builder->where($column, '=', $value);
			}
		}
		
		/* apply filters for search */
		$class   = $this->getModelClass();
		$model   = new $class;
		$columns = ModelManager::getColumns($model);
		$search  = $this->getParameter('search', '');
		
		if ( ! empty($search)) {
			$builder->where(
							function ($query) use($model, $columns, $search)
							{
								foreach($columns as $column) {
									if (ModelManager::isAssociatedModelField($model, $column)) {
										$related_model = ModelManager::getAssociatedModel($model, $column);
										$relation_name = substr($column, 0, -3);
										
										foreach(ModelManager::getColumns($related_model) as $related_column) {
											$related_items = $related_model::select('id')
												->where($related_column, 'like', '%'.$search.'%')
												->lists('id');
											if ( ! empty($related_items)) {
												$query->orWhereIn($column, $related_items);
											}
										}
									} else {
										$query->orWhere($column, 'like', '%'.$search.'%');
									}
								}
							});
		}
		
		return $builder;
	}
	
    public function setModelClass($model_class) {
        $this->model_class = $model_class;
        
        Event::fire('listwidget.model-class-set', array($this, $model_class));
        
        return $this;
    }
    
    public function getModelClass() {
        return $this->model_class;
    }
    
	private function getModelID() {
		$list = parent::getList();
		return $this->getParameter($list->getListItemName());
	}
	
	function loadTitle() {
		if ( ! is_null($this->getList())) {
			$this->setTitle(ucfirst(trans(
										  'clearwebapps::list_widget.item_list',
										  array('item'=>\Lang::choice($this->getLangPrefix().'.'.$this->getList()->getListItemName(), 2))
										  )));
		}
	}

}