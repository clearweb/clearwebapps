<?php namespace Clearweb\Clearwebapps\Eloquent;

abstract class RankedListWidget extends ListWidget {
	function init() {
		parent::init();
		
        $this->setOrderAttribute('rank')
            ->setOrderDirection('ASC')
            ->addClass('ranked-list-widget')
            ;
        
		return $this;
	}

    public function execute()
    {
        $this->getList()
            ->addActionLink(
                            (new UpInRankActionLink)
                            ->setList($this->getList())
                            ->setModelClass($this->getModelClass())
                            )
            ->addActionLink(
                            (new DownInRankActionLink)
                            ->setList($this->getList())
                            ->setModelClass($this->getModelClass())
                            )
            ->hideColumn('rank')
            ;
        
        parent::execute();
        
        $this->detectRankingError();
        

        
        return $this;
    }
    
    protected function detectRankingError()
    {
        if ( ! ModelManager::hasColumn(ModelManager::create($this->getModelClass()), 'rank')) {
            throw new ModelHasNoRankAttribute("Model '{$this->getModelClass()}' has no attribute 'rank'.");
        }
        
        $ranks = array_map(function($item) { return $item['rank']; }, $this->getList()->getItems());
        
        if (count(array_unique($ranks)) != count($ranks)) {
            $this->reorderList();
        }
    }
    
    protected function reorderList()
    {
        foreach($this->getList()->getItems() as $index => $item) {
            $object = ModelManager::create($this->getModelClass(), $item['id']);
            $object->rank = $index + 1;
            $object->save();
        }
    }
}