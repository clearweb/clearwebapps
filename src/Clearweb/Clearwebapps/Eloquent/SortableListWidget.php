<?php namespace Clearweb\Clearwebapps\Eloquent;

abstract class SortableListWidget extends ListWidget {
	private $query_listeners = array();
    private $sortable = true;
	
	function init() {
		parent::init();
		
        if ($this->getSortable()) {
            $this->addListener($this->getList()->getListItemName().'_order');
            $this->addListener($this->getList()->getListItemName().'_ordertype');
            
            $this->setOrderAttribute($this->getParameter($this->getList()->getListItemName().'_order', $this->getOrderAttribute()));
            $this->setOrderDirection($this->getParameter($this->getList()->getListItemName().'_ordertype', $this->getOrderDirection()));
		}
        
		parent::init();
		
		$this->addScript('sortable_list', 'packages/clearweb/clearwebapps/js/sortable_list_widget.js', array('jquery'));
		
		return $this;
	}
	
	public function execute() {
		$this->getContainer()->addClass('sortable-list-widget');
		
		parent::execute();
	}
    
    public function setSortable($sortable) {
        $this->sortable = $sortable;
        return $this;
    }

    public function getSortable() {
        return $this->sortable;
    }
}