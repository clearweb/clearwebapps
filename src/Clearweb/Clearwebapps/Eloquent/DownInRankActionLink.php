<?php namespace Clearweb\Clearwebapps\Eloquent;

use Clearweb\Clearwebapps\Action\AjaxActionAnchor;

use Clearweb\Clearworks\Communication\ReloadCurrentWidget;

use Clearweb\Clearwebapps\Lists\ListObject;

use Clearworks;

class DownInRankActionLink extends AjaxActionAnchor
{
    private $list;
    private $modelClass;
    
    public function setList(ListObject $list)
    {
        $this->list = $list;
        
        return $this;
    }
    
    public function getList()
    {
        return $this->list;
    }

    public function setModelClass($modelClass)
    {
        $this->modelClass = $modelClass;
        
        return $this;
    }
    
    public function getModelClass()
    {
        return $this->modelClass;
    }
    
    public function init()
    {
        $this->setClasses([]);
        
        parent::init();
        
        return $this;
    }
    
    public function execute()
    {
        $this->setOnFinishAction(new ReloadCurrentWidget)
            ->addClass('lower-level')
            ;
        $items = $this->getList()->getItems();
        $index = array_search($this->getParameters(), $items);
        
        if ($index < count($items) - 1) {
            $params = [
                       'oldRank' => $this->getParameter('rank'),
                       'newRank' => $items[$index + 1]['rank'],
                       'id' => $this->getParameter('id'),
                       'model' => $this->getModelClass()
                       ];
            
            $this->setUrl(Clearworks::getActionUrl(new SwapRankAction, $params));
        } else {
            $this->addClass('disabled')
                ->setUrl('#')
                ;
        }
        
        return parent::execute();
    }
}