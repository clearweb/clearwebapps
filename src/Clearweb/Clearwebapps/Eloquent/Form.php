<?php namespace Clearweb\Clearwebapps\Eloquent;

class Form extends \Clearweb\Clearwebapps\Form\Form {
	protected $model = null;
	protected $hidden_fields = array('id', 'password', 'created_at', 'updated_at');
	protected $required_fields = array();
	
	public function __construct() {
		$this->loadDefaultModel();
		$this->loadFields();
		$this->loadRequiredFields();
	}

	function loadDefaultModel() {
		$reflector = new \ReflectionClass($this);
		$namespace = $reflector->getNamespaceName();
		$class = "\\$namespace\\$namespace";
		$this->setModel(new $class);
	}
	
	/**
	 * Sets the model for the form.
	 * @param \Eloquent $model the model to build the form for.
	 * @return EloquentForm this instance for chaining purposes.
	 */
	public function setModel(\Eloquent $model) {
		$this->model = $model;
		$this->loadFields();
		return $this;
	}

	/**
	 * Gets the model of the form.
	 * @return \Eloquent the model
	 */
	public function getModel() {
		return $this->model;
	}
	
	function hideField($property) {
		$this->hidden_fields[] = $property;
	}
	
	/**
	 * @override
	 */
	public function getValue($key, $default = NULL) {
		return $this->model->$key;
	}
	
	/**
	 * (Re)loads all the fields (scaffolding)
	 */
	public function loadFields() {
		$this->clearFields();
		
		if ( ! is_null($this->model)) {
			$this->addField(\Clearweb\Clearwebapps\Form\FieldFactory::make('id', 'hidden', $this->model->id));
			$this->addFields(ModelFieldScaffolder::getModelFields($this->model, $this->hidden_fields));
			
		}
		
		$this->addField(with(new \Clearweb\Clearwebapps\Form\SubmitField)->setName('submit')->setValue('submit'));
	}
	
	private function isVisibleField($field){
		return ( ! in_array($field, $this->hidden_fields));
	}
	
	private function loadRequiredFields() {
		$required_fields = array();
		foreach(ModelManager::getColumns($this->model) as $field) {
			$required_fields[] = $this->isVisibleField($field);
		}
		
		return $required_fields;
	}
	
	protected function getRequiredFields() {
		return $this->required_fields;
	}
	
	/**
	 * throws \Clearweb\Clearwebapps\Form\Exception\UnknownFieldException
	 */
	protected function setRequiredFields(array $required_fields) {
		$this->required_fields = array();
		
		foreach($required_fields as $required_field) {
			$this->addRequiredField($required_field);
		}
		
		return $this;
	}
	
	/**
	 * throws \Clearweb\Clearwebapps\Form\Exception\UnknownFieldException
	 */
	protected function addRequiredField($field) {
		if ($this->nameExists($field)) {
			$this->required_fields[] = $field;
		} else {
			throw new \Form\Exception\UnknownFieldException("field '$field' does not exist");
		}
		
		return $this;
	}
	
	/**
	 * throws \Clearweb\Clearwebapps\Form\Exception\UnknownFieldException
	 */
	protected function removeRequiredField($field) {
		if ($this->nameExists($field)) {
			foreach($this->required_fields as $i => $required_field) {
				if ($required_field == $field) {
					unset($this->required_fields[$i]);
				}
			}
		} else {
			throw new \Clearweb\Clearwebapps\Form\Exception\UnknownFieldException("field '$field' does not exist");
		}
		
		return $this;
	}
	
	public function validate(array $post) {
		$valid = TRUE;
		
		$fields = $this->getRequiredFields();
		
		$validation_rules = array();
		foreach($fields as $field){
			if ( $this->isVisibleField($field))
				$validation_rules[$field] = 'required';
		}
		
		$validator = \Validator::make($post,$validation_rules);
		
		if ($validator->fails()) {
			$valid = FALSE;
		
			foreach($fields as $field) {
				if ( $this->isVisibleField($field) && $validator->messages()->has($field)) {
					$this->addValidationError($field, $validator->messages()->first($field));
				}
			}
		}
		
		return $valid;
	}
	
	public function getSubmitted() {
		return false;
	}
	
	public function submit(array $post) {
		$model = $this->model;
		
		foreach($post as $key => $value) {
			if ($key != 'id') {
				if ($model->isFillable($key)) {
					$model->$key = $value;
				} elseif ($model->isGuarded($key)) {
					throw new \Illuminate\Database\Eloquent\MassAssignmentException($key);
				}
			}
		}
		
		$model->save();
		
		$this->model = $model;
		return $model->id;
	}
}