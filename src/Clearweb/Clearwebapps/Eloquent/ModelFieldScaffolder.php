<?php namespace Clearweb\Clearwebapps\Eloquent;

use Clearweb\Clearwebapps\Form\FieldFactory;

class ModelFieldScaffolder
{
	public static function getModelFields(\Eloquent $model, array $hidden_fields = array()) {
		$form_fields = array();
		
		$types = ModelManager::getColumnTypes($model);
		
		foreach($types as $name => $type) {
			if ( ! in_array($name, $hidden_fields)) {
				if ($type instanceof \Doctrine\DBAL\Types\IntegerType
					&& substr($name, -3) == '_id'
					&& method_exists($model, substr($name, 0, -3))
					&& call_user_func(array(&$model , substr($name, 0, -3))) instanceof \Illuminate\Database\Eloquent\Relations\Relation
				) {
					
					$relation = call_user_func(array(&$model , substr($name, 0, -3)));
					$related_model = $relation->getRelated();
					
					
					$options = array();
					foreach($related_model->get() as $option_model) {
						$option_name = ModelManager::getModelLabel($option_model);
						
						$options[$option_model->id] = $option_name;
					}
					$form_fields[] = with(FieldFactory::make($name, 'select', $related_model->id, $options))
						->setLabel(substr($name, 0, -3));
					
				} elseif (
						  $type instanceof \Doctrine\DBAL\Types\StringType ||
						  $type instanceof \Doctrine\DBAL\Types\IntegerType ||
						  $type instanceof \Doctrine\DBAL\Types\DecimalType ||
						  $type instanceof \Doctrine\DBAL\Types\FloatType
				) {
					$form_fields[] = FieldFactory::make($name, 'text', $model->$name);
				} elseif ($type instanceof \Doctrine\DBAL\Types\DateType) {
					$form_fields[] = FieldFactory::make($name, 'date', $model->$name);
				} elseif ($type instanceof \Doctrine\DBAL\Types\DateTimeType) {
					$form_fields [] = FieldFactory::make($name, 'datetime', $model->$name);
				} elseif ($type instanceof \Doctrine\DBAL\Types\TextType) {
					if ($name == 'summary' || $name == 'description') {
						$form_fields[] = FieldFactory::make($name, 'html', $model->$name);
					} else {
						$form_fields[] = FieldFactory::make($name, 'textarea', $model->$name);
					}
				} elseif ($type instanceof \Doctrine\DBAL\Types\BooleanType) {
					$form_fields[] = FieldFactory::make($name, 'checkbox', $model->$name);
				} else {
					echo "field '$name' of type '".get_class($type)."' could not be scaffolded <br />";
				}
			}
		}
		
		return $form_fields;
	}
}