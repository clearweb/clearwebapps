<?php namespace Clearweb\Clearwebapps\Eloquent;

use Clearweb\Clearworks\Communication\ParameterChanger;
use Clearweb\Clearworks\Action\IAction;

use Clearweb\Clearwebapps\Action\AjaxConfirmAnchor;

use Event;

class DeleteActionLink extends AjaxConfirmAnchor {
	private $action = null;
    private $model_class   = null;
    private $lang_prefix   = 'app';
    
	function __construct() {
		$this->setTitle(trans('clearwebapps::list_widget.delete'));
        $this->setModelClass(ModelManager::getModelClassFromSibling($this));
	}
	
	public function setDeleteAction(IAction $action) {
		$this->action = $action;
		return $this;
	}
	
	public function getDeleteAction() {
		return $this->action;
	}
	
    public function setModelClass($model_class) {
        $this->model_class = $model_class;
        
        Event::fire('delete-link.model-class-set', array($this, $model_class));
        
        return $this;
    }
    
    public function getModelClass() {
        return $this->model_class;
    }
    
	public function execute() {
		$model_id = $this->getParameter('id', null);
		
        $model_class = $this->getModelClass();
		$model = ModelManager::create($model_class, $model_id);
		
		$model_label = ModelManager::getModelLabel($model);
		$model_name  = ModelManager::getModelName($model_class);
		
		$delete_url = \Clearworks::getActionUrl($this->getDeleteAction(), array($model_name=>$model_id));
		$this->addClass('delete-link');
		$this->setURL($delete_url);
		$this->setOnFinishAction(
                                 with(new ParameterChanger)->setParameter($model_name, '0')
                                 );
		$this->setConfirmTitle(ucfirst(trans('clearwebapps::list_widget.delete_item', array('item'=>\Lang::choice($this->getLangPrefix().'.'.$model_name, 1)))));
		
		$this->setConfirmMessage(trans(
									   'clearwebapps::list_widget.delete_confirm',
									   array('item-type'=>\Lang::choice($this->getLangPrefix().'.'.$model_name, 1), 'item-name'=>$model_label)
									   ));
        
        parent::execute();
	}
    
    public function setLangPrefix($lang_prefix) {
        $this->lang_prefix = $lang_prefix;
		
		return $this;
	}
	
	public function getLangPrefix() {
		return $this->lang_prefix;
	}
}