<?php namespace Clearweb\Clearwebapps\Eloquent;

use Clearweb\Clearworks\Action\Action;

use Event;

class DeleteAction extends Action
{
	private $deleted = false;
    private $model_class   = null;
    
    public function __construct()
    {
        $this->setModelClass(ModelManager::getModelClassFromSibling($this));
    }
    
	public function init()
	{
		return $this;
	}
	
	public function execute()
	{
		$model = $this->getModel();
		if ( ! is_null($model)) {
			$model->delete();
		}
		
		return $this;
	}
	
	function getJSON() {
		$json = array();
		
		if ($this->deleted) {
			$json[] = 'success';
		} else {
			$json['error'] = 'Could not be deleted';
		}
		
		return $json;
	}
	
	protected function getModel() {
		$model = null;
		
        $class = $this->getModelClass();
		$id    = $this->getParameter(ModelManager::getModelName($class), 0);
		
		if ($class::exists($id)) {
			$model = ModelManager::create($class, $id);
		}
		
		return $model;
	}
    
	
    public function setModelClass($model_class) {
        $this->model_class = $model_class;
        
        Event::fire('delete-action.model-class-set', array($this, $model_class));
        
        return $this;
    }
    
    public function getModelClass() {
        return $this->model_class;
    }
}