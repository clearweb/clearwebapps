<?php namespace Clearweb\Clearwebapps\Action;

use Clearweb\Clearworks\Action\ActionButton;
use Clearweb\Clearworks\Action\ScriptAction;

use View;

class ScriptActionButton extends Button {
	private $script_action = null;
	
	public function getScriptAction() {
		return $this->script_action;
	}
	
	public function setScriptAction(ScriptAction $script_action) {
		$this->script_action = $script_action;
		return $this;
	}
	
	public function getView() {
		$attributes = '';
		foreach($this->getHTMLAttributes() as $key=>$value) {
			$attributes .= ' '.$key.'='.json_encode($value).'';
		}
        
        return View::make('clearwebapps::script_action_button')
            ->with('attributes', $attributes)
            ->with('title', $this->getTitle())
            ->with('classes', $this->getClasses())
            ->with('script', $this->getScriptAction()->getActionScript())
            ->with('buttonType', $this->getButtonType())
            ;
	}
    
	public function init() {}
	public function execute() {}
}