<?php namespace Clearweb\Clearwebapps\Action;

//use \Clearweb\Clearworks\Action\ActionAnchor;
use \Clearweb\Clearworks\Action\ScriptAction;

/**
 * This class represents anchors which execute an ajax action with a ScriptAction to execute on finish.
 */
class AjaxActionAnchor extends Button //ActionAnchor
{
	private $on_finish_action = null;
	
	function init() {
		$this->addClass('ajax-link')
            ->addScript('jquery-ui', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js', array('jquery'))
            ->addScript('ajax-links', 'packages/clearweb/clearwebapps/js/ajax_link.js', array('jquery-ui'));
        ;
        
        return parent::init();
	}
	
	function setOnFinishAction(ScriptAction $action) {
		$this->on_finish_action = $action;
		return $this;
	}
	
	function getOnFinishAction() {
		return $this->on_finish_action;
	}
	
	function execute() {
		$this->addHTMLAttribute('data-finish-callback', $this->getOnFinishAction()->getActionScript());
		parent::execute();
        return $this;
	}
	
}