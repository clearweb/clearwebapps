<?php namespace Clearweb\Clearwebapps\Action;

use Clearweb\Clearworks\Action\ActionAnchor;
use View;

class Button extends ActionAnchor
{
    private $buttonType = 'default';
    private $mappings = [];
    
    public function getButtonType()
    {
        return $this->buttonType;
    }
    
    public function setButtonType($buttonType)
    {
        $this->buttonType = $buttonType;
        
        return $this->buttonType;
    }
    
    public function getView()
    {
        $attributes = '';
		foreach($this->getHTMLAttributes() as $key=>$value) {
			$attributes .= ' '.$key.'='.json_encode($value).'';
		}
        
        return View::make('clearwebapps::button')
            ->with('attributes', $attributes)
            ->with('title', $this->processMappings($this->getTitle()))
            ->with('classes', $this->getClasses())
            ->with('buttonType', $this->getButtonType())
            ->with('url', $this->processMappings($this->getUrl()))
            ;
    }
    
    public function addMapping($key, $param)
    {
        $this->mappings[$key] = $param;

        return $this;
    }

    public function getMappings()
    {
        return $this->mappings;
    }
    
    public function processMappings($content)
    {
        foreach($this->getMappings() as $key => $mapper) {
            if (is_callable($mapper)) {
                $replacement = call_user_func($mapper, $this);
                $content = str_replace(':'.$key, $replacement, $content);
            } else {
                $content = str_replace(':'.$key, $this->getParameter($mapper), $content);
            }
        }

        return $content;
    }
}