<?php namespace Clearweb\Clearwebapps\Action;

use Clearweb\Clearworks\Action\ScriptAction;

class ScrollToId extends ScriptAction
{
	private $id = null;
	
	public function __construct($id)
	{
		$this->id = $id;
	}
	
	public function getActionScript() {
		return '$("body").scrollTop($("#'.$this->id.'").offset().top);';
	}
}