<?php namespace Clearweb\Clearwebapps\Action;

class AjaxConfirmAnchor extends AjaxActionAnchor
{
	private $confirm_message;
	private $confirm_title;
	
	function init() {
		parent::init();
		$this->addClass('confirm');
	}
	
	function execute() {
		parent::execute();
		$this->addHTMLAttribute('data-confirm-title', $this->getConfirmTitle());
		$this->addHTMLAttribute('data-confirm-content', $this->getConfirmMessage());
	}
	
	function setConfirmTitle($confirm_title) {
		$this->confirm_title = $confirm_title;
		return $this;
	}

	function getConfirmTitle() {
		return $this->confirm_title;
	}
	
	function setConfirmMessage($confirm_message) {
		$this->confirm_message = $confirm_message;
		return $this;
	}

	function getConfirmMessage() {
		return $this->confirm_message;
	}
}