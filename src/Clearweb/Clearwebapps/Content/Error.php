<?php namespace Clearweb\Clearwebapps\Content;

class Error extends Message
{
	public function __construct($message) {
		parent::__construct($message);
		
		$this->addClass('error');
	}
}