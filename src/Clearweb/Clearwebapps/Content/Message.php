<?php namespace Clearweb\Clearwebapps\Content;

use \Clearweb\Clearworks\Contracts\IViewable;

class Message implements IViewable
{
	private $message = '';
	private $classes = array();
	
	public function __construct($message) {
		$this->setMessage($message);
	}
	
	public function addClass($class) {
		$this->classes[] = $class;
		return $this;
	}
	
	public function setClasses(array $classes) {
		$this->classes = $classes;
		return $this;
	}
	
	public function getClasses() {
		return $this->classes;
	}
	
	public function setMessage($message) {
		$this->message = $message;
		return $this;
	}
	
	public function getMessage() {
		return $this->message;
	}
	
	public function getScripts() {
		return array();
	}
	
	public function getStyles() {
		return array('css/messages.css');
	}
	
	public function getView() {
		return "<div class=\"".implode(' ', $this->getClasses())."\">{$this->getMessage()}</div>";
	}
}