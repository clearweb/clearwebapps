<?php namespace Clearweb\Clearwebapps\Content;

use View;

class WidgetHeader implements \Clearweb\Clearworks\Contracts\IViewable
{
	private $title = '';
	
	function __construct($title = '') {
		$this->setTitle($title);
	}
	
	public function setTitle($title) {
		$this->title = $title;
		return $this;
	}
	
	public function getTitle() {
		return $this->title;
	}
	
	public function getView()
	{
        return View::make('clearwebapps::widget_header')
            ->with('title', $this->getTitle())
            ;
	}
	
	public function getStyles()
	{
		return array();
	}
	
	public function getScripts()
	{
		return array();
	}
}