<?php namespace \Clearweb\Clearwebapps\Content;

trait HasContainer
{
	private $container = null;
	private $additional_scripts = array();
	private $additional_styles  = array();
	
	function setContainer(Container $container) {
		$this->container = $container;
		return $this;
	}
	
	function getContainer() {
		return $this->container;
	}
	
	function addScript($script) {
		$this->additional_scripts[] = $script;
		return $this;
	}
	
	function addStyle($style) {
		$this->additional_styles[] = $style;
		return $this;
	}
	
	function getView()
	{
		if (empty($this->container)) {
			throw new NoContainerException('No container defined');
		}
		
		return $this->container->getView();
	}
	
	function getStyles()
	{
		if (empty($this->container)) {
			throw new NoContainerException('No container defined');
		}
		
		return array_merge($this->container->getStyles(), $this->additional_styles);
	}

	function getScripts()
	{
		if (empty($this->container)) {
			throw new NoContainerException('No container defined');
		}
		
		return array_merge($this->container->getScripts(), $this->additional_scripts);
	}
}