<?php namespace Clearweb\Clearwebapps\Content;

class Notice extends Message
{
	public function __construct($message) {
		parent::__construct($message);
		
		$this->addClass('notice');
	}
}