<?php namespace Clearweb\Clearwebapps\Theme;

use Illuminate\View\Factory;




use Illuminate\View\ViewFinderInterface;

use Illuminate\Events\Dispatcher;
use Illuminate\Container\Container;
use Illuminate\View\Engines\EngineResolver;

class ViewFactory extends Factory
{
    private $viewOverrides = [];
    
    public function addOverride($viewable, $view)
    {
        $override = [
                     'viewable' => $viewable,
                     'view' => $view,
                     ];
        
        if ($this->hasOverrideForViewable($viewable)) {
            return false;
        } else {
            $this->viewOverrides[] = $override;
            
            return true;
        }
    }
    
    public function hasOverrideForViewable($viewable)
    {
        $found = false;
        
        foreach($this->getOverrides() as $override) {

            if (is_a($override['viewable'], get_class($viewable), true)) {
                $found = true;
            }
        }
        
        return $found;
    }
    
    public function getOverrideView($viewable)
    {
        $view = false;
        
        foreach($this->getOverrides() as $override) {
            //if (get_class($override['viewable']) == get_class($viewable)) {
            if (is_a($override['viewable'], get_class($viewable), true)) {
                $view = $override['view'];
            }
        }
        
        return $view;
    }
    
    public function getOVerrides()
    {
        return $this->viewOverrides;
    }
    
    public function make($view, $data = [], $mergeData = [])
    {
        $trace = debug_backtrace();
        $i = 0;
        while (isset($trace[$i])) {
            if (isset($trace[$i]['object'])) {
                //$class = $trace[$i]['class'];
                $object = $trace[$i]['object'];
                
                if ($object instanceof \Clearweb\Clearworks\Contracts\IViewable) {
                    if ($this->hasOverrideForViewable($object)) {
                        $view = $this->getOverrideView($object);
                    }
                    
                    break;
                }
            }
            
            $i++;
        }
        
        return parent::make($view, $data, $mergeData);
    }
}