<?php namespace Clearweb\Clearwebapps\Theme;

use File;
use Clearweb\Clearworks\Asset\AssetBag;

class ThemeAssetHandler
{
    public function showScriptInclusion($javascripts)
    {
        $assetBag = new AssetBag;
        $assetBag->setScripts($javascripts);
        
        if (\Theme::getTheme()) {
            \Theme::getTheme()->onScripts($assetBag);
            $javascripts = $assetBag->getScripts();
        }
        
        foreach($this->orderScripts($javascripts) as $javascript) {
            if (preg_match('#^https?://.*#', $javascript['url'])) {
                echo '<script src="'.$javascript['url'].'"></script>'.PHP_EOL;
            } else {
                echo '<script src="'.$this->getAssetUrl($javascript['url']).'"></script>'.PHP_EOL;
            }
        }
    }
    
    public function showStyleInclusion($stylesheets)
    {
        $assetBag = new AssetBag;
        $assetBag->setStyles($stylesheets);
        
        if (\Theme::getTheme()) {
            \Theme::getTheme()->onStyles($assetBag);
            $stylesheets = $assetBag->getStyles();
        }
        
        foreach(array_unique($stylesheets) as $stylesheet) {
            echo '<link rel="stylesheet" href="'.$this->getAssetUrl($stylesheet).'" />'.PHP_EOL;
        }
    }

    /* asset finding */
    
    public function getAssetUrl($asset)
    {
        if (
            \Theme::getTheme()
            &&
            $this->themeAssetExists($asset)
        ) {
            return $this->getThemeAssetUrl($asset);
        } else {
            return asset($asset);
        }
        /*
        $matches = array();
        if (preg_match('#^([0-9a-zA-Z_-]+)::(.*)$#', $asset, $matches)) {
            if (\Theme::getTheme()) {
                
                
                //$file = $this->getThemeAssetPath($matches[2], $matches[1]);
                
            } else {
                
            }
        } else {
            
        }
        */
    }
    
    public function themeAssetExists($asset)
    {
        return File::exists($this->getThemeAssetPath($asset));
    }
    
    public function getThemeAssetPath($asset)
    {
        $themeName = \Theme::getTheme()->getName();
        $path = \Theme::getThemePath().$themeName.'/assets/';

        return $path.$asset;
    }
        
    public function getThemeAssetUrl($asset)
    {
        $themeName = \Theme::getTheme()->getName();
        $baseUrl = \Theme::getThemeUrl().'/'.$themeName.'/assets/';
        return $baseUrl.$asset;
    }
    
    /*
    public function getAssetPath($asset, $package=null)
    {
        
    }
    
    public function getThemeAssetPath($asset, $package=null)
    {
        $themeName = \Theme::getTheme()->getName();
        $path = \Theme::getThemePath().$themeName.'/assets/';
        
        if ($package) {
            $path .= $package.'/';
        }
        
        return $path.$asset;
    }
    */    
    
    /* organize dependencies */
    
    private function orderScripts($javascripts) {
        $order = array();
        $deadlock = false;
        
        $javascriptsCount = count($javascripts);
        
        while(
              count($order) < $javascriptsCount &&
              ! $deadlock
        ) {
            $addedScript = false;
            
            foreach($javascripts as $i => $javascript) {
                if (count($javascript['dependencies'])) {
                    $depsMet = true;
                    foreach($javascript['dependencies'] as $dep) {
                        if ( ! $this->dependencyInOrder($dep, $order)) {
                            $depsMet = false;
                        }
                    }
                    
                    if ($depsMet) {
                        $addedScript = true;
                        $order[] = $javascript;
                        unset($javascripts[$i]);
                    }
                    
                } else {
                    $addedScript = true;
                    $order[] = $javascript;
                    unset($javascripts[$i]);
                }
            }
            
            $deadlock = ! $addedScript;
        }
        
        if ($deadlock) {
            echo 'scripts have deadlock!';
        }
        
        return $order;
    }
    
    private function dependencyInOrder($scriptName, $order)
    {
        $found = false;
        
        foreach($order as $orderScript) {
            if ($orderScript['name'] == $scriptName) {
                $found = true;
            }
        }
        return $found;
    }
}