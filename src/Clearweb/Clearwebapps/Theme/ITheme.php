<?php namespace Clearweb\Clearwebapps\Theme;

use Clearweb\Clearworks\Contracts\IExecutable;
use Clearweb\Clearworks\Asset\AssetBag;

interface ITheme extends IExecutable
{
    public function getName();
    public function setName($name);
    
    /**
     * A callback executed whenever scripts will be shown.
     */
    public function onScripts(AssetBag &$assetBag);
    
    /**
     * A callback executed whenever Styles will be shown.
     */
    public function onStyles(AssetBag &$assetBag);
}