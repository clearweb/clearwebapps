<?php namespace Clearweb\Clearwebapps\Theme;

abstract class Theme implements ITheme
{
    private $name = '';
    private $status = ITheme::STATUS_START;
    
    public function setName($name)
    {
        $this->name = $name;
        
        return $this;
    }
    
    public function getName()
    {
        return $this->name;
    }
    
    public function init() {
        $this->status = ITheme::STATUS_INITED;
    }
    
    public function execute()
    {
        $this->status = ITheme::STATUS_EXECUTED;
    }
    
    public function isInited()
    {
        return (in_array(
                         $this->status, 
                         array(ITheme::STATUS_INITED, ITheme::STATUS_EXECUTED)
                         ));
    }
    
    public function isExecuted()
    {
        return $this->status == ITheme::STATUS_EXECUTED;
    }
}