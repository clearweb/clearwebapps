<?php namespace Clearweb\Clearwebapps\Theme;

use File;

class ThemeHandler
{
    private static $theme = null;
    private static $themePath = '';
    private static $themeUrl = '';
    
    private static $assetHandler = null;
    
    public function __construct()
    {
        static::$themePath = public_path()."/themes/";
        static::$themeUrl  = url("/themes/");
        
        static::setAssetHandler(new ThemeAssetHandler);
    }
    
    public static function selectTheme($name)
    {
        $theme = false;
        $themeClass = ucfirst(camel_case($name)).'Theme';
        
        if(
           File::exists(static::getThemePath().$name)
           &&
           File::exists(static::getThemePath().$name.'/'.$themeClass.'.php')
        ) {
            require_once(static::getThemePath().$name.'/'.$themeClass.'.php');
            if (
                class_exists($themeClass) && 
                is_subclass_of($themeClass, '\Clearweb\Clearwebapps\Theme\ITheme')
            ){
                $theme = new $themeClass;
                $theme->setName($name);
            }
        }
        
        if ( ! empty($theme)) {
            static::setTheme($theme);
        }
        
        return $theme;
    }
    
    public static function setTheme(Theme $theme)
    {
        static::$theme = $theme;
    }
    
    public static function getTheme()
    {
        return static::$theme;
    }
    
    public static function setThemePath(ThemePath $themePath)
    {
        static::$themePath = $themePath;
    }
    
    public static function getThemePath()
    {
        return static::$themePath;
    }

    public static function setThemeUrl(ThemeUrl $themeUrl)
    {
        static::$themeUrl = $themeUrl;
    }
    
    public static function getThemeUrl()
    {
        return static::$themeUrl;
    }
    
    public static function getAssetHandler()
    {
        return static::$assetHandler;
    }
    
    public static function setAssetHandler($assetHandler)
    {
        static::$assetHandler = $assetHandler;
    }
    
    public static function styles($styles)
    {
        static::getAssetHandler()->showStyleInclusion($styles);
    }
    
    public static function scripts($scripts)
    {
        static::getAssetHandler()->showScriptInclusion($scripts);
    }
}