<?php namespace Clearweb\Clearwebapps\Theme;

use Illuminate\View\FileViewFinder;

class ViewFileFinder extends FileViewFinder
{
    public function find($name)
    {
        if (\Theme::getTheme()) {
            $themeName = \Theme::getTheme()->getName();
            $themeViewsPath = \Theme::getThemePath().$themeName.'/views/';
        
            $matches = array();
            if (preg_match('#^([0-9a-zA-Z_-]+)::(([0-9a-zA-Z_-]+)([\.|\/]([0-9a-zA-Z_-]+))*)$#', $name, $matches)) {
                // we have a namespace
                $namespace = $matches[1];
                $view      = $matches[2];
            
                $fileBasePath = $themeViewsPath.'packages/'.$namespace.'/'.str_replace('.', '/', $view);
            } elseif(preg_match('#^([0-9a-zA-Z_-]+)([\.|\/][0-9a-zA-Z_-]+)*$#', $name, $matches)) {
                // we have a view file
                $view = $matches[0];
                $fileBasePath = $themeViewsPath.str_replace('.', '/', $view);
            }
        
            if ( ! empty($fileBasePath)) {
                foreach($this->getExtensions() as $extension) {
                    if ($this->getFileSystem()->exists($fileBasePath.'.'.$extension)) {
                        $file = $fileBasePath.'.'.$extension;
                    }
                }
            }
        }
        
        if (empty($file)) {
            $file = parent::find($name);
        }
        
        return $file;
    }
}