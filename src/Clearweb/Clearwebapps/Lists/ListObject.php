<?php namespace Clearweb\Clearwebapps\Lists;

use Clearweb\Clearworks\Action\IActionButton;
use Clearweb\Clearworks\Contracts\IViewable;

class ListObject implements IViewable {
	private $columns = array();
    
	private $hidden_columns = array('password');
	private $items = array();
	private $action_links = array();
	private $selected = NULL;
    private $selectionKey = 'id';
	private $item_name = '';
    
    private $presenter = null;
    private $lang_prefix = 'app';
	
    
    
	/**
	 * Gets items for the list.
	 * @return array an array with the items as objects.
	 * The objects in the array should contain all columns as properties
	 */	
	public function getItems() {
		return $this->items;
	}
	
	/**
	 * Sets the items of the list.
	 * @param Array $items an array with the list items
	 * @return $this
	 */
	public function setItems(array $items) {
		$this->items = $items;
		return $this;
	}
	
	/**
	 * Gets the columns of the list.
	 * @return array an array with the columns as an array.
     * example: <code>array(array('name'=>'id', 'callback'=>[callback function]))</code>
	 */
	public function getColumns() {
		return $this->columns;
	}
	
	/**
	 * Adds a column to the list.
	 * @param string $name the name of the column.
	 * @param closure $callback the callback to calculate the value of the column
	 * @return $this
	 * @todo implement $callback functionality
	 */
	public function addColumn($name, $callback = null, $position = 0, array $attributes = array()) {
        if (empty($callback)) {
			$callback = function($row) use ($name) {
                return $row[$name]; 
            };
		}
        
        $column = array('name' => $name, 'callback' => $callback);
        
		if ($position > 0) {
			$this->columns = array_merge(array_slice($this->columns, 0, $position), array($column), array_slice($this->columns, $position));
		} else {
			$this->columns[] = $column;
		}
        
		return $this;
	}

    /**
     * Replaces a column with another one using a callback.
	 * @param string $name the name of the column to replace.
	 * @param closure $callback the callback to calculate the value of the column
     */
	public function replaceColumn($name, $callback = null) {
        if (empty($callback)) {
			$callback = function($row) use ($name) {
                return $row[$name]; 
            };
		}
        
		if ( ! $this->columnExists($name)) {
			$this->addColumn($name, $callback);
		} else {
            foreach($this->columns as $i => $column) {
                if ($column['name'] == $name) {
                    $this->columns[$i]['callback'] = $callback;
                }
            }
        }
        
        return $this;
	}
	
    /**
     * Moves a column to a new position
	 * @param string $name the name of the column to replace.
     * @param int $position the new position for the column
     */
    public function moveColumn($name, $position) {
        foreach($this->columns as $i => $column) {
            if ($column['name'] == $name) {
                unset($this->columns[$i]);
                $callback = $column['callback'];
                break;
            }
        }
        
        if (isset($callback)) {
            $this->addColumn($name, $callback, $position);
        }
        
        return $this;
    }
    
	/**
	 * checks if a column exists or not
	 */
	public function columnExists($name)
	{
        $found = false;
        foreach($this->columns as $i => $column) {
            if ($column['name'] == $name) {
                $found = true;
            }
        }
        return $found;
	}
	
	/**
	 * Forces this column to be hidden
	 * @param string $column the name of the column to be hidden.
	 */
	function hideColumn($column) {
		$this->hidden_columns[] = $column;
		return $this;
	}
	
	/**
	 * Unforces this column to be hidden
	 * @param string $column the name of the column to be unhidden.
	 */
	function unhideColumn($column) {
		foreach($this->hidden_columns as $key => $hidden_column) {
			if ($hidden_column == $column) {
				unset($this->hidden_columns[$key]);
			}
		}
			                  
		return $this;
	}
	
	
	/**
	 * Removes a column from the list
	 * @param string $name the name of the column
	 */
	function removeColumn($name)
	{
        $key = false;
        
        foreach($this->columns as $i => $column) {
            if ($column['name'] == $name) {
                $key = $i;
            }
        }
        
		if ( $key === false ) {
			throw new UnknownColumnException("column '$name' is not defined in the list.");
		} else {
			unset($this->columns[$key]);
		}
		
		return $this;
	}
	
	/**
	 * Gets the hidden columns
	 * @return array an array of strings. 
	 */
	function getHiddenColumns() {
		return $this->hidden_columns;
	}
    
    /**
     * Sets the prefix for translation of the columns trans([prefix].[colname]);
     * @param string $lang_prefix the prefix
     */
    public function setLangPrefix($lang_prefix) {
		$this->lang_prefix = $lang_prefix;
		
		return $this;
	}
    
    /**
     * gets the prefix for translation of the columns trans([prefix].[colname]);
     * @return string the prefix
     */
	public function getLangPrefix() {
		return $this->lang_prefix;
	}
    
	/**
	 * Gets the name of the entity represented by each list item.
	 * @return string a string with the name
	 */
	public function getListItemName() {
		return $this->item_name;
	}
	
	/**
	 * Gets the name of the entity represented by each list item.
	 * @param string $item_name a string with the name
	 * @return $this
	 */
	public function setListItemName($item_name) {
		$this->item_name = $item_name;
		return $this;
	}
	
	/**
	 * Sets the action links we can execute on the list items
	 * @param Array $action_links an array of ActionButton instances
	 */
	function setActionLinks(Array $action_links) {
		$this->action_links = $action_links;
		return $this;
	}
	
    /**
     * Adds an action link to the links
     * @param \Clearweb\Clearworks\Action\IActionButton $action_link the action link to add
     * @param string $name an optional name for the link
     * @return $this
     */
	function addActionLink(IActionButton $action_link, $name=false) {
        if (empty($name)) {
            $this->action_links[] = $action_link;
        } else {
            $this->action_links[$name] = $action_link;
        }
		return $this;
	}
    
    /**
     * Removes a named action link from the links
     * @param string $name the name of the link
     * @return $this
     */
    function removeActionLink($name) {
        if (isset($this->action_links[$name])) {
            unset($this->action_links[$name]);
        }
        
        return $this;
    }
	
	/**
	 * Gets the actions links we can execute on the list items
	 * @return array an array with the action links.
	 * Should be instances of ActionButton.
	 */
	function getActionLinks() {
		return $this->action_links;
	}
	
	/**
	 * Sets the selected item
	 * @param string $id the id of the selected element
	 * @return $this
	 */
	function setSelected($id) {
		$this->selected = $id;
		return $this;
	}
	
	/**
	 * Gets the selected item.
	 * @return string the id of the selected item
	 */
	function getSelected() {
		return $this->selected;
	}
    
    function setSelectionKey($selectionKey)
    {
        $this->selectionKey = $selectionKey;
        return $this;
    }
    
    function getSelectionKey()
    {
        return $this->selectionKey;
    }
    
    public function setItemPresenter(ItemPresenter $presenter)
    {
        $this->presenter = $presenter;
        
        return $this;
    }
    
    public function getItemPresenter()
    {
        return $this->presenter;
    }
    
	public function getView() {
        $hidden_columns = $this->getHiddenColumns();
        $columns = array_filter(
                                $this->getColumns(),
                                function($column) use ($hidden_columns){
                                    return ( ! in_array($column['name'], $hidden_columns));
                                }
                                )
            ;
        
        /*
        $columns = array_map(
                             function($column) {
                                 $column['name'] = $column['name'];
                                 return $column;
                             },
                             $columns
                             )
            ;
        */
        
        $html = '';
        
        $this->getItemPresenter()
            ->setColumns($columns)
            ->setTotalCount(count($this->getItems()))
            ->setListItemName($this->getListItemName())
            ->setActionLinks($this->getActionLinks())
            ->setLangPrefix($this->getLangPrefix())
            ;
        
        $i = 1;
        foreach($this->getItems() as $item) {
            $this->getItemPresenter()->setIndex($i);
            $this->getItemPresenter()->setItemData($item);
            
            $selected = $this->getSelected();
            
            if (isset($item[$this->getSelectionKey()]) && $item[$this->getSelectionKey()] == $selected) {
                $this->getItemPresenter()->setSelected(true);
            } else {
                $this->getItemPresenter()->setSelected(false);
            }
            
            $html .= $this->getItemPresenter()->getView();
            
            $i++;
        }
        
        return $html;
	}

	function getScripts() {
		$scripts = array(
                         array('name' => 'list',
                               'url'=>'packages/clearweb/clearwebapps/js/list.js',
                               'dependencies' => array('jquery'),
                               )
                         );
		foreach($this->getActionLinks() as $link) {
			$scripts = array_merge($scripts, $link->getScripts());
		}
        
		return $scripts;
	}
	
	function getStyles() {
		$styles = array('css/list.css');
		foreach($this->getActionLinks() as $link) {
			$styles = array_merge($styles, $link->getStyles());
		}
		return $styles;
	}
	
    public function __construct() {
        $this->setItemPresenter(with(new DefaultItemPresenter)->setViewName('clearwebapps::grid_item'));
    }
    
	public function init() {}
    
	public function execute() {
        foreach($this->getActionLinks() as $link) {
            $link->init();
		}
    }
}