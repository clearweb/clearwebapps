<?php namespace Clearweb\Clearwebapps\Lists;

use Clearweb\Clearworks\Contracts\IViewable;

abstract class ItemPresenter implements IViewable
{
    private $data         = array();
    private $columns      = array();
    private $actionLinks  = array();
    private $index        = 0;
    private $selected     = false;
    private $totalCount   = 0;
    private $listItemName = '';
    private $langPrefix   = 'app';
    
    /**
     * Gets the index of the item. Index starts from 1.
     */
    public function getIndex()
    {
        return $this->index;
    }
    
    public function setIndex($index)
    {
        $this->index = $index;
        
        return $this;
    }
    
    public function getTotalCount()
    {
        return $this->totalCount;
    }
    
    public function setTotalCount($totalCount)
    {
        $this->totalCount = $totalCount;
        
        return $this;
    }
    
    public function getListItemName()
    {
        return $this->listItemName;
    }
    
    public function setListItemName($listItemName)
    {
        $this->listItemName = $listItemName;
        
        return $this;
    }
    
    public function getSelected()
    {
        return $this->selected;
    }
    
    public function setSelected($selected)
    {
        $this->selected = $selected;
        
        return $this;
    }
    
    public function getItemData()
    {
        return $this->data;
    }
    
    public function setItemData(array $data)
    {
        $this->data = $data;
        
        return $this;
    }
    
    public function getColumns()
    {
        return $this->columns;
    }
    
    public function setColumns(array $columns)
    {
        $this->columns = $columns;
        
        return $this;
    }
    
    public function getActionLinks()
    {
        return $this->actionLinks;
    }
    
    public function setActionLinks(array $actionLinks)
    {
        $this->actionLinks = $actionLinks;
        
        return $this;
    }
    
    /**
     * Sets the prefix for translation of the columns trans([prefix].[colname]);
     * @param string $lang_prefix the prefix
     */
    public function setLangPrefix($lang_prefix) {
		$this->lang_prefix = $lang_prefix;
		
		return $this;
	}
    
    /**
     * gets the prefix for translation of the columns trans([prefix].[colname]);
     * @return string the prefix
     */
	public function getLangPrefix() {
		return $this->lang_prefix;
	}
    
    abstract public function getView();
}