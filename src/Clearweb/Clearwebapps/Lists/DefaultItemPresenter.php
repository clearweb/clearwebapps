<?php namespace Clearweb\Clearwebapps\Lists;

use View;

class DefaultItemPresenter extends ItemPresenter
{
    private $viewName = '';
    
    public function getView()
    {
        return View::make($this->getViewName())
            ->with('columns', $this->getColumns())
            ->with('item', $this->getItemData())
			->with('actionLinks', $this->getActionLinks())
            ->with('selected', $this->getSelected())
            ->with('index', $this->getIndex())
            ->with('totalCount', $this->getTotalCount())
            ->with('listItemName', $this->getListItemName())
            ->with('langPrefix', $this->getLangPrefix())
            ;
    }

    public function getViewName()
    {
        return $this->viewName;
    }
    
    public function setViewName($viewName)
    {
        $this->viewName = $viewName;
        
        return $this;
    }
    
    public function getStyles()
    {
        return array();
    }
    
    public function getScripts()
    {
        return array();
    }
}