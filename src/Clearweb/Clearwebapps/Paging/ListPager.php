<?php namespace Clearweb\Clearwebapps\Paging;

use Clearweb\Clearworks\Contracts\IViewable;
use Paginator;

class ListPager implements IViewable
{
	private $number_pages   = 1;
	private $selected_page  = 1;
	private $items_per_page = 20;
	private $paging_parameter = 'page';
	
	public function getPagingParameter()
	{
		return $this->paging_parameter;
	}
	
	public function setPagingParameter($paging_parameter)
	{
		$this->paging_parameter = $paging_parameter;
		
		return $this;
	}
		
	public function setItemsPerPage($items_per_page)
	{
		$this->items_per_page = $items_per_page;
		return $this;
	}
	
	public function getItemsPerPage()
	{
		return $this->items_per_page;
	}
	
	public function setNumberPages($number_pages)
	{
		$this->number_pages = $number_pages;
		return $this;
	}
	
	public function getNumberPages()
	{
		return $this->number_pages;
	}
	
	public function setSelectedPage($selected_page)
	{
		$this->selected_page = $selected_page;
		return $this;
	}
	
	public function getSelectedPage()
	{
		return $this->selected_page;
	}
	
	public function getView() {
		$number_pages  = $this->getNumberPages();
		$selected_page = min($this->getSelectedPage(), $number_pages);
        
        Paginator::setCurrentPage($this->getSelectedPage());
		$paginator = Paginator::make(array(), $this->getNumberPages() * $this->getItemsPerPage(), $this->getItemsPerPage());
        $paginator->fragment($this->getPagingParameter());
        
        return $paginator->links('clearwebapps::paging');
	}
	
	public function getScripts() {
		return array();
	}
	
	public function getStyles() {
		return array();
	}
}
