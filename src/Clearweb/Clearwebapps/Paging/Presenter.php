<?php namespace Clearweb\Clearwebapps\Paging;

use Clearweb\Clearworks\Action\ScriptActionButton;
use Clearweb\Clearworks\Communication\ParameterChanger;
use Illuminate\Pagination\Paginator;

class Presenter extends \Illuminate\Pagination\Presenter
{
	private $paging_parameter = 'page';

    public function __construct(Paginator $pagination)
    {
        parent::__construct($pagination);
        
        $this->setPagingParameter($pagination->fragment());
    }
	
	public function getPagingParameter()
	{
		return $this->paging_parameter;
	}
	
	public function setPagingParameter($paging_parameter)
	{
		$this->paging_parameter = $paging_parameter;
		
		return $this;
	}
	
	public function getActivePageWrapper($text)
    {
        return '<li class="active current"><span>'.$text.'</span></li>';
    }

    public function getDisabledTextWrapper($text)
    {
        return '<li class="disabled unavailable"><span>'.$text.'</span></li>';
    }
	
    public function getPageLinkWrapper($url, $page, $rel = null)
    {
		$link = with(new ScriptActionButton)
			->setScriptAction(
							  with(new ParameterChanger)
							  ->setParameter($this->getPagingParameter(), $page)
							  )
			->setTitle($page)
			
			;
        return '<li>'.$link->getView().'</li>';
    }

	/**
	 * Get the previous page pagination element.
	 *
	 * @param  string  $text
	 * @return string
	 */
	public function getPrevious($text = '&laquo;')
	{
		// If the current page is less than or equal to one, it means we can't go any
		// further back in the pages, so we will render a disabled previous button
		// when that is the case. Otherwise, we will give it an active "status".
		if ($this->currentPage <= 1)
		{
			return $this->getDisabledTextWrapper($text);
		}
		else
		{
			$link = with(new ScriptActionButton)
				->setScriptAction(
								  with(new ParameterChanger)
								  ->setParameter($this->getPagingParameter(), $this->currentPage - 1)
								  )
				->setTitle($text)
				
				;
			return '<li>'.$link->getView().'</li>';
		}
	}

	/**
	 * Get the next page pagination element.
	 *
	 * @param  string  $text
	 * @return string
	 */
	public function getNext($text = '&raquo;')
	{
		// If the current page is greater than or equal to the last page, it means we
		// can't go any further into the pages, as we're already on this last page
		// that is available, so we will make it the "next" link style disabled.
		if ($this->currentPage >= $this->lastPage)
		{
			return $this->getDisabledTextWrapper($text);
		}
		else
		{
			$link = with(new ScriptActionButton)
				->setScriptAction(
								  with(new ParameterChanger)
								  ->setParameter($this->getPagingParameter(), $this->currentPage + 1)
								  )
				->setTitle($text)
				
				;
			return '<li>'.$link->getView().'</li>';
		}
	}
}