<?php namespace Clearweb\Clearwebapps\Page;

use Clearweb\Clearworks\Action\IActionButton;

use Clearweb\Clearwebapps\Widget\PageHeadWidget;
use Event;

class Page extends \Clearweb\Clearworks\Page\LinearPage {
	private $title   = '';
    private $heading = '';
    private $actions = array();
    
	public function init()
	{
        $title = $this->getHeading();
        if (empty($title)) $title = $this->getTitle();
        
		$this->addWidgetLinear(\App::make('menu')->get('admin-sidebar'), 'sidebar')
            ->addWidgetLinear(
                              with(new PageHeadWidget)->setActions($this->getActions())
                              ->setTitle($this->getTitle())
                              ,
                              'top',
                              1
                              );
        
        Event::fire('clearwebapps.theme', array('page' => $this));
        
		return parent::init();
	}
	
	/**
	 * returns actions of the page
	 * @return Array with elements of \Clearweb\Clearworks\Action\IActionButton
	 */
	public function getActions() {
		return $this->actions;
	}
    
    /**
	 * Sets actions of the page
	 * @param Array $actions with elements of \Clearweb\Clearworks\Action\IActionButton
     * @retun $this
	 */
	public function setActions(array $actions) {
		$this->actions = $actions;
        
        return $this;
	}
    
    /**
     * Adds an action to the page actions
     * @param \Clearweb\Clearworks\Action\IActionButton $action the action
     * @return $this
     */
    public function addAction($name, IActionButton $action, $position=0) {
        $actionArray = array('name' => $name, 'action' => $action);
        
        if ($position > 0) {
            $this->actions = array_merge(array_slice($this->actions, 0, $position-1), array($actionArray), array_slice($this->actions, $position-1));
        } else {
            $this->actions[] = $actionArray;
        }
    }
    
    /**
     * Removes a named action link from the links
     * @param string $name the name of the link
     * @return $this
     */
    function removeAction($name) {
        foreach($this->actions as $i => $action) {
            if ($action['name'] == $name) {
                unset($this->actions[$i]);
            }
        }
        
        return $this;
    }
    
    /**
     * Sets the page title
     * @param string $title the title
     * @return $this
     */
	public function setTitle($title) {
		$this->title = $title;
        
        return $this;
	}
	
    /**
     * Gets the page title
     * @return the title
     */
	public function getTitle() {
		if (empty($this->title)) {
			return 'Remember to set the title';
		} else {
			return $this->title;
		}
	}
        
    /**
     * sets the page heading (the page head text)
     * @param string $heading the heading
     * @return this
     */
	public function setHeading($heading) {
		$this->heading = $heading;
        
        return $this;
	}

    /**
     * Gets the page heading (the page head text)
     * @return the heading
     */
	public function getHeading() {
        return $this->heading;
	}
}