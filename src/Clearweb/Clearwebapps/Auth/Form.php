<?php namespace Clearweb\Clearwebapps\Auth;

use Clearweb\Clearwebapps\Form\FieldFactory;

class Form extends \Clearweb\Clearwebapps\Form\Form {
	public function init() {
		parent::init();
	
		$env = \Clearworks::getCurrentEnvironment();
		$env_url = \Clearworks::getEnvironmentUrl($env);
		
		$this->setAction(\Clearworks::getPageUrl(new Page));
		$this->addField(FieldFactory::make('username', 'text'));
		$this->addField(FieldFactory::make('password', 'password'));
		$this->addField(FieldFactory::make('redirect', 'hidden', $env_url));
		$this->addField(FieldFactory::make('submit', 'submit'));
		return $this;
	}

}