<?php namespace Clearweb\Clearwebapps\Auth;

use Clearweb\Clearworks\Page\PageInterface;

use Illuminate\Events\Dispatcher;
use Auth;

class Environment extends \Clearweb\Clearworks\Environment {
    private $authPage = null;
    
    public function __construct(Dispatcher $dispatcher)
    {
        parent::__construct($dispatcher);
        $this->setAuthPage(new Page);
    }

    public function init() {
        $this->addPage($this->getAuthPage());

        return parent::init();
    }
    
    /**
     * Set the page you see when a user is not logged in.
     */
    public function setAuthPage(PageInterface $page) {
        $this->authPage = $page;

        return $this;
    }
	
    public function getAuthPage()
    {
        return $this->authPage;
    }
	
    protected function deducePageFromUrl($url='') {
        if ($this->checkRights()) {
            return parent::deducePageFromUrl($url);
        } else {
            return $this->getAuthPage();
        }
    }

    protected function checkRights() {
        return (Auth::check() && Auth::user()->hasRole('admin-login'));
    }
}