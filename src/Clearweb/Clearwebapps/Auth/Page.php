<?php namespace Clearweb\Clearwebapps\Auth;

use Clearweb\Clearwebapps\Form\Validator;
use Clearweb\Clearwebapps\User\User;
use Clearweb\Clearwebapps\Content\Notice;

use Clearweb\Clearworks\Content\Container;
use Clearweb\Clearworks\Content\RawHTML;
use Clearweb\Clearworks\Action\Redirect;

use Auth;

class Page extends \Clearweb\Clearworks\Page\SlugPage {
	private $content_container = null;
	private $validator = null;
	
	public function __construct() {
		$this->setSlug('password');
	}
	
	function getStyles() {
		return array('css/style.css', 'css/login.css');
	}
	
	function getScripts() {
		return array();
	}
	
	function init() {
		parent::init();
		$this->loadValidator();
	}
	
	protected function loadValidator() {
		$validator = new Validator();
		$validator->addExtension('checkLogin', function($attribute, $value, $parameters) use ($validator) {
				$data = $validator->getData();
				$auth = false;
				
				if (isset($parameters[0]) && isset($data[$parameters[0]])) {
					$password = $data[$parameters[0]];
					$auth =  Auth::validate(array('email'=>$value, 'password'=>$password));
                    
                    if ($auth) {
                        $user = Auth::getProvider()->retrieveByCredentials(['email'=>$value, 'password'=>$password]);
                        
                        $auth = $user->hasRole('admin-login');
                    }
				}
				
				return $auth;
			});
		
		$validator->setRules(array(
								   'username' => 'required|checkLogin:password',
								   'password' => 'required',
								   ));
		$this->setValidator($validator);
	}
	
	public function getValidator() {
		return $this->validator;
	}
	
	public function setValidator(Validator $validator) {
		$this->validator = $validator;
		return $this;
	}
	
	function execute() {
		parent::execute();

		$this->content_container = new Container;
		$env = \Clearworks::getCurrentEnvironment();
		$env_url = \Clearworks::getEnvironmentUrl($env);
		
		if ($this->getParameter('action') == 'logout') {
			\Auth::logout();
			
			$this->content_container->addViewable(
												  with(new Redirect)
												  ->setLocation($env_url)
												  );
		} else {
			
			$validator = $this->getValidator();
			$validator->setData($_POST);
			
			if (empty($_POST)) {
                $this->content_container->addViewable(
                                                      with(new RawHTML)
                                                      ->setHTML(trans('clearwebapps::auth.no_rights'))
                                                      );
            } else {
                if($validator->passes()) {
                    $user = User::where('email', '=', $_POST['username'])->first();
                    
                    \Auth::login($user);
                    
                    $this->content_container->addViewable(
                                                          with(new Redirect)
                                                          ->setLocation($env_url)
                                                          );
                    
                } else {
                    $this->content_container->addViewable(new Notice(trans('clearwebapps::auth.access_denied')));
                }
                
            }
            
            $form = new Form;
            $form->init();
            $form->execute();
            

            $this->content_container->addViewable($form);
            
		}
		
		return $this;
	}
	
	/**
	 * @pre content_container is defined
	 */
	function getView() {
		$html = '<head>';
		$html .= '<link rel="stylesheet" href="/css/style.css" type="text/css"/></head><body class="login">';
		$html .= $this->content_container->getView();
		$html .= '</body>';
		return $html;
	}
	
}