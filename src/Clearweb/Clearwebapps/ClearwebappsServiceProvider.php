<?php namespace Clearweb\Clearwebapps;

use Illuminate\Support\ServiceProvider;

//use Illuminate\View\Factory;

use Clearweb\Clearwebapps\Auth\Environment;
use Clearweb\Clearwebapps\Theme\ViewFileFinder;
use Clearweb\Clearwebapps\Theme\ViewFactory;
use Clearweb\Clearwebapps\Theme\ThemeHandler;
use Clearweb\Clearwebapps\Menu\MenuHandler;

use Event;
use Config;
use View;

class ClearwebappsServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->package('clearweb/clearwebapps');
        
        $this->app->booting(function() {
                $loader = \Illuminate\Foundation\AliasLoader::getInstance();
                $loader->alias('FileManager', 'Clearweb\Clearwebapps\File\FileManager');
                $loader->alias('Settings', 'Clearweb\Clearwebapps\Setting\SettingManager');
                $loader->alias('Menu', 'Clearweb\Clearwebapps\Menu\Menu');
                $loader->alias('Theme', 'Clearweb\Clearwebapps\Theme\ThemeHandler');
            
                Event::fire('clearwebapps.booted');
            });
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
        /* make an admin env */
        $adminEnv = with(new Environment(Event::getFacadeRoot()))
            ->setName('admin')
            ->setSlug('admin')
            ;
        
        $clearworks = $this->app->make('clearworks');
        $clearworks->addEnvironment($adminEnv);
        
        
        
        /* register menu */
        $menu = new MenuHandler;
        $this->app['menu'] = $this->app->share(
                                               function() use($menu)
                                               {
                                                   return $menu;
                                               }
                                               );
        
        
        /* register theming */
        $theme = new ThemeHandler;
        $this->app['theme'] = $this->app->share(
                                                function() use($theme)
                                                {
                                                    return $theme;
                                                }
                                                );
        
        
        $this->registerViewFactory($theme);
        
        Event::listen('env.init', function() use ($theme) {
                if (\Theme::getTheme()) {
                    \Theme::getTheme()->init();
                }
            });
	}
    
    public function registerViewFactory($theme)
    {
        //$oldFileFinder = View::getFinder();
                
        $themeName = Config::get('app.theme');
        if($themeName && ! $theme->getTheme()) {
            $theme->selectTheme($themeName);
        }
        
		$this->app->bindShared('view', function($app) 
		{
			$oldFileFinder = $app['view.finder'];
            $finder = new ViewFileFinder($oldFileFinder->getFileSystem(), $oldFileFinder->getPaths(), $oldFileFinder->getExtensions());
            $resolver = $app['view.engine.resolver'];
            
			$env = new ViewFactory($resolver, $finder, $app['events']);
            
			// We will also set the container instance on this view environment since the
			// view composers may be classes registered in the container, which allows
			// for great testable, flexible composers for the application developer.
			$env->setContainer($app);

			$env->share('app', $app);

			return $env;
		});
        
        
    }

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array();
	}

}