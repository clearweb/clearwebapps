<?php namespace Clearweb\Clearwebapps\Bootstrap;

use Clearweb\Clearworks\Layout\BasicLayout;
use Clearweb\Clearworks\Widget\WidgetInterface;

use Clearweb\Clearwebapps\Bootstrap\IRow;
use Clearweb\Clearwebapps\Bootstrap\Row;


use View;
use Theme;

class Layout extends BasicLayout implements IRowContainer{
    private $rows = [];
    private $title = '';
    private $viewName = 'clearwebapps::bootstrap';
    
    public function __construct() {
        $this
            ->addScript('jquery', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js')
            ->addScript('jquery-ui', 'https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.0/external/jquery/jquery.min.js', array('jquery'))
            
            ->addScript('bootstrap', 'https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js', ['jquery'])
            
            
            
            ->addStyle('https://ajax.aspnetcdn.com/ajax/jquery.ui/1.10.4/themes/blitzer/jquery-ui.min.css')
            ->addStyle('https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css')
            
            ;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }
    
    public function getViewName()
    {
        return $this->viewName;
    }

    public function setViewName($viewName)
    {
        $this->viewName = $viewName;
        
        return $this;
    }
    
    public function getRowsView()
    {
        $html = '';
        foreach($this->getRows() as $row) {
            $html .= $row->getView();
        }

        return $html;
    }
    
    public function getView()
    {
		return \View::make($this->getViewName())
            ->with('title', $this->getTitle())
            ->with('styles', $this->getStyles())
            ->with('scripts', $this->getScripts())
			->with('rows', $this->getRowsView())
			;
    }

    /*
    public function getCompatibleLocationClasses()
    {
        return ['Clearweb\Clearwebapps\Bootstrap\WidgetLocation'];
    }
    */

    /** 
     * Makes and adds a row then returns it.
     * @param integer $id the id of the row
     * @return \Clearweb\Clearworks\Bootstrap\IRow
     */
    public function newRow($id)
    {
        $row = $this->makeRow();
        $row->setId($id);

        $this->addRow($row);
        
        return $row;
    }

    public function makeRow()
    {
        $row = new Row;
        return $row;
    }
    
    public function addRow(IRow $row)
    {
        $this->rows[] = $row;
        return $this;
    }

    public function deleteRow($id)
    {
        foreach($this->rows as $i => $row) {
            if ($row->getId() == $id) {
                unset($this->rows[$i]);
            }
        }
    }

    public function getRows()
    {
        return $this->rows;
    }

    public function setRows(array $rows)
    {
        $this->rows = $rows;
        
        return $this;
    }

    public function getWidgets()
    {
        $widgets = [];
        
        foreach($this->getRows() as $row) {
            foreach($row->getViewables() as $viewableArray) {
                $viewable = $viewableArray['viewable'];
                if ($viewable instanceof Column) {
                    foreach($viewable->getViewables() as $widget) {
                        $widgets[] = $widget['viewable'];
                    }
                } else {
                    if ($viewable instanceof WidgetInterface) {
                        $widgets[] = $viewable;
                    }
                }
            }
            
        }
        
        return $widgets;
    }
}