<?php namespace Clearweb\Clearwebapps\Bootstrap;

use Clearweb\Clearworks\Contracts\IViewable;
use Clearweb\Clearworks\Widget\WidgetInterface;

use Clearweb\Clearworks\Content\Container;

class Row extends Container implements IRow
{
    protected $hasContainer = false;
    protected $widgets = [];
    
    public function __construct()
    {
        $this->addClass('row');
    }
    
    public function getHasContainer()
    {
        return $this->hasContainer;
    }
    
    public function setHasContainer($hasContainer)
    {
        $this->hasContainer = $hasContainer;
        return $this;
    }
    
    public function addWidget(WidgetInterface $widget, array $columnModifiers)
    {
        $nrSizes = 0;
        $column = new Column;
        $column->addViewable($widget);
        
        foreach($columnModifiers as $modifier) {
            if (starts_with($modifier, 'col-')) {
                $column->addClass($modifier);
                $nrSizes++;
            } else {
                $column->addClass($modifier);
            }
        }

        if ( ! $nrSizes) {
            throw new ColumnSizesNotDefinedException('There should be at least 1 column size defined and column sizes should always start with "col-"');
        }
        
        return $this->addViewable($column);
    }
}