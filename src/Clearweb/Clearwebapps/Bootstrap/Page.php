<?php namespace Clearweb\Clearwebapps\Bootstrap;

use Clearweb\Clearworks\Page\LayoutPage;

class Page extends LayoutPage implements IRowContainer {
    private $title   = '';

    /**
     * Sets the page title
     * @param string $title the title
     * @return $this
     */
	public function setTitle($title) {
		$this->title = $title;
        
        return $this;
	}
	
    /**
     * Gets the page title
     * @return the title
     */
	public function getTitle() {
        return $this->title;
	}
    
    protected function getCompatibleLayoutClasses() {
		return array('Clearweb\Clearwebapps\Bootstrap\Layout');
    }
    
    public function init() {
        parent::init();
        foreach($this->getWidgets() as $widget) {
            $widget->setParameters($this->getParameters());
            $widget->__init();
        }
    }
	
    public function execute() {
        
        parent::execute();
        foreach($this->getWidgets() as $widget) {
            $widget->__execute();
        }
    }
    
    public function getView()
    {
        $this->getLayout()->setTitle($this->getTitle());
        return parent::getView();
    }
    
    /* -- function to redirect to current bootstrap layout -- */

    public function makeRow()
    {
        return $this->getLayout()->makeRow();
    }

    public function setRows(array $rows)
    {
        return $this->getLayout()->setRows($rows);
    }

    public function getRows()
    {
        return $this->getLayout()->getRows();
    }

    public function deleteRow($id)
    {
        return $this->getLayout()->delteRow($id);
    }
    
    public function newRow($id)
    {
        return $this->getLayout()->newRow($id);
    }
    
    public function addRow(IRow $row)
    {
        return $this->getLayout()->addRow($row);
    }

    public function getWidgets()
    {
        return $this->getLayout()->getWidgets();
    }
}