<?php namespace Clearweb\Clearwebapps\Bootstrap;

use Clearweb\Clearworks\Contracts\IViewable;
use Clearweb\Clearworks\Widget\WidgetInterface;

interface IRow extends IViewable
{
    public function getHasContainer();
    public function setHasContainer($hasContainer);
    public function addWidget(WidgetInterface $widget, array $sizes);

    public function setID($id);
    public function getID();
}