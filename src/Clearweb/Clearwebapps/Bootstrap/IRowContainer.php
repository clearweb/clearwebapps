<?php namespace Clearweb\Clearwebapps\Bootstrap;

interface IRowContainer
{
    /** 
     * Makes and adds a row then returns it.
     * @param integer $id the id of the row
     * @return \Clearweb\Clearworks\Bootstrap\IRow
     */
    public function newRow($id);
    
    public function addRow(IRow $row);
    
    public function deleteRow($id);

    public function makeRow();
    
    public function getRows();
    
    public function setRows(array $rows);

}