<?php
return array(
             'name' => 'Volledige naam',
             'email' => 'E-mailadres',
             'image' => 'Profielfoto',
             
             'account' => 'Account',
             'logout' => 'Afmelden',
             
             'profile' => 'Profiel',
             
             'user' => 'gebruiker|gebruikers',
             'new_password' => 'Nieuwe wachtwoord',
             'password-desc' => 'Vul dit veld in als u uw wachtwoord wil wijzigen. Laat het leeg wanneer u dat niet wil.',
             )
;