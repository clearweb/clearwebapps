<?php

return array(
			 'item_list' => ':itemlijst',
             'unknown_related_item' => '-- :item-name onbekend --',
             
			 /* delete action (+ link) */
			 'delete' => 'verwijderen',
			 'delete_item' => ':item verwijderen',
			 'delete_confirm' => 'Weet u zeker dat u :item-type \':item-name\' wilt verwijderen?',
			 );