<?php echo View::make('clearwebapps::header', compact('stylesheets', 'javascripts')); ?>
<div id="sidebar">
	 <?php echo $sidebar;?>
</div>
<!-- #sidebar -->
<div id="content">
	 <div id="top">
	 	<?php echo $top;?>
	 </div>
	 <div id="cols">
		 <div id="col1">
		 <?php echo $col1;?>
		 </div>
		 <!-- #col1 -->
		 <div id="col2">
		 <?php echo $col2;?>
		 </div>
		 <!-- #col2 -->
		 <div id="col3" class="sexy-sidebar">
		 <?php echo $col3;?>
		 </div>
		 <!-- #col3 -->
	 </div>
</div>
<!-- #content -->
<?php echo View::make('clearwebapps::footer'); ?>