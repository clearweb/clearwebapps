<?php

if ( ! function_exists('menuShowChildren')) {
    function menuShowChildren($links) {
        foreach($links as $link):
            menuShowItem($link);
        endforeach;
    }
}

if ( ! function_exists('menuShowItem')) {
    function menuShowItem($link) {
        
        $url_postfix = '';
        if (isset($link['params'])) {
            foreach($link['params'] as $key => $value) {
                $url_postfix .= "/$key/$value";
            }
        }
        
        if (preg_match('#([^/]+/)*([^/]+)#', $link['url'], $matches)) {
            $link_class = $matches[2];
        } else {
            $link_class = $link['url'];
        }
        
    	$menu_class = str_replace(' ','-', strtolower( $link_class )); 
    ?>

    <li>
    
    <a class="<?php echo $link_class . ' ';?><?php echo ($link['selected'])?'selected':''; ?>" href="<?php echo $link['url'].$url_postfix; ?>"><?php echo $link['label']; ?></a>
    
        <?php if ($link['children']): ?>
        <ul>
        <?php
        menuShowChildren($link['children']);
        ?>
        </ul>
        <?php endif; ?>
        
        </li>
    <?php
    }
}


?>
<nav id="<?php echo $id; ?>">
  <ul class="nav">
    <?php
       foreach($links as $link):
       menuShowItem($link);
       endforeach;
       ?>
  </ul>
</nav>
<?php if ( ! empty($id) && $mobile): ?>
<a class="menu-toggle" href="#<?= $id ?>">Menu</a>
<script>
  $(document).ready(function() {
  $('#<?= $id ?>').mmenu(
  <?= json_encode($mobileOptions) ?>,
  <?= json_encode($mobileConfig) ?>
  );
  });
</script>
<?php endif; ?>
