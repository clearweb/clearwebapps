<?php if ($index == 1): ?>
<script type="text/javascript">
	 /* TODO remove ALL communication widget specific code */
	 
	function listSelectItem(id, list_type) {
		$('.list[data-listtype="' + list_type + '"] .list-item').removeClass('selected');
		$('.list[data-listtype="' + list_type + '"] .list-item[data-itemid="' + id + '"]').addClass('selected');
	}

	subscribeToPageState('<?php echo $listItemName;?>',
						function(newVal) {
							listSelectItem(newVal, '<?php echo $listItemName;?>');
						});
</script>
<table class="list" data-listtype="<?php echo $listItemName;?>">
  <tr class="list-head">
    <?php foreach($columns as $column): ?>
    <td data-columnname="<?php echo $column['name']; ?>">
      <?php echo ucfirst(trans_choice($langPrefix.'.'.$column['name'], 1)); ?>
    </td>
    <?php endforeach; ?>

    <?php if( ! empty($actionLinks)): ?>
    <?php foreach($actionLinks as $link): ?>
    <td class="action-column"></td>
    <?php endforeach; ?>
    <?php endif; ?>
  </tr>
<?php endif; ?>

<tr class="<?php echo($selected)?'selected':'';?> list-item" data-itemid="<?php echo (isset($item['id'])?$item['id']:''); ?>">
<?php foreach($columns as $column):?>
	<td>
	<?php
	try{
		$callback = $column['callback'];
		echo call_user_func($callback, $item);
	} catch(Exception $e) {
		if (Config::get('app.debug'))
			echo ('Error in view '.__FILE__.':'.__LINE__.', the callback for column "'.$column['name'].'" gives error for values: '.print_r($item, true).'. The error is: <br />'.$e->getMessage());
	}
	?>
	</td>
<?php endforeach;?>
<?php if( ! empty($actionLinks)): ?>
	<?php
	foreach($actionLinks as $link):
		?>
		<td class="action-column">
		<?php
		try{
			$link->setParameters($item);
			$link->execute();
			echo $link->getView();
		} catch(Exception $e) {
			if (Config::get('app.debug'))
				echo ('Error in view '.__FILE__.':'.__LINE__.', the execution of action "'.get_class($link).'" gives error: <br/>'.$e->getMessage());
		}
		?>
		</td>
		<?php
	endforeach;
	?>
<?php endif; ?>
</tr>


<?php if ($index == $totalCount): ?>
</table>
<?php endif; ?>

