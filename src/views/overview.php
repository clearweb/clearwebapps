<?php echo View::make('clearwebapps::header', compact('stylesheets', 'javascripts')); ?>
<div id="sidebar">
	 <?php echo $sidebar;?>
</div>
<!-- #sidebar -->
<div id="content">
	 <div id="top">
	 	<?php echo $top;?>
	 </div>
	 <!-- #top -->
	 <div id="left_info">
	 <?php echo $left_info;?>
	 </div>
	 <!-- #left_info -->
	 <div id="right_info" class="sexy-sidebar">
	 <?php echo $right_info;?>
	 </div>
	 <!-- #right_info -->
</div>
<!-- #content -->
<?php echo View::make('clearwebapps::footer'); ?>