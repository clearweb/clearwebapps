<nav id="<?php echo $id; ?>">
  <ul class="nav">
    <?php
    foreach($items as $item):
        echo $item->getView();
    endforeach;
    ?>
  </ul>
</nav>
<?php if ( ! empty($id) && $mobile): ?>
<a class="menu-toggle" href="#<?= $id ?>">Menu</a>
<script>
  $(document).ready(function() {
  $('#<?= $id ?>').mmenu(
  <?= json_encode($mobileOptions) ?>,
  <?= json_encode($mobileConfig) ?>
  );
  });
</script>
<?php endif; ?>