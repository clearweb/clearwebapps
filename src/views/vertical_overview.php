<?php echo View::make('clearwebapps::header', compact('stylesheets', 'javascripts')); ?>
<div id="sidebar">
	 <?php echo $sidebar;?>
</div>
<!-- #sidebar -->
<div id="content">
	 <div id="top">
	 	<?php echo $top;?>
	 </div>
	 <!-- #top -->
	 <div id="middle">
	 <?php echo $middle;?>
	 </div>
	 <!-- #left_info -->
	 <div id="bottom">
	 <?php echo $bottom;?>
	 </div>
	 <!-- #right_info -->
</div>
<!-- #content -->
<?php echo View::make('clearwebapps::footer'); ?>