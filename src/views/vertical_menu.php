<ul id="<?php echo $id; ?>" class="nav">
<?php
foreach($links as $link):
	 $url_postfix = '';
	 if (isset($link['params'])) {
		 foreach($link['params'] as $key => $value) {
			 $url_postfix .= "/$key/$value";
		 }
	 }
?>

<?php 
/*
	$link_class =  str_replace('/admin/','',$link['url']);
	$menu_class = str_replace(' ','-', strtolower( $link_class )); 
*/
if (preg_match('#([^/]+/)*([^/]+)#', $link['url'], $matches)) {
    $link_class = $matches[2];
} else {
    $link_class = $link['url'];
}

	$menu_class = str_replace(' ','-', strtolower( $link_class )); 
?>

<li><a class="<?php echo $link_class . ' ';?><?php echo ($link['selected'])?'selected':''; ?>" href="<?php echo $link['url'].$url_postfix; ?>"><?php echo $link['name']; ?></a></li>
<?php
endforeach;
?>
</ul>