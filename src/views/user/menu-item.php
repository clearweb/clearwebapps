<li class="nav-header">
  <a href="/admin/account" class="account">
    <div>
      <img alt="image" class="img-circle" src="<?= empty($profileImage)?'/img/no-avatar.jpg':$profileImage ?>">
    </div>
    <div class="block m-t-xs"><strong class="font-bold"><?= $user->name ?></strong></div>
    <div class="text-muted text-xs block"><?= $user->email ?> <b class="caret"></b></div>
  </a>
</li>
