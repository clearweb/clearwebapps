<?php include('header.php'); ?>
<div id="sidebar">
	 <?php echo $sidebar;?>
</div>
<!-- #sidebar -->
<div id="content">
	 <div id="top">
	 	<?php echo $top;?>
	 </div>
	 <!-- #top -->
	 <div id="main">
	 <?php echo $content;?>
	 </div>
	 <!-- #content -->
</div>
<!-- #content -->
<?php include('footer.php'); ?>