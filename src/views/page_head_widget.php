<div class="widget">
  <div class="page-head">
    <h1><?php echo $title; ?></h1>
    <div class="page-operations operations">
      <?php
	 foreach($actions as $action):
	 $action['action']->init();
      $action['action']->execute();
      echo $action['action']->getView();
      endforeach;
      ?>
    </div>
  </div>
</div>
