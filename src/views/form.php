<?php
switch ($method) {
case \Clearweb\Clearwebapps\Form\Form::METHOD_GET:
	$method_html = 'GET';
	break;
case \Clearweb\Clearwebapps\Form\Form::METHOD_POST:
default:
	$method_html = 'POST';
	break;
}
?>
<div class="widget-body">
<form action="<?php echo $action; ?>" method="<?php echo $method_html; ?>">
<?php echo $fields; ?>
</form>
</div>