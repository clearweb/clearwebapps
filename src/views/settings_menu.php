<h3><?php echo trans('clearwebapps::settings.settings'); ?></h3>
<ul>
<?php foreach($packages as $package): ?>
  <li><a href="<?php echo $package->getUrl(); ?>"><?php echo $package->getTitle(); ?></a></li>
<?php endforeach; ?>
</ul>