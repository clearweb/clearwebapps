</div>
<div id="footer">
	<div class="version-box">
<?php echo ucfirst(trans('clearwebapps::layout.version')); ?> <?php echo Config::get('app.version', '?'); ?>
	</div>
	<div class="center-box"></div>
	<div class="credits-box">
		powered by <a  target="_blank" href="http://www.clearweb.nl">Clearweb</a>
	</div>

</div>
</div>
<!-- #page-wrapper -->
</body>
</html>