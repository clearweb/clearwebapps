<label><?php echo $label; ?></label>
<?php if ( ! empty($description)): ?>
  <p><?php echo $description; ?></p>
<?php endif; ?>
<input type="text" name="<?php echo $name; ?>" value="<?php echo $value; ?>"
       placeholder="<?php echo $placeholder; ?>" class="<?php echo $class; ?>"
       <?php echo ($disabled)?'disabled="disabled"':''; ?> 
       />
