<html>
  <head>
    <title>{{ $title }}</title>
    
    {{ Theme::styles($styles)}}
    {{ Theme::scripts($scripts)}}
  </head>
  <body>
    {{ $rows }}
  </body>
</html>
