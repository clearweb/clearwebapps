<label><?php echo $label; ?></label>
<?php if ( ! empty($description)): ?>
  <p><?php echo $description; ?></p>;
<?php endif; ?>
<input type="text" name="<?php echo $name; ?>" value="<?php echo $value; ?>"
       placeholder="<?php echo $placeholder; ?>" class="colorpicker-text"
       <?php echo ($disabled)?'disabled="disabled"':''; ?> 
       />
<input class="colorpicker" style="background-color: <?php echo $value; ?>" 
     <?php echo ($disabled)?'disabled="disabled"':''; ?> 
     />