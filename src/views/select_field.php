<?php
$option_html = implode(
                       '',
                       array_map(
                                 function($value, $label) use($selected) {
                                     return '<option '.
                                     (($value==$selected)?'selected="selected"':'').
                                     ' value="'.$value.'">'
                                     .$label
                                     .'</option>';
                                 },
                                 array_keys($options),
                                 $options 
                                 )
                       );
?>
<label><?php echo $label; ?></label>
<?php if ( ! empty($description)): ?>
  <p><?php echo $description; ?></p>
<?php endif; ?>
<select class="chosen-select" name="<?php echo $name.($multiple?'[]':''); ?>"
     data-placeholder="<?= $placeholder ?>" 
     <?php echo $multiple?'multiple="multiple"':''; ?> 
>
  <?php foreach($options as $value => $label): ?>
  <option value="<?= $value ?>"
    <?php
    $selectedArray = explode(';', $selected);
    $showSelect = false;
    
    foreach($selectedArray as $selectedOption) {
        if ($value == $selectedOption) {
            $showSelect = true;
        }
    }
    if ($showSelect) { echo ' selected="selected" '; }
    ?>
    >
    <?= $label ?>
  </option>
<?php endforeach; ?>
</select>
<script>
$('.chosen-select').chosen({disable_search_threshold: 10});
</script>