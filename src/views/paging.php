<?php
	$presenter = new Clearweb\Clearwebapps\Paging\Presenter($paginator);
?>
<?php if ($paginator->getLastPage() > 1): ?>
<ul class="pager">
			<?php echo $presenter->render(); ?>
</ul>
<?php endif; ?>