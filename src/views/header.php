<html>
<head>
<!-- <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/headjs/1.0.3/head.load.min.js"></script> -->

<?php
if (true):
    $hasFormWidget = false;
    foreach($javascripts as $javascript) {
        if ($javascript['name'] == 'form_widget') {
            $hasFormWidget = true;
        }
    }

    if ( ! $hasFormWidget) {
        $javascripts[] = [
                          'name' => 'form_widget',
                          'url'=>'packages/clearweb/clearwebapps/js/form_widget.js',
                          'dependencies' => ['jquery']
                          ];
    }
    
    Theme::styles($stylesheets);
    Theme::scripts($javascripts);

else:
?>

<script type="text/javascript">
	head.ready(document,function() {
		<?php
			foreach(array_unique($stylesheets) as $stylesheet) {
				echo "head.load('".asset($stylesheet)."');".PHP_EOL;
			}
		?>
	});

	head.ready(document,function() {
		<?php
			foreach(array_unique($javascripts) as $javascript) {
				if (preg_match('#^https?://.*#', $javascript)) {
					echo 'head.load("'.$javascript.'");'.PHP_EOL;
				} else {
					echo 'head.load("'.asset($javascript).'");'.PHP_EOL;
				}
			}
		?>
	});
</script>


<?php
endif;
?>
<title>Dashboard</title>
</head>
<body>
<div id="page-wrapper">
<div id="top-bar">
	<div id="top-bar-start">
		<?php
		$env = \Clearworks::getCurrentEnvironment();

		$home_url = \Clearworks::getEnvironmentUrl($env);
		?>
		<h1 id="app-name"><a href="<?php echo $home_url; ?>"><?php echo Config::get('app.name', 'ClearwebApps'); ?></a></h1>
	</div>
	<div id="top-bar-middle">
		<div class="widget">
		<?php
		$search_widget = new \Clearweb\Clearwebapps\Widget\SearchWidget;
		$search_widget->init();
		$search_widget->execute();
		echo $search_widget->getView();
		?>
                </div>
		<ul class="top-nav">
		<li class="action"><a href="/" target="_blank">Home</a></li>
		<li class="action"><a href="/admin/password/action/logout">afmelden</a></li>
	</ul>
	</div>
</div>
<div id="page-content">