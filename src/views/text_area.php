<label><?= $label; ?></label>
<?php if ( ! empty($description)): ?>
<p><?= $description; ?></p>
<?php endif; ?>
<textarea <?= ($disabled)?'disabled="disabled"':'' ?> name="<?= $name ?>" class="<?= $class ?>"><?= $value ?></textarea>