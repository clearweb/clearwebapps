
	
<?php /* if (is_null($selected_id)): ?>
setPageState('<?php echo $list_item_name;?>_id', 0, '<?php echo $list_item_name; ?>_list');
<?php endif; */ ?>
</script>
<div class="widget-body">
<table class="list" data-listtype="<?php echo $list_item_name;?>">
<tr class="list-head">
<?php foreach($columns as $column):?>
<td data-columnname="<?php echo $column; ?>">
<?php echo ucfirst(trans_choice('app.'.$column, 1)); ?>
</td>
<?php endforeach;?>
<?php if( ! empty($action_links)): 
	foreach($action_links as $link):
		?>
		<td class="action-column"></td>
		<?php
	endforeach;
endif;
?>
</tr>
<?php foreach($items as $item):?>
	<tr class="<?php echo($selected_id == $item['id'])?'selected':'';?> list-item" data-itemid="<?php echo (isset($item['id'])?$item['id']:''); ?>">
	<?php foreach($columns as $column):?>
		<td>
		<?php
		if (isset($column_callbacks[$column])) {
			try{
				echo $column_callbacks[$column]($item);
			} catch(Exception $e) {
				if (Config::get('app.debug'))
					echo ('Error in view '.__FILE__.':'.__LINE__.', the callback for column "'.$column.'" gives error for values: '.print_r($item, true).'. The error is: <br />'.$e->getMessage());
			}
		} elseif(isset($item[$column])) {
			echo $item[$column];
		}
		?>
		</td>
	<?php endforeach;?>
	<?php if( ! empty($action_links)): ?>
		<?php
		foreach($action_links as $link):
			?>
			<td class="action-column">
			<?php
			try{
				$link->setParameters($item);
				$link->init();
				$link->execute();
				echo $link->getView();
			} catch(Exception $e) {
				if (Config::get('app.debug'))
					echo ('Error in view '.__FILE__.':'.__LINE__.', the execution of action "'.get_class($link).'" gives error: <br/>'.$e->getMessage());
			}
			?>
			</td>
			<?php
		endforeach;
		?>
	<?php endif; ?>
	</tr>
<?php endforeach;?>
</table>
</div>
