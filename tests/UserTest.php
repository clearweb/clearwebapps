<?php

class UserTest extends TestCase {
    
    public function testAddRemoveRoles() {
        $user = new User;
        
        $user->name = 'test';
        $user->email = 'test@test.nl';
        $user->password = \Hash::make('test');
        
        $user->save();
        
        $this->assertFalse($user->hasRole('test'));
        
        $user->addRole('test');
        
        $this->assertTrue($user->hasRole('test'));
        
        $user->removeRole('test');
        
        $this->assertFalse($user->hasRole('test'));
    }
    
}