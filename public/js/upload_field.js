$(document).ready(function() {
    $('body').on('change', 'input.ajaxupload', function(e) {
	upload_field = this;
	
	upload_container = $(upload_field).closest('.field');
	
	for(raw_file_index in upload_field.files) {
	    if ( ! isNaN(raw_file_index)) {
		
		file = upload_field.files[raw_file_index];
		
		if (uploadIsMultiple(upload_container)) {
		    file_index = uploadGetNewFileIndex(upload_container);
		    file_container = uploadMakeFileContainer(upload_container, file, file_index);
		} else {
		    file_index = 0;
		    uploadRemoveFile(upload_container, 0);
		    file_container = uploadMakeFileContainer(upload_container, file, 0);
		}
		
		uploadFile(upload_container, file, file_index);
	    }
	}
    })
    
    // delete link
    $('body').on('click', '.file-container .delete', function(){
	upload_container = $(this).closest('.field');
	file_index = $(this).closest('li').attr('data-fileindex');
	
	uploadRemoveFile(upload_container, file_index);
    });
    
    // for image upload
    $('body').on('click', '.file-container .placeholder', function(){
	$(this).closest('.field').find('.ajaxupload').click();
    });

})



/* upload functions */

function uploadGetNewFileIndex(upload_container) {
    if ($(upload_container).find('.file-container li').length > 0) {
	last_index = $(upload_container).find('.file-container li').last().attr('data-fileindex');
    } else {
	last_index = 0;
    }
    return Number(last_index) + 1;
}

function uploadMakeFileContainer(upload_container, file, file_index) {
    file_field = $(upload_container).find('input[type="file"]');
    file_name = file.name;
    
    $(upload_container).find('.file-container').append(
	
	$('<li />')
	    .attr('data-fileindex', file_index)
	    .addClass('new-file')
	    .append(
		$('<input />')
		    .attr('type', 'hidden')
		    .attr('name', file_field.attr('data-fieldname'))
	    )
	    .append(
		$('<div/>')
		    .addClass('file-content')
		    .append(
			$('<a target="_blank" class="file-name">' + file_name + '</a>')
		    )
		    .append(
			$('<div class="delete">delete</div>')
		    )
	    )
	    .append(
		$('<progress min="0" max="100" />')
		    .html('0%')
	    )
    );
}

function uploadRemoveFile(upload_container, file_index) {
    $(upload_container).find('.file-container li[data-fileindex="' + file_index + '"]').remove();
    
    if (uploadIsImage(upload_container)) {
	uploadImageRemovePlaceholder(upload_container);
    }
}

function uploadIsMultiple(upload_container) {
    return ($(upload_container).find('input[type="file"]').attr('multiple') == 'multiple');
}

function uploadFile(upload_container, file, file_index) {
    file_name = file['name'];
    
    if (uploadIsImage(upload_container)) {
	$(upload_container).find('.img-loader').show();
	$(upload_container).find('.placeholder span').hide();
    }
    
    var xhr = new XMLHttpRequest();
    xhr.upload.addEventListener("progress", function(e) {uploadProgress(upload_container, file_index, e);}, false);
    xhr.addEventListener("load",  function(e) {uploadComplete(upload_container, file_index, e);}, false);
    xhr.addEventListener("error", function(e) {uploadFailed(upload_container, file_index, e);}, false);
    xhr.addEventListener("abort", function(e) {uploadCancelled(upload_container, file_index, e);}, false);
    
    upload_url = $(upload_container).find('input[type="file"]').attr('data-uploadurl');
    xhr.open("POST", upload_url + '/filename/' + file_name, true);
    
    xhr.onreadystatechange = function (event) {  
	if (xhr.readyState === 4) {
            if (xhr.status != 200) {
		uploadFailed(upload_container, file_index, event);
	    }
	}
    };
    
    xhr.send(file);
}

/* upload image */

function uploadIsImage(upload_container) {
    return ($(upload_container).is('.image-upload-field'));
}

function uploadImageSetPlaceholder(upload_container, image_url) {
    $(upload_container).find('.placeholder').css('background-image', 'url(' + image_url + ')');
}

function uploadImageRemovePlaceholder(upload_container) {
    $(upload_container).find('.placeholder').css('background-image', '');
}


/* - upload events - */

function uploadProgress(upload_container, file_index, event) {
    progress = (event.loaded / event.total) * 100;
    
    $(upload_container).find('li[data-fileindex="' + file_index + '"] progress')
	.val(progress)
	.html(progress + '%');
}

function uploadComplete(upload_container, file_index, event) {
    result = $.parseJSON(event.target.responseText);
    file_id   = result['file_id'];
    file_name = result['file_name'];
    file_url  = result['file_url'];
    
    $(upload_container).find('li[data-fileindex="' + file_index + '"] .file-name')
	.attr('href', file_url)
	.html(file_name)
    ;
    $(upload_container).find('li[data-fileindex="' + file_index + '"] input[type="hidden"]').val(file_id);
    
    
    if (uploadIsImage(upload_container)) {
	uploadImageSetPlaceholder(upload_container, file_url);
	
	$(upload_container).find('.img-loader').hide();
	$(upload_container).find('.placeholder span').show();
    }
}

function uploadFailed(upload_container, file_index, event) {
    if (uploadIsImage(upload_container)) {
	$(upload_container).find('.img-loader').hide();
	$(upload_container).find('.placeholder span').show();
    }
    
    alert('Error: ' + event.target.statusText);
}

function uploadCancelled(upload_container, file_index, event) {
    $(upload_container).find('.img-loader').hide();
    $(upload_container).find('.placeholder span').show();
    
    console.log('cancelled');
}