maximized_widget = false;

function cwaMaximizeWidget(widget) {
    widget_clone = widget.clone();
    
    headerHeight = 40;
    footerHeight = 15;
    screenHeight = $('#page-wrapper').outerHeight() - $('#top-bar').outerHeight();
    screenWidth  = $('#page-wrapper').width() - 40;
    
    widget_clone.css('opacity', '0');

    $('body').append(widget_clone);
    
    widget_clone.css({
	'position': 'absolute',
	'margin' : 0,
    });
    
    widget_clone.position({
	my: "left+10 top+10",
	at: "left top",
	of: widget
    });
    
    widget_clone.animate({
	'opacity': '1.0',
	'height': screenHeight,
	'width': screenWidth,
	'top': 10,
	'left': 10,
    });
    
    widget_clone.position({
	my: "left+10 top+10",
	at: "left top",
	of: "body"
    });
    
    widget_clone.addClass('widget-clone')
    
    maximized_widget = widget_clone;
    
    maximized_widget.disableSelection();
}

function cwaMinimizeWidget(widget) {
    widget.closest('.widget').animate({'opacity': '0.0'}, 300, function() {
	widget.closest('.widget').remove();
	maximized_widget = false;
    });
}

function cwaResizeApp() {
    $('#page-content').height($('body').innerHeight() - $('#top-bar').outerHeight() - $('#footer').outerHeight());
    $('#content').height($('#page-content').outerHeight() + 1);    
}

$(document).ready(function() {
    cwaResizeApp();
    
    $('.widget h3').disableSelection();
    $('body').on('dblclick', '.widget h3', function(e) {
	if ( ! $(e.target).closest('.widget').hasClass('widget-clone')) {
	    cwaMaximizeWidget($(e.target).closest('.widget'));
	}
    });
    
    $('body').on('dblclick', '.widget-clone h3', function(e) {
	cwaMinimizeWidget($(e.target).closest('.widget'));
    });
    
    $(document).keyup(function(e) {
	if (e.keyCode == 27) { 	// esc
	    cwaMinimizeWidget(maximized_widget);
	}
    });
    
    /* disable selection on heads */
    $('body').on('DOMNodeInserted', '.widget', function(e) {
	if ($(e.target).hasClass('communicating-widget')) {
	    $(e.target).find('h3').disableSelection();
	}
    });

});

$(window).resize(function() {
    cwaResizeApp();
});