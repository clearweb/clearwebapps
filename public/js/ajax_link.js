function ajax_link(context) {
    url = context.attr('href');
    after_action = context.attr('data-finish-callback');
    $.get(url, {}, function () {
	// after_action could use the variable: context
	eval(after_action);
    });
}

$(document).ready(function() {
    $('body').on('click', '.ajax-link', function(e) {
	e.stopPropagation();
	e.preventDefault();
	context = $(this);
	
	if ($(this).hasClass('confirm')) {
	    $('body').append(
		$('<div></div>')
		    .attr('id', 'dialog-confirm')
		    .attr('title', context.attr('data-confirm-title'))
		    .html(context.attr('data-confirm-content'))
	    );
	    
	    $( "#dialog-confirm" ).dialog({
		resizable: false,
		modal: true,
		buttons: {
		    "Do it": function() {
			ajax_link(context);
			$( this ).dialog( "close" );
			$( this ).remove();
		    },
		    Cancel: function() {
			$( this ).dialog( "close" );
			$( this ).remove();
		    }
		}
	    });
	} else {
	    ajax_link(context);
	}
    });
});