$(document).ready(function() {
    sexy_sidebar_width = $('.sexy-sidebar').width();
    siblings = {};
    nr_siblings = 0;
    $('.sexy-sidebar').siblings().each(function(index, sibling) {
	if (typeof($(sibling).attr('id')) != "undefined") {
	    siblings[$(sibling).attr('id')] = $(sibling).width();
	    nr_siblings++;
	}
    });
});


function sexyHideSidebar() {
    $.each(siblings, function(index, width) {
	sibling_width = width + Math.floor(sexy_sidebar_width / nr_siblings);
	$('#' + index).animate({'width':sibling_width});
    });
    
    $('.sexy-sidebar').animate({'width': '0px'});
    
    //$('.sexy-sidebar').hide();
    $('.sexy-sidebar .widget').hide();
}

function sexyShowSidebar(communicating_widget_id) {
    /*
    $('#col1').animate({'width':'40%'}, 200);
    $('#col2').animate({'width':'40%'}, 200, 'swing', function() {
	$('.sexy-sidebar').show(800, function() {
	    sexyOpenTab(communicating_widget_id);
	});
    });
    */
    
    sexy_sidebar_queue = $({});
    
    $.each(siblings, function(index, width) {
	//$('#' + index).animate({'width':width});
	sexy_sidebar_queue.queue(function (next) {
	    $('#' + index).animate({'width':width});
	    next();
	})
    });
    
    sexy_sidebar_queue.queue(function (next) {
	$('.sexy-sidebar').animate({'width':sexy_sidebar_width});
	next();
    })
    
    sexy_sidebar_queue.queue(function (next) {
	sexyOpenTab(communicating_widget_id);
	next();
    })
}

function sexyOpenTab(communicating_widget_id) {
    sexyTabsUpdate(communicating_widget_id);
    $('.sexy-sidebar .widget').slideUp();
    $('#' + communicating_widget_id).closest('.widget').slideDown();
}

function sexyTabsUpdate(communicating_widget_id) {
    $('.sexy-sidebar .widget .sexy-tabs').remove();
    sexy_tabs = $('<ul></ul>')
	.addClass('sexy-tabs');

    $('.sexy-sidebar .widget').each(function(index, widget){
	title = $(widget).find('h3').first().text();
	id    = $(widget).find('.communicating-widget').attr('id');
	tab = $('<li></li>')
	    .html(title)
	    .data('widget-id', id)
	    .click(function() {
		sexyOpenTab($(this).data('widget-id'));
	    });
	if (id == communicating_widget_id)
	    tab.addClass('selected');
	sexy_tabs.append(tab);
    });
    $('#' + communicating_widget_id).closest('.widget').prepend(sexy_tabs);
}

$(document).ready(function() {
    sexyHideSidebar();
    
    $('body').on('DOMNodeInserted', '.sexy-sidebar', function(e) {
	if ($(e.target).hasClass('communicating-widget')
	    && typeof($(e.target).attr('id')) != 'undefined') {
	    
	    console.log('dom inserted');
	    communicating_widget_id = $(e.target).attr('id');
	    sexyShowSidebar(communicating_widget_id);
	}
    });
});

