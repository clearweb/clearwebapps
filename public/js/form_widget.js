$(document).ready(function() {
    $('body').on('submit', '.widget-form-container form', function(e) {
	e.stopPropagation();
	e.preventDefault();
	
	form = $(e.target);
	
	$.post(form.attr('action'), form.serialize(), function(data) {
	    console.log(form);
	    form.closest('.widget-form-container')
		.replaceWith(data);
	}).fail(function() {
	    alert('there was an error');
	});
    })
});