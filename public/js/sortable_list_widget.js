$(document).ready(function() {
    $('body').on('click', '.sortable-list-widget .list .list-head td', function() {
	name = $(this).attr('data-columnname');
	list = $(this).closest('.list');
	list_name = list.attr('data-listtype');
	
	if (getPageState(list_name + '_order') == name) {
	    if(getPageState(list_name + '_ordertype') == 'desc') {
		setPageState(list_name + '_ordertype', 'asc');
	    } else {
		setPageState(list_name + '_ordertype', 'desc');
	    }
	} else {
	    setPageState(list_name + '_order', name);
	    setPageState(list_name + '_ordertype', 'asc');
	}
    });
});