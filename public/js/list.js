$(document).ready(function() {
    $('body').on('click', '.list .list-item', function(e) {
	if ($(e.target).closest('.action-column').length < 1) {
	    list_type = $(this).closest('.list').attr('data-listtype');
	    list_id = list_type + '_list';
	    parameter_name = list_type;
	    
	    item_id = $(this).attr('data-itemid');
	    
	    setPageState(parameter_name, item_id, list_id);
	}
    });
});