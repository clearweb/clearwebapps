$(document).ready(function() {
    $('body').on('focus', '.datepicker', function(){
	$(this).datepicker({ dateFormat: 'yy-mm-dd' });
    });
    $('body').on('focus', '.datetimepicker', function(){
	$(this).datetimepicker({ dateFormat: 'yy-mm-dd' });
    });
})