screenHeight = 0;
firstMiddleHeight = 0;
firstBottomHeight = 0;

widgetMiddleHeight = 0;
widgetBottomHeight = 0;

function verticalOverviewInitVars() {
    screenHeight = $('#sidebar').height();
    
    firstMiddleHeight = $('#middle').height();
    firstBottomHeight = $('#bottom').height();
    
    widgetMiddleHeight = $('#middle .widget').height();
    widgetBottomHeight = $('#bottom .widget').height();
}

function calcNewWidgetHeight(oldContainerHeight, oldHeight, newContainerHeight) {
    newHeight = newContainerHeight - (oldContainerHeight - oldHeight);
    return newHeight;
}

$(document).ready(function() {
    verticalOverviewInitVars();
    
    $('#middle').height(screenHeight * 0.3);
    $('#bottom').height(screenHeight * 0.7);
    
    console.log('screen height: ' + screenHeight)
    bottomHeight = $('#bottom').height();
    middleHeight = Number(screenHeight - bottomHeight)
    
    $('#middle .widget').height(calcNewWidgetHeight(firstMiddleHeight, widgetMiddleHeight, middleHeight));
    $('#bottom .widget').height(calcNewWidgetHeight(firstBottomHeight, widgetBottomHeight, bottomHeight));
    
    
    $("#bottom").resizable({
	handles: 'n',
    });
    
    $("#bottom").bind("resize", function (event, ui) {
	bottomHeight = $('#bottom').height();
	middleHeight = Number(screenHeight - bottomHeight);
	
	$('#middle').height(middleHeight);
	$('#bottom').css('top', 0);
	
	$('#middle .widget').height(calcNewWidgetHeight(firstMiddleHeight, widgetMiddleHeight, middleHeight));
	$('#bottom .widget').height(calcNewWidgetHeight(firstBottomHeight, widgetBottomHeight, bottomHeight));
    });
});